import dbUpgrade from './database-upgrade.json';
class DatabaseInit {
  async updateDatabaseTables(db) {
    //console.log("Beginning database updates...");
    // First: create tables if they do not already exist
    return await this.getDatabaseVersion(db)
      .then(version => {
        if (version === dbUpgrade.version) {
          console.log('version matches', version, dbUpgrade.version);
        } else {
          console.log('versions do not match', version, dbUpgrade.version);
          this.upgradeScript(db, version);
        }
        return 0;
      })
      .catch(error => {
        console.log(`getDatabaseVersion: ${error}`);
        return 0;
      });
  }

  async getDatabaseVersion(db) {
    // Select the highest version number from the version table
    //console.log("Beginning getDatabaseVersion...");
    return await db
      .executeSql("SELECT value FROM config WHERE key='db version';")
      .then(([results]) => {
        if (results.rows && results.rows.length > 0) {
          const version = parseInt(results.rows.item(0).value, 10);
          return version;
        } else {
          db.executeSql('REPLACE into config (key,value) VALUES (?,?)', [
            'db version',
            dbUpgrade.version,
          ]);
          return dbUpgrade.version;
        }
      })
      .catch(error => {
        //console.log(`No version set. Returning 0. Details: ${error}`);
        return 0;
      });
  }

  upgradeScript(db, previousVersion) {
    let statements = [];
    let version = dbUpgrade.version - (dbUpgrade.version - previousVersion) + 1;
    let length = Object.keys(dbUpgrade.upgrades).length;

    for (let i = 0; i < length; i++) {
      let upgrade = dbUpgrade.upgrades[`to_v${version}`];
      console.log(upgrade, version);
      if (upgrade) {
        statements = [...statements, ...upgrade];
      } else {
        break;
      }
      version++;
    }

    statements = [
      ...statements,
      ...[
        [
          'REPLACE into config (key,value) VALUES (?,?);',
          ['db version', dbUpgrade.version],
        ],
      ],
    ];

    return db
      .sqlBatch(statements)
      .then(() => console.log('Success!'))
      .catch(error => console.log('Error:', error));
  }
}
module.exports = new DatabaseInit();
