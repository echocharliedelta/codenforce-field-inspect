import SQLite from 'react-native-sqlite-storage';
import DatabaseInit from './DatabaseInit';
import {HelperOffline} from '../utils/HelperOffline';
import {Helper} from '../utils/Helper';
import {InspectionDBUtils} from '../utils/DBUtils/InspectionDBUtils';
import {InspectionSpacesDBUtils} from '../utils/DBUtils/InspectionSpacesDBUtils';
import {InspectionStatus} from '../utils/constants';

const LOCATION = 1;

class Database {
  constructor() {
    this.database_name = 'CNFDB.db';
    // eslint-disable-next-line no-bitwise
    this.conn = SQLite.SQLiteDatabase | undefined;
  }

  async open() {
    SQLite.DEBUG(true);
    SQLite.enablePromise(true);
    let databaseInstance;
    return await SQLite.openDatabase({
      name: this.database_name,
      createFromLocation: LOCATION,
    })
      .then(async db => {
        databaseInstance = db;
        //console.log("[db] Database open!");
        // Perform any database initialization or updates, if needed
        return await DatabaseInit.updateDatabaseTables(databaseInstance);
      })
      .then(() => {
        this.conn = databaseInstance;
        return databaseInstance;
      });
  }

  async getDatabase() {
    if (this.conn) {
      return await Promise.resolve(this.conn);
    }
    // otherwise: open the database first
    return await this.open();
  }

  // get Records from Local DB
  async getRecords(
    tableName,
    selectedFileds = '*',
    where = '',
    otherJoin = '',
    orderBy = '',
  ) {
    return await this.getDatabase()
      .then(async db => {
        this.getAllTables();
        let query = 'SELECT DISTINCT ' + selectedFileds + ' FROM ' + tableName;

        // // add JOIN conditon
        if (!Helper.isUndefined(otherJoin)) {
          query += ' ' + otherJoin;
        }
        // add WHERE conditon
        if (!Helper.isUndefined(where)) {
          query += ' WHERE ' + where;
        }

        // // add sorting conditon
        if (!Helper.isUndefined(orderBy)) {
          query += ' ORDER BY ' + orderBy;
        }

        console.log('🚀 ~ file: Database.js:55 ~ Database ~ query:', query);

        //console.log('Fetch -> ', query);
        return await db.executeSql(query).then(async ([results]) => {
          // console.log("🚀 ~ file: Database.js:76 ~ Database ~ returnawaitdb.executeSql ~ results:", results)
          return await this.fetchAll(results);
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return false;
      });
  }

  async fetchAll(result) {
    let output = [];
    const count = result.rows.length;
    for (let i = 0; i < count; i++) {
      output.push(result.rows.item(i));
    }
    return output;
  }

  fetchOne(result) {
    return result.rows.item(0);
  }

  async getAllTables() {
    return await this.getDatabase()
      .then(async db => {
        return await db
          .executeSql("SELECT name FROM sqlite_master WHERE type ='table'")
          .then(async ([results]) => {
            console.log('RESULTS ====>', results);
            let output = [];
            const count = results.rows.length;
            for (let i = 0; i < count; i++) {
              output.push(results.rows.item(i).name);
            }
            return output;
          })
          .catch(error => {
            console.log('error', error);
            return [];
          });
      })
      .catch(error => {
        return false;
      });
  }

  async truncateTable(table_name, deleted) {
    return this.getDatabase()
      .then(db => {
        let query = 'DELETE FROM ' + table_name;
        if (deleted) {
          query += ' WHERE deleted=1';
        }
        return db.executeSql(query).then(() => {
          return true;
        });
      })
      .catch(error => {
        console.log(error);
        return false;
      });
  }

  //  Insert new Records into Local DB
  async insertList(tableName, itemList, fields) {
    return await this.getDatabase().then(db => {
      let query =
        'INSERT or REPLACE INTO ' +
        tableName +
        ' (' +
        fields.toString() +
        ') VALUES ';
      itemList.forEach(item => {
        query += '(';
        fields.forEach(field => {
          let value = !Helper.isUndefined(item[field])
            ? Helper.escapeDoubleQuotes(item[field])
            : '';
          value = HelperOffline.checkIsBooleanValue(value);
          query += '"' + value + '",';
        });
        query = query.replace(/,\s*$/, '');
        query += '),';
      });
      query = query.replace(/,\s*$/, '');
      console.log('------> ', query);
      return db
        .executeSql(query)
        .then(results => {
          console.log('RESULT +++++++++', results);
          return results;
        })
        .catch(error => {
          console.log('Insert Errror = ', error);
        });
    });
  }

  //  Delete  Records into Local DB
  async deleteRecords(tableName, where = null) {
    return await this.getDatabase()
      .then(async db => {
        let query = 'DELETE FROM ' + tableName;
        if (where != null) {
          query += ' WHERE ' + where;
        }
        return await db.executeSql(query).then(([results]) => {
          return results.rowsAffected;
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return null;
      });
  }

  //  Insert new Records into Local DB
  async insertRecord(tableName, item, fields) {
    return await this.getDatabase().then(async db => {
      let query =
        'INSERT or REPLACE INTO ' +
        tableName +
        ' (' +
        fields.toString() +
        ') VALUES (';

      fields.forEach(field => {
        let value = !Helper.isUndefined(item[field])
          ? Helper.escapeDoubleQuotes(item[field])
          : '';
        query += '"' + value + '",';
      });
      query = query.replace(/,\s*$/, '');
      query += ')';
      return await db
        .executeSql(query)
        .then(async results => {
          return await results;
        })
        .catch(error => {
          console.log('Insert Errror = ', error);
        });
    });
  }

  //  Update new Records into Local DB
  async updateRecord(tableName, nameValueList, where = '') {
    return await this.getDatabase().then(async db => {
      let query = 'UPDATE ' + tableName + ' SET ';
      nameValueList.forEach(item => {
        if (!Helper.isUndefined(item.value)) {
          query +=
            item.name + '="' + Helper.escapeDoubleQuotes(item.value) + '",';
        } else {
          query += item.name + "='" + item.value + "',";
        }
      });
      query = query.replace(/,\s*$/, '');
      query += ' WHERE ' + where;
      console.log('QUERY ***************', query);
      return await db.executeSql(query).then(async results => {
        return results[0].rowsAffected ? 1 : 0;
      });
    });
  }

  async getInspectionOfflineListData(moduleName, tableName, searchParams) {
    let where = '';
    let municipality_code = searchParams?.municipality_code;
    let authPeriod = searchParams?.authPeriod;
    let isInspectionDone = searchParams?.isInspectionDone;

    where +=
      ' WHERE occinspection.muniCode=' +
      municipality_code +
      ' AND occinspection.authPeriod=' +
      authPeriod +
      ' AND occinspection.isInspectionDone=' +
      isInspectionDone;

    if (isInspectionDone === InspectionStatus.DONE) {
      where += ' ORDER BY occinspection.isSync ASC';
    } else {
      where += ' ORDER BY occinspection.effectiveDate DESC';
    }
    console.log('WHERE ========>', where);

    return await this.getDatabase()
      .then(async db => {
        let checklist_join =
          ' JOIN occchecklist ON occinspection.occChecklistId = occchecklist.checkListId ';
        let query =
          'SELECT DISTINCT occinspection.*,occchecklist.title,occchecklist.governingCodeSourceId FROM ' +
          tableName +
          checklist_join;
        if (where) {
          query += where;
        }

        return await db.executeSql(query).then(async ([result]) => {
          let result_array = await this.fetchAll(result);
          if (result_array.length) {
            return await InspectionDBUtils.prepareInspectionObject(
              result_array,
            );
          } else {
            return {data: []};
          }
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return false;
      });
  }

  async getInspectedSpacesOfflineListData(moduleName, tableName, searchParams) {
    let where = '';
    let inspectionId = searchParams.inspectionId;
    let isFromUpload = Helper.isUndefined(searchParams?.isFromUpload)
      ? false
      : searchParams?.isFromUpload;
    let userId = Helper.isUndefined(searchParams?.userId)
      ? ''
      : searchParams?.userId;

    where += `WHERE occInspectionId ='${inspectionId}' AND occinspectedspaces.deactivatedts='' OR occinspectedspaces.deactivatedts IS NULL`;
    return await this.getDatabase()
      .then(async db => {
        let occchecklistspacetype_join =
          ' JOIN occchecklistspacetype ON occinspectedspaces.occChecklistSpaceTypeId = occchecklistspacetype.checklistSpaceTypeId ';
        let orderBy = ' ORDER BY occchecklistspacetype.spaceTitle ASC ';

        let query =
          'SELECT DISTINCT occinspectedspaces.*,occchecklistspacetype.* FROM ' +
          tableName +
          occchecklistspacetype_join;

        if (where) {
          query += where + orderBy;
        }

        console.log('QUERY ---------------******', query);

        return await db.executeSql(query).then(async ([result]) => {
          let result_array = await this.fetchAll(result);
          if (result_array.length) {
            return await InspectionSpacesDBUtils.prepareInspectionSpacesObject(
              result_array,
              isFromUpload,
              userId,
            );
          } else {
            return {data: []};
          }
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return false;
      });
  }

  async getInspectedSpacesRelationData(moduleName, tableName, searchParams) {
    let where = '';
    let inspectedSpaceId = searchParams.inspectedSpaceId;
    let isFromUpload = searchParams.isFromUpload;

    where += ` WHERE inspectedSpaceId='${inspectedSpaceId}'`;

    // console.log("WHERE ========>", where);

    return await this.getDatabase()
      .then(async db => {
        let query;
        let codeElement_join =
          ' JOIN codeelement ON occInspectedSpaceElementList.codeElementId = codeelement.elementId';

        if (isFromUpload) {
          query =
            'SELECT DISTINCT occInspectedSpaceElementList.* FROM ' + tableName;
        } else {
          query =
            'SELECT DISTINCT occInspectedSpaceElementList.*,codeelement.ordSubSecTitle,codeelement.elementId,codeelement.name,codeelement.year,codeelement.ordSubSecNum FROM ' +
            tableName +
            codeElement_join;
        }

        if (where) {
          query += where;
        }

        console.log('QUERY ---------------******', query);

        return await db.executeSql(query).then(async ([result]) => {
          let result_array = await this.fetchAll(result);
          if (result_array.length) {
            return result_array;
          } else {
            return {data: []};
          }
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return false;
      });
  }

  async getInspectionOverallPhotosListData(
    moduleName,
    tableName,
    searchParams,
  ) {
    let inspectionId = searchParams?.inspectionId;
    let photosIds;
    if (searchParams?.ordinancePhotosIds?.length) {
      const quotedElements = searchParams?.ordinancePhotosIds.map(
        photo => `'${photo.bytesId}'`,
      );
      photosIds = quotedElements.join(', ');
    }
    // let ordinancePhotosIds = searchParams?.ordinancePhotosIds?.map(photo => photo.bytesId) || [];
    // console.log('ordinance photo ids', ordinancePhotosIds);
    // let ordinancePhotosIds = searchParams?.ordinancePhotosIds;
    let where = '';
    let whereQuery = `inspectionId='${inspectionId}' AND deactivatedts='' OR deactivatedts IS NULL ORDER BY imageOrder ASC`;

    if (searchParams?.ordinancePhotosIds?.length) {
      where += ' WHERE bytesId NOT IN ' + `(${photosIds}) AND ` + whereQuery;
    } else {
      where += ' WHERE ' + whereQuery;
    }

    console.log('WHERE ========>', where);

    return await this.getDatabase()
      .then(async db => {
        let query = 'SELECT DISTINCT blobbytes.* FROM ' + tableName;
        if (where) {
          query += where;
        }

        console.log('QUERY ---------------******', query);

        return await db.executeSql(query).then(async ([result]) => {
          let result_array = await this.fetchAll(result);
          if (result_array.length) {
            return await InspectionDBUtils.prepareInspectionOverallPhotosObject(
              result_array,
            );
          } else {
            return {data: []};
          }
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return false;
      });
  }

  async getOrdinanceAndCategories(moduleName, tableName, searchParams) {
    let where = '';
    let inspectedSpaceId = searchParams?.inspectedSpaceId;
    let guideEntryId = searchParams?.guideEntryId;

    where += ` WHERE inspectedSpaceId='${inspectedSpaceId}'`;

    if (!Helper.isUndefined(guideEntryId)) {
      console.log('In--------->');
      where += ` AND codeelementguide.guideEntryId='${guideEntryId}'`;
    }

    console.log('WHERE ========>', where);

    return await this.getDatabase()
      .then(async db => {
        let codeElement_join =
          ' JOIN codeelement ON occInspectedSpaceElementList.codeElementId = codeelement.elementId ';
        let codeElementGuide_join =
          ' LEFT JOIN codeelementguide ON codeelement.guideEntryId = codeelementguide.guideEntryId ';
        let query =
          'SELECT DISTINCT occInspectedSpaceElementList.*,codeelement.*,codeelementguide.* FROM ' +
          tableName +
          codeElement_join +
          codeElementGuide_join;
        if (where) {
          query += where;
        }

        console.log('QUERY ---------------******', query);

        return await db.executeSql(query).then(async ([result]) => {
          let result_array = await this.fetchAll(result);
          if (result_array.length) {
            // console.log('-----result array----', result_array)
            return await InspectionDBUtils.prepareInspectedOrdinanceAndCategories(
              result_array,
            );
          } else {
            return {data: []};
          }
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return false;
      });
  }

  async getOrdinanceAndCategoriesDetails(moduleName, tableName, searchParams) {
    let where = '';
    let guideEntryId = searchParams?.guideEntryId;
    let checklistSpaceTypeId = searchParams?.checklistSpaceTypeId;

    where += ` WHERE checklistSpaceTypeId='${checklistSpaceTypeId}' AND (occChecklistSpaceTypeElements.deactivatedts='' OR occChecklistSpaceTypeElements.deactivatedts IS NULL)`;

    if (!Helper.isUndefined(guideEntryId)) {
      where += ` AND codeelementguide.guideEntryId='${guideEntryId}'`;
    }

    console.log('WHERE ========>', where);

    return await this.getDatabase()
      .then(async db => {
        let codeElementGuide_join =
          ' LEFT JOIN codeelementguide ON codeelement.guideEntryId = codeelementguide.guideEntryId ';
        let codeElement_Join =
          ' JOIN codeelement ON occChecklistSpaceTypeElements.codeElementLinkedId = codeelement.elementId ';
        let query =
          'SELECT DISTINCT occChecklistSpaceTypeElements.*,codeelement.*,codeelementguide.* FROM ' +
          tableName +
          codeElement_Join +
          codeElementGuide_join;
        if (where) {
          query += where;
        }

        return await db.executeSql(query).then(async ([result]) => {
          let result_array = await this.fetchAll(result);
          console.log('-----result array----', result_array);
          if (result_array.length) {
            console.log('-----result array----', result_array);
            return await InspectionDBUtils.prepareInspectedOrdinanceAndCategories(
              result_array,
            );
          } else {
            return {data: []};
          }
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return false;
      });
  }

  async getInspectionDetails(moduleName, tableName, searchParams) {
    let where = '';
    let inspectionId = searchParams?.inspectionId;
    where += ` WHERE inspectionId='${inspectionId}'`;

    console.log('WHERE ========>', where);

    return await this.getDatabase()
      .then(async db => {
        let query = 'SELECT * FROM ' + tableName;
        if (where) {
          query += where;
        }

        return await db.executeSql(query).then(async ([result]) => {
          let result_array = await this.fetchAll(result);
          if (result_array.length) {
            return await InspectionDBUtils.prepareInspectionDetailsObject(
              result_array,
            );
          } else {
            return {data: []};
          }
        });
      })
      .catch(error => {
        console.log('error from Database..', error);
        return false;
      });
  }

  async updateInspectionData(inspectionData) {
    return await this.getDatabase()
      .then(async db => {
        let queries = [];
        queries.push(
          `UPDATE occinspection SET inspectionId=${
            inspectionData?.inspectionId
          },
             isSync=${1} WHERE inspectionId='${
            inspectionData?.appInspectionId
          }'`,
        );
        queries.push(
          `UPDATE blobbytes SET inspectionId='${inspectionData?.inspectionId}'
            WHERE inspectionId='${inspectionData?.appInspectionId}'`,
        );
        // console.log("INSPECTION QUERY **************",queries);
        return await db.sqlBatch(queries).then(async result => {
          return true;
        });
      })
      .catch(error => {
        console.log(error);
        return false;
      });
  }

  async updateInspectedSpacesData(inspectionData) {
    return await this.getDatabase()
      .then(async db => {
        let queries = [];
        await inspectionData.occInspectedSpaceDTOList.forEach(inspection => {
          queries.push(
            `UPDATE occinspectedspaces SET inspectedSpaceId=${inspection?.inspectedSpaceId},
               occInspectionId=${inspectionData?.inspectionId} WHERE inspectedSpaceId='${inspection?.appInspectedSpaceId}'`,
          );
        });
        return await db.sqlBatch(queries).then(async result => {
          return true;
        });
      })
      .catch(error => {
        console.log(error);
        return false;
      });
  }

  async updateInspectedSpaceElementData(inspectionData) {
    return await this.getDatabase()
      .then(async db => {
        let queries = [];
        await inspectionData.occInspectedSpaceDTOList.forEach(inspection => {
          inspection?.occInspectedSpaceElementDTOList.forEach(element => {
            queries.push(
              `UPDATE occInspectedSpaceElementList SET inspectedSpaceId=${inspection?.inspectedSpaceId},
              inspectedSpaceElementId=${element?.inspectedSpaceElementId} WHERE inspectedSpaceElementId='${element?.appInspectedSpaceElementId}'`,
            );
            queries.push(
              `UPDATE occinspectedspaceelementphotodoc SET 
              inspectedSpaceElementId=${element?.inspectedSpaceElementId} WHERE inspectedSpaceElementId='${element?.appInspectedSpaceElementId}'`,
            );
          });
        });
        // console.log("INSPECTION SPACES ELEMENT QUERY **************",queries);
        return await db.sqlBatch(queries).then(async result => {
          return true;
        });
      })
      .catch(error => {
        console.log(error);
        return false;
      });
  }

  async updateBlobByteData(inspectionId) {
    return await this.getDatabase()
      .then(async db => {
        let queries = [];
        queries.push(
          `UPDATE blobbytes SET isSync=${0}
          WHERE inspectionId='${inspectionId}'`,
        );
        return await db.sqlBatch(queries).then(async result => {
          return true;
        });
      })
      .catch(error => {
        console.log(error);
        return false;
      });
  }

  async deleteDiscardedPhotosFromFindings(bytesIds) {
    try {
      const db = await this.getDatabase();
      const queries = bytesIds.map(
        bytesId =>
          `UPDATE occinspectedspaceelementphotodoc SET deactivatedts='${Helper.getCurrentUTCDateTime()}' WHERE bytesId='${bytesId}'`,
      );

      await db.sqlBatch(queries);
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
}

module.exports = new Database();
