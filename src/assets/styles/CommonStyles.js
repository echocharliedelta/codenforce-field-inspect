import {StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';

export const dynamicCommonStyles = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const Colors = useTheme().colors;

  return StyleSheet.create({
    // header style
    hederStyle: {
      backgroundColor: Colors.BG_SECONDARY,
    },

    html: {
      fontFamily: 'SF Pro',
    },

    hederTitleStyle: {
      color: Colors.TEXT_SECONDARY,
      fontSize: 30,
      fontWeight: '700',
      // marginLeft: 18,
      marginTop: 3,
      fontFamily: 'SF Pro',
    },
    // Button styles
    buttonPrimary: {
      height: 50,
      borderRadius: 8,
      backgroundColor: Colors.BUTTON_PRIMARY,
      width: '100%',
    },

    buttonPrimaryTitle: {
      fontSize: 17,
      fontWeight: '400',
      fontFamily: 'SF Pro',
      lineHeight: 22,
      color: Colors.WHITE,
    },

    // login textinput icon style
    loginTextInputIcon: {
      position: 'absolute',
      bottom: 6,
      right: 16,
    },
    // search textinput icon style
    searchInputIcon: {
      position: 'relative',
      bottom: 6,
      left: 0,
    },

    // inspection card title
    inspectionCardTitle: {
      color: Colors.TEXT_DARK_2,
      fontFamily: 'SF Pro',
      fontSize: 17,
      fontWeight: '700',
      marginBottom: 10,
    },

    // inspection card icons styles
    inspectionCardIconContainer: {
      position: 'absolute',
      top: 0,
      width: 40,
      right: 70,
    },

    inspectionCardDeleteContainer: {
      position: 'absolute',
      top: 0,
      width: 40,
      right: 18,
    },

    inspectionCardIcon: {
      color: Colors.BUTTON_PRIMARY,
    },

    inspectionCardDeleteIcon: {
      color: Colors.TEXT_MAIN,
      opacity: 0.7,
    },
    inspectionCardBedge: {
      position: 'absolute',
      top: -4,
      right: 11,
      color: '#fff',
      backgroundColor: Colors.BG_PRIMARY,
      paddingHorizontal: 6,
      borderRadius: 20,
    },

    inspectionCardBedgeIOS: {
      position: 'absolute',
      top: -4,
      right: 10,
      color: '#fff',
      overflow: 'hidden',
      backgroundColor: Colors.BG_PRIMARY,
      paddingHorizontal: 6,
      paddingVertical: 2,
      borderRadius: 11,
    },

    inspectionCardCheck: {
      borderRadius: 50,
      overflow: 'hidden',
      marginBottom: 12,
      marginLeft: 12,
      height: 19,
      width: 19,
      justifyContent: 'center',
      alignContent: 'center',
    },

    // Spaces card icons styles
    spacesCardIcon: {
      marginVertical: 10,
      color: Colors.TEXT_MAIN,
      opacity: 0.7,
    },
    spacesCardPencilIcon: {
      color: Colors.BG_PRIMARY,
    },

    SpacesCardBottomTabs: {
      position: 'relative',
      bottom: 0,
      backgroundColor: Colors.BUTTON_PRIMARY,
      width: '100%',
      marginLeft: 0,
      borderWidth: 0,
      borderRadius: 0,
      marginVertical: 0,
    },
    DoneSpacesCardBottomTabs: {
      position: 'absolute',
      bottom: 0,
      backgroundColor: Colors.TEXT_LIGHT,
      width: '100%',
      marginLeft: 0,
      borderWidth: 0,
      borderRadius: 0,
      marginVertical: 0,
    },
    spacesCardIconContainer: {
      position: 'absolute',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      top: 0,
      right: 18,
      backgroundColor: Colors.BG_LIGHT_BLUE,
      height: 35,
      width: 35,
      borderRadius: 4,
    },

    // ordinances card styles
    ordinancesCardContainer: {
      marginHorizontal: 10,
      borderRadius: 44,
      backgroundColor: 'transparent',
      marginTop: 10,
      borderColor: Colors.TEXT_BLUE,
    },

    // overall photos css
    overallPhotosBadge: {
      overflow: 'hidden',
      backgroundColor: Colors.BG_SEARCH,
      fontSize: 10,
      fontWeight: 'bold',
      textAlign: 'center',
      color: Colors.WHITE,
      marginTop: 10,
      marginLeft: 8,
      paddingHorizontal: 8,
      paddingVertical: 5,
      borderRadius: 11,
    },
    overallPhotosCard: {
      width: 90,
      borderWidth: 1,
      height: 90,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      borderStyle: 'dashed',
      borderRadius: 8,
      marginRight: 18,
    },

    // image card styles
    imageCard: {
      height: 80,
      width: 80,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#C6ECF8',
      borderRadius: 8,
      marginRight: 10,
      marginTop: 16,
    },
    imageCardClose: {
      position: 'absolute',
      right: 5,
      top: 5,
    },
    imageCardBadge: {
      overflow: 'hidden',
      position: 'absolute',
      right: 5,
      bottom: 5,
      backgroundColor: Colors.BG_PRIMARY,
      color: Colors.WHITE,
      paddingHorizontal: 4,
      paddingVertical: 3,
      textAlign: 'center',
      textAlignVertical: 'center',
      fontSize: 10,
      borderRadius: 8,
    },

    // add spaces styles
    addSpaceButton: {
      position: 'absolute',
      bottom: 0,
      width: '100%',
      marginTop: 10,
    },
    // icon styles
    iconColor: {
      color: Colors.TEXT_INPUT,
    },

    //opacity
    opacity20: {
      opacity: 0.2,
    },
    opacity50: {
      opacity: 0.5,
    },

    // text style
    textCapital: {
      textTransform: 'capitalize',
    },
    textUpper: {
      textTransform: 'uppercase',
    },
    textLower: {
      textTransform: 'lowercase',
    },

    // colors
    colorBlack: {
      color: Colors.BLACK,
    },

    colorWhite: {
      color: Colors.WHITE,
    },
    textPrimary: {
      color: Colors.TEXT_PRIMARY,
    },
    textSecondary: {
      color: Colors.TEXT_SECONDARY,
    },
    textInput: {
      color: Colors.TEXT_INPUT,
    },
    textLight: {
      color: Colors.TEXT_LIGHT,
    },
    textMain: {
      color: Colors.TEXT_MAIN,
    },
    textDark1: {
      color: Colors.TEXT_DARK_1,
    },
    textDark2: {
      color: Colors.TEXT_DARK_2,
    },
    textDark3: {
      color: Colors.TEXT_DARK_3,
    },
    textBlue: {
      color: Colors.TEXT_BLUE,
    },
    textWhite: {
      color: Colors.WHITE,
    },

    // icon colors
    dialogIconColor: {
      color: Colors.ICON_COLOR,
    },

    // background colors
    bgPrimary: {
      backgroundColor: Colors.BG_PRIMARY,
    },
    bgSecondary: {
      backgroundColor: Colors.BG_SECONDARY,
    },
    bgTextarea: {
      backgroundColor: Colors.BG_TEXTAREA,
    },
    bgSearchInput: {
      backgroundColor: Colors.BG_SEARCH,
    },
    bgBottomSheet: {
      backgroundColor: Colors.BG_BOTTOMSHEET,
    },
    bgBottomNavigation: {
      backgroundColor: Colors.BG_BOTTOM_NAVIGATION,
    },
    bgPrimaryLight: {
      backgroundColor: Colors.BUTTON_PRIMARY_LIGHT,
    },
    bgSynch: {
      backgroundColor: Colors.BG_SYNC,
    },
    bgGreen: {
      backgroundColor: Colors.BG_GREEN,
    },
    bgBlue: {
      backgroundColor: Colors.BUTTON_PRIMARY,
    },
    bgCategory: {
      backgroundColor: Colors.BG_CATEGORY,
    },
    bgTextLight: {
      backgroundColor: Colors.TEXT_LIGHT,
    },
    bgButtonUploadInsp: {
      backgroundColor: Colors.BUTTON_UPLOAD_INSP,
    },

    // color styles
    colorLogout: {
      color: Colors.BUTTON_PRIMARY,
    },

    // width styles
    w100: {
      width: 100,
    },
    w120: {
      width: 120,
    },
    w126: {
      width: 126,
    },
    w140: {
      width: '38%',
    },
    w90: {
      width: '90%',
    },
    w60: {
      width: '60%',
    },
    w10: {
      width: 10,
    },
    // min height style
    minH20: {
      minHeight: 20,
    },
    minH30: {
      minHeight: 30,
    },
    minH35: {
      minHeight: 35,
    },
    minH36: {
      minHeight: 36,
    },
    minH40: {
      minHeight: 40,
    },
    minH50: {
      minHeight: 50,
    },
    minH60: {
      minHeight: 60,
    },
    // max height
    maxH30: {
      maxHeight: 30,
    },
    maxH35: {
      maxHeight: 35,
    },
    maxH36: {
      maxHeight: 36,
    },
    maxH40: {
      maxHeight: 40,
    },
    // height Styles
    h5: {
      height: 5,
    },
    h10: {
      height: 10,
    },
    h15: {
      height: 15,
    },
    h20: {
      height: 20,
    },
    h25: {
      height: 25,
    },
    h30: {
      height: 30,
    },
    h35: {
      height: 35,
    },
    h40: {
      height: 40,
    },
    h45: {
      height: 45,
    },
    h50: {
      height: 50,
    },
    h55: {
      height: 55,
    },
    h60: {
      height: 60,
    },
    h70: {
      height: 70,
    },
    h600: {
      height: 600,
    },
    // text alignments
    textAlignLeft: {
      textAlign: 'left',
    },
    textAlignRight: {
      textAlign: 'right',
    },
    textAlignCenter: {
      textAlign: 'center',
    },
    textAlignVerticalTop: {
      textAlignVertical: 'top',
    },
    textAlignJustify: {
      textAlign: 'justify',
    },

    // flex style
    dFlex: {
      display: 'flex',
    },
    fRow: {
      flexDirection: 'row',
    },
    fColumn: {
      flexDirection: 'column',
    },
    F1: {
      flex: 1,
    },
    F2: {
      flex: 2,
    },

    fGrow1: {
      flexGrow: 1,
    },
    fGrow2: {
      flexGrow: 2,
    },
    fGrow3: {
      flexGrow: 3,
    },
    fGrow4: {
      flexGrow: 4,
    },
    fGrow5: {
      flexGrow: 5,
    },
    fGrow6: {
      flexGrow: 6,
    },
    fGrow7: {
      flexGrow: 7,
    },
    fGrow8: {
      flexGrow: 8,
    },

    fShrink1: {
      flexShrink: 1,
    },
    fShrink2: {
      flexShrink: 2,
    },
    fShrink3: {
      flexShrink: 3,
    },
    fShrink4: {
      flexShrink: 4,
    },
    fShrink5: {
      flexShrink: 5,
    },
    // flex wrap
    fWrap: {
      flexWrap: 'wrap',
    },

    // flex virtical alignment
    alignItemsStart: {
      alignItems: 'flex-start',
    },
    alignItemsEnd: {
      alignItems: 'flex-end',
    },
    alignItemsCenter: {
      alignItems: 'center',
    },

    // flex horizontal alignment
    justifyContentStart: {
      justifyContent: 'flex-start',
    },
    justifyContentEnd: {
      justifyContent: 'flex-end',
    },
    justifyContentSpaceBetween: {
      justifyContent: 'space-between',
    },
    justifyContentEvenly: {
      justifyContent: 'space-evenly',
    },
    justifyContentCenter: {
      justifyContent: 'center',
    },

    // font family
    ftSFpro: {
      fontFamily: 'SF Pro',
    },
    // font size
    ftSize10: {
      fontSize: 10,
    },
    ftSize11: {
      fontSize: 11,
    },
    ftSize12: {
      fontSize: 12,
    },
    ftSize13: {
      fontSize: 13,
    },
    ftSize14: {
      fontSize: 14,
    },
    ftSize15: {
      fontSize: 15,
    },
    ftSize16: {
      fontSize: 16,
    },
    ftSize17: {
      fontSize: 17,
    },
    ftSize18: {
      fontSize: 18,
    },
    ftSize20: {
      fontSize: 20,
    },
    ftSize22: {
      fontSize: 22,
    },
    ftSize30: {
      fontSize: 30,
    },
    ftSize50: {
      fontSize: 50,
    },

    // font weight

    ftWeight500: {
      fontWeight: '500',
    },
    ftWeight600: {
      fontWeight: '600',
    },
    ftWeight700: {
      fontWeight: '700',
    },
    ftWeight800: {
      fontWeight: '800',
    },

    // margin vertical
    mV0: {
      marginVertical: 0,
    },
    mV5: {
      marginVertical: 5,
    },
    mV10: {
      marginVertical: 10,
    },
    mV15: {
      marginVertical: 15,
    },
    mV20: {
      marginVertical: 20,
    },
    // margin horizontal
    mH5: {
      marginHorizontal: 5,
    },
    mH10: {
      marginHorizontal: 10,
    },
    mH15: {
      marginHorizontal: 15,
    },
    mH18: {
      marginHorizontal: 18,
    },
    mH20: {
      marginHorizontal: 20,
    },
    // line Height
    lH10: {
      lineHeight: 10,
    },
    lH20: {
      lineHeight: 20,
    },
    lH30: {
      lineHeight: 30,
    },
    // margin top
    // top margin
    mT0: {
      marginTop: 0,
    },
    mT1: {
      marginTop: 1,
    },
    mT2: {
      marginTop: 2,
    },
    mT3: {
      marginTop: 3,
    },
    mT5: {
      marginTop: 5,
    },
    mT10: {
      marginTop: 10,
    },
    mT16: {
      marginTop: 16,
    },
    mT17: {
      marginTop: 17,
    },
    mT18: {
      marginTop: 18,
    },
    mT19: {
      marginTop: 19,
    },
    mT20: {
      marginTop: 20,
    },
    mT25: {
      marginTop: 25,
    },

    // margin Bottom
    mB0: {
      marginBottom: 0,
    },
    mB5: {
      marginBottom: 5,
    },
    mB10: {
      marginBottom: 10,
    },
    mB15: {
      marginBottom: 15,
    },
    mB20: {
      marginBottom: 20,
    },
    mB25: {
      marginBottom: 25,
    },
    mB30: {
      marginBottom: 30,
    },
    mB35: {
      marginBottom: 35,
    },
    mB40: {
      marginBottom: 40,
    },
    mB45: {
      marginBottom: 45,
    },
    mB48: {
      marginBottom: 48,
    },
    mB50: {
      marginBottom: 50,
    },
    mB55: {
      marginBottom: 55,
    },
    mB60: {
      marginBottom: 60,
    },
    mB65: {
      marginBottom: 65,
    },
    mB70: {
      marginBottom: 70,
    },
    mB75: {
      marginBottom: 77,
    },
    mB77: {
      marginBottom: 75,
    },
    mB80: {
      marginBottom: 80,
    },
    mB85: {
      marginBottom: 85,
    },

    // margin Left
    mL0: {
      marginLeft: 0,
    },
    mL5: {
      marginLeft: 5,
    },
    mL10: {
      marginLeft: 10,
    },
    mL11: {
      marginLeft: 11,
    },
    mL12: {
      marginLeft: 12,
    },
    mL13: {
      marginLeft: 13,
    },
    mL15: {
      marginLeft: 15,
    },
    mL16: {
      marginLeft: 16,
    },
    mL17: {
      marginLeft: 17,
    },
    mL18: {
      marginLeft: 18,
    },
    mL20: {
      marginLeft: 20,
    },
    mL25: {
      marginLeft: 25,
    },
    mL30: {
      marginLeft: 30,
    },
    mL60: {
      marginLeft: 60,
    },
    mL65: {
      marginLeft: 65,
    },
    mL70: {
      marginLeft: 70,
    },

    // margin right
    mR0: {
      marginRight: 0,
    },
    mR3: {
      marginRight: 3,
    },
    mR5: {
      marginRight: 5,
    },
    mR10: {
      marginRight: 10,
    },
    mR15: {
      marginRight: 15,
    },
    mR18: {
      marginRight: 18,
    },
    mR20: {
      marginRight: 20,
    },
    mR25: {
      marginRight: 25,
    },
    mR30: {
      marginRight: 30,
    },
    mR35: {
      marginRight: 35,
    },
    mR60: {
      marginRight: 60,
    },
    mR65: {
      marginRight: 65,
    },
    mR70: {
      marginRight: 70,
    },
    mR75: {
      marginRight: 75,
    },

    // padding vertical
    pV0: {
      paddingVertical: 0,
    },
    pV5: {
      paddingVertical: 5,
    },

    // padding horizontal
    pH0: {
      paddingHorizontal: 0,
    },
    pH5: {
      paddingHorizontal: 5,
    },
    pH10: {
      paddingHorizontal: 10,
    },
    pH15: {
      paddingHorizontal: 15,
    },
    pH18: {
      paddingHorizontal: 18,
    },
    pH20: {
      paddingHorizontal: 20,
    },
    // padding styles
    p0: {
      padding: 0,
    },
    pd2: {
      padding: 2,
    },
    pd3: {
      padding: 3,
    },
    pd5: {
      padding: 5,
    },
    pd10: {
      padding: 10,
    },
    pd15: {
      padding: 15,
    },
    pd16: {
      padding: 16,
    },
    pd20: {
      padding: 20,
    },
    pd30: {
      padding: 30,
    },

    // padding top
    pdT0: {
      paddingTop: 0,
    },
    pdT1: {
      paddingTop: 1,
    },
    pdT2: {
      paddingTop: 2,
    },
    pdT3: {
      paddingTop: 3,
    },
    pdT5: {
      paddingTop: 5,
    },
    pdT10: {
      paddingTop: 10,
    },
    pdT15: {
      paddingTop: 15,
    },
    pdT20: {
      paddingTop: 20,
    },

    // padding Bottom
    pdB0: {
      paddingBottom: 0,
    },
    pdB2: {
      paddingBottom: 2,
    },
    pdB5: {
      paddingBottom: 5,
    },
    pdB8: {
      paddingBottom: 8,
    },
    pdB10: {
      paddingBottom: 10,
    },
    pdB11: {
      paddingBottom: 11,
    },
    pdB14: {
      paddingBottom: 14,
    },
    pdB15: {
      paddingBottom: 15,
    },
    pdB20: {
      paddingBottom: 20,
    },
    pdB40: {
      paddingBottom: 40,
    },
    pdB60: {
      paddingBottom: 60,
    },
    pdB70: {
      paddingBottom: 70,
    },
    // padding Right
    pdR0: {
      paddingRight: 0,
    },
    pdR5: {
      paddingRight: 5,
    },
    pdR10: {
      paddingRight: 10,
    },
    pdR15: {
      paddingRight: 15,
    },

    // padding Left
    pdL0: {
      paddingLeft: 0,
    },
    pdL5: {
      paddingLeft: 5,
    },
    pdL10: {
      paddingLeft: 10,
    },
    pdL15: {
      paddingLeft: 15,
    },

    // border
    bPrimary: {
      borderWidth: 1,
      borderColor: Colors.BUTTON_PRIMARY,
    },
    bPrimaryDark: {
      borderWidth: 1,
      borderColor: Colors.TEXT_SECONDARY,
    },
    bPrimaryLight: {
      borderWidth: 1,
      borderColor: Colors.BG_LIGHT_BLUE,
    },
    // border top
    bT1: {
      borderTopWidth: 1,
    },

    bTprimary: {
      borderTopColor: Colors.BORDER_PRIMARY,
    },
    // border Bottom
    bBtransparent: {
      borderBottomColor: 'transparent',
    },
    bBTransparent: {
      // borderBottomColor: 'transparent',
      borderBottomWidth: 0,
    },

    bBprimary: {
      borderBottomColor: Colors.BORDER_PRIMARY,
    },
    bBPrimary: {
      borderBottomColor: Colors.BORDER_PRIMARY,
    },

    bB1: {
      borderBottomWidth: 1,
    },

    // border radius
    bR0: {
      borderRadius: 0,
    },
    bR3: {
      borderRadius: 3,
    },
    bR4: {
      borderRadius: 4,
    },
    bR5: {
      borderRadius: 5,
    },
    bR7: {
      borderRadius: 7,
    },
    bR16: {
      borderRadius: 16,
    },
    bR10: {
      borderRadius: 10,
    },
    bR44: {
      borderRadius: 44,
    },

    // common styles for pages
    // inspection Details Screen
    dateTimePickerInput: {
      height: 25,
      paddingTop: 5,
      fontSize: 17,
      color: Colors.TEXT_INPUT,
    },
    customInputStyle: {
      textAlignVertical: 'top',
      paddingTop: 5,
      fontSize: 17,
      color: Colors.TEXT_INPUT,
    },

    // common style for inspection card
    inspectionCardNames: {
      paddingBottom: 8,
      fontSize: 15,
      fontFamily: 'SF Pro',
    },

    incrementDecrementRightIcon: {
      overflow: 'hidden',
      borderWidth: 1,
      borderColor: Colors.TEXT_INPUT,
      borderRadius: 15,
      textAlign: 'center',
      textAlignVertical: 'center',
      marginRight: 20,
      marginBottom: 10,
      paddingHorizontal: 3,
      paddingVertical: 3,
    },

    incrementDecrementLeftIcon: {
      overflow: 'hidden',
      borderWidth: 1,
      borderColor: Colors.TEXT_INPUT,
      borderRadius: 14,
      textAlign: 'center',
      textAlignVertical: 'center',
      marginRight: 20,
      marginLeft: 5,
      marginBottom: 10,
      paddingHorizontal: 3,
      paddingVertical: 3,
    },
  });
};
