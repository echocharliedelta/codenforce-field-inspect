// import axiosInstance from "../config/inspectionAxios.config";
import inspectionAxiosInstance from "../config/inspectionAxios.config";
// import axiosInstance from "../config/axios.config";
import { OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME, microServices } from "../config/config";
import Database from '../database/Database';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {MasterDBUtils} from '../utils/DBUtils/MasterDBUtils';
import { Helper } from "../utils/Helper";
import { useDispatch } from "react-redux";
import { setUploadedInspectionId } from "../store/slices/authSlice";

const getInspectionDeterminationSyncService = async (searchParams = '') => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'determinations' + searchParams)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};


const getInspectionCauseSyncService = async (searchParams = '') => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'causes' + searchParams)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};

const getInspectionIntensityClassesSyncService = async (searchParams = '') => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'intensityclasses' + searchParams)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};

const getInspectionChecklistsSyncService = async (searchParams = '') => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'checklists' + searchParams)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};

const getCodeElementsSyncService = async (searchParams = '') => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'codeelements' + searchParams)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};

const getCodeElementGuideSyncService = async (searchParams = '') => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'codeelementguides' + searchParams)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};

const getChecklistSpaceTypesSyncService = async (searchParams = '') => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'checklistspacetypes' + searchParams)
    .then(async response => {
       return await MasterDBUtils.prepareOccChecklistSpaceTypeObject(response.data);
    })
    .catch(error => {
      throw error;
    });
};

const getChecklistSpaceTypeElementsSyncService = async (searchParams = '') => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'checklistspacetypeelements' + searchParams)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};


const getInspectionsData = async (data, moduleName) => {
  const occInspectedSpaces = [];
  const inspectedSpaceElements = [];
  const inspections = [];
  const authPeriod = await AsyncStorage.getItem('auth_period')
  await data.forEach((master) => {
    const { occInspectedSpaceDTOList, ...rest } = master;
    inspections.push({ ...rest, authPeriod: authPeriod });
    if ([OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE, OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT].includes(moduleName)) {
      if(occInspectedSpaceDTOList?.length){
        occInspectedSpaceDTOList.forEach((space) => {
          const { occInspectedSpaceElementDTOList, ...spaceData } = space;
          occInspectedSpaces.push(spaceData);
          inspectedSpaceElements.push(...occInspectedSpaceElementDTOList);
        });
      }
    }
  });

  if (moduleName === OFFLINE_MODULE_NAME.INSPECTIONS) {
    return inspections;
  } else if (moduleName === OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE) {
    return occInspectedSpaces;
  } else {
    return inspectedSpaceElements;
  }
}

const getInspectionsListSyncService = async (searchParams = '', moduleName) => {
  return await inspectionAxiosInstance
    .get(microServices.inspections + 'dispatch/all' + searchParams)
    .then(async response => {
      return await getInspectionsData(response.data, moduleName);
    })
    .catch(async error => {
      return await Database.getInspectionOfflineListData(
        OFFLINE_MODULE_NAME.INSPECTIONS,
        OFFLINE_TABLE_NAME.INSPECTIONS,
        searchParams,
      );
      throw error;
    });
}

const uploadInspectionSyncService = async (payload,municipality_code,auth_period , inspectionId) => {
  // console.log("PAYLOAD *********",payload);
  return await inspectionAxiosInstance
  .post(microServices.inspections + `/uploadInspection?municipality_code=${municipality_code}&auth_period=${auth_period}`,payload)
  .then(async response => {
    console.log("RESPAINE *******",response.data);
    if(response.status === 200){
         if(!Helper.isUndefined(inspectionId)){
          await AsyncStorage.setItem('uploadedInspectionId',(response?.data?.inspectionId).toString()); 
         }
         await Database.updateInspectionData(response.data);
         await Database.updateInspectedSpacesData(response.data);
         await Database.updateInspectedSpaceElementData(response.data);
    }
    return;
  })
  .catch(async error => {
    console.log("ERROR ******",error);
    Helper.showFlashMessage('danger','There is problem in synching inspection/s, Please try after some time and/or contact administrator.')
    console.log("ERROR **********",error.response);
  });
}

const uploadInspectionPhotosSyncService = async (payload , municipality_code , auth_period) => {
  console.log("PAYLOAD *************************** UPLOAD PHOTO *",payload);
  return await inspectionAxiosInstance
  .post(microServices.inspections + `/dispatch/uploadBlob?municipality_code=${municipality_code}&auth_period=${auth_period}`,payload)
  .then(async response => {
    if(response?.status === 200){
      await Database.updateBlobByteData(payload?.inspectionId);
    }
    return;
  })
  .catch(async error => {
    console.log("ERROR ********************",error.response);
  });
}


export {
  getInspectionDeterminationSyncService,
  getInspectionCauseSyncService,
  getInspectionIntensityClassesSyncService,
  getInspectionChecklistsSyncService,
  getCodeElementsSyncService,
  getCodeElementGuideSyncService,
  getChecklistSpaceTypesSyncService,
  getInspectionsListSyncService,
  uploadInspectionSyncService,
  uploadInspectionPhotosSyncService,
  getChecklistSpaceTypeElementsSyncService
};