import {Helper} from '../utils/Helper';
import {SyncUtils} from '../utils/syncUtils';
import Database from '../database/Database';
import {InspectionDBUtils} from '../utils/DBUtils/InspectionDBUtils';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../config/config';
import {InspectionSpacesDBUtils} from '../utils/DBUtils/InspectionSpacesDBUtils';
import {Sync} from '../utils/Sync';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {checkHasInternet} from '../utils/functions/checkHasInternet';
import store from '../store/configureStore';

const offlineAddInspectionPhoto = async (
  imageData,
  inspectionId,
  userInfo = null,
  inspectedSpaceElementId = null,
  isFromFinding = false,
) => {
  imageData = {
    ...imageData,
    created_at: await Helper.getCurrentUTCDateTime(),
  };
  let values = await InspectionDBUtils.addInspectionPhotoRequest(
    imageData,
    inspectionId,
    userInfo,
  );

  let selectFields = await SyncUtils.getOccInspectionBlobBytes();
  return await Database.insertRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
    values,
    selectFields,
  )
    .then(async insertResponse => {
      if (insertResponse.length) {
        if (isFromFinding) {
          offlineAddInspectionPhotoAgainstOrdinance(
            values.bytesId,
            inspectedSpaceElementId,
          );
        }
        return true;
      }
    })
    .catch(function (error) {
      return 0;
    });
};

const offlineAddInspectionPhotoAgainstOrdinance = async (
  bytesId,
  inspectedSpaceElementId,
) => {
  let findingsData = {
    bytesId: bytesId,
    inspectedSpaceElementId: inspectedSpaceElementId,
  };
  let values = await InspectionDBUtils.preparePayloadToAddFindingPhoto(
    findingsData,
  );

  let selectFields = await SyncUtils.getOccInspectionElementPhotoDoc();

  return await Database.insertRecord(
    OFFLINE_TABLE_NAME.INSPECTION_INSPECTED_ELEMENT_PHOTO_DOC,
    values,
    selectFields,
  )
    .then(async insertResponse => {
      if (insertResponse.length) {
        return true;
      }
    })
    .catch(function (error) {
      return 0;
    });
};

const getInspectionPhotoAgainstOrdinance = async inspectedSpaceElementId => {
  let where;
  let join;
  where = `(occinspectedspaceelementphotodoc.deactivatedts='' OR occinspectedspaceelementphotodoc.deactivatedts IS NULL) AND inspectedSpaceElementId='${inspectedSpaceElementId}'`;
  join =
    'JOIN blobbytes ON blobbytes.bytesId = occinspectedspaceelementphotodoc.bytesId';

  return await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTION_INSPECTED_ELEMENT_PHOTO_DOC,
    '*',
    where,
    join,
  )
    .then(async response => {
      if (response.length) {
        return await InspectionDBUtils.prepareInspectionOverallPhotosObject(
          response,
        );
      }
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const offlineDeletePhoto = async (bytesId, tableName) => {
  let nameValueList = [];
  nameValueList.push(
    {
      name: 'deactivatedts',
      value: Helper.getCurrentUTCDateTime(),
    },
    {
      name: 'lastupdatedts',
      value: Helper.getCurrentUTCDateTime(),
    },
  );
  let where = 'bytesId=' + `'${bytesId}'`;
  await Database.updateRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
    nameValueList,
    where,
  )
    .then(async => {
      return 1;
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const offlineUpdateInspectionDetails = async (
  inspectionId,
  inspectionDetails,
  OFFLINE_TABLE_NAME,
) => {
  let nameValueList = [];
  nameValueList.push(
    {
      name: 'causeId',
      value: inspectionDetails.causeId,
    },
    {
      name: 'effectiveDate',
      value: inspectionDetails.effectiveDate,
    },
    {
      name: 'timeStart',
      value: inspectionDetails.timeStart,
    },
    {
      name: 'generalComments',
      value: inspectionDetails.generalComments,
    },
    {
      name: 'maxOccupantsAllowed',
      value: inspectionDetails.maxOccupantsAllowed,
    },
    {
      name: 'numBedRooms',
      value: inspectionDetails.numBedRooms,
    },
    {
      name: 'numBathRooms',
      value: inspectionDetails.numBathRooms,
    },
    {
      name: 'notesPreInspection',
      value: inspectionDetails.notesPreInspection,
    },
    {
      name: 'dispatchNotes',
      value: inspectionDetails.dispatchNotes,
    },
  );
  let where = 'inspectionId=' + `'${inspectionId}'`;
  return await Database.updateRecord(OFFLINE_TABLE_NAME, nameValueList, where)
    .then(async => {
      return 1;
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const offlineOrdinancePassFail = async (
  isBulkAction = false,
  inspectedSpacesElementIds,
  action,
  userInfo,
) => {
  let nameValueList = [];
  let where = '';
  if (action === 'inspectionPassed') {
    nameValueList.push(
      {name: 'lastInspectedByUserId', value: userInfo?.userId},
      {
        name: 'lastInspectedTS',
        value: Helper.getCurrentUTCDateTime(),
      },
      {name: 'complianceGrantedByUserId', value: userInfo?.userId},
      {
        name: 'complianceGrantedTS',
        value: Helper.getCurrentUTCDateTime(),
      },
    );
  }
  if (action === 'inspectionViolated') {
    nameValueList.push(
      {name: 'lastInspectedByUserId', value: userInfo?.userId},
      {
        name: 'lastInspectedTS',
        value: Helper.getCurrentUTCDateTime(),
      },
      {name: 'complianceGrantedByUserId', value: ''},
      {name: 'complianceGrantedTS', value: ''},
    );
  }

  if (action === 'inspectionNotInspected') {
    nameValueList.push(
      {name: 'lastInspectedByUserId', value: ''},
      {name: 'lastInspectedTS', value: ''},
      {name: 'complianceGrantedByUserId', value: ''},
      {name: 'complianceGrantedTS', value: ''},
    );
  }

  if (isBulkAction) {
    where = 'inspectedSpaceElementId IN' + `(${inspectedSpacesElementIds})`;
  } else {
    where = 'inspectedSpaceElementId=' + `'${inspectedSpacesElementIds}'`;
  }
  return await Database.updateRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
    nameValueList,
    where,
  )
    .then(async => {
      return 1;
    })
    .catch(function (error) {
      console.log(
        'offline mode spaces inspection status update in error...',
        error,
      );
      return 0;
    });
};

const getAvailableSpacesAgainstInspection = async (
  inspectionSpaceIds,
  occChecklistId,
) => {
  let muniCode = await AsyncStorage.getItem('municipality_code');
  let where;
  if (inspectionSpaceIds?.length > 0) {
    where =
      'checklistId=' +
      `${occChecklistId}` +
      ' AND checklistSpaceTypeId NOT IN ' +
      `(${inspectionSpaceIds})` +
      `AND deactivatedts='' OR deactivatedts IS NULL AND muniCode=${muniCode}`;
  } else {
    where =
      'checklistId=' +
      `${occChecklistId}` +
      ` AND occchecklistspacetype.deactivatedts='' OR occchecklistspacetype.deactivatedts IS NULL AND muniCode=${muniCode}`;
  }

  let orderBy = ` occchecklistspacetype.spaceTitle ASC `;


  return await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE,
    '*',
    where, 
    "",
    orderBy
  )
    .then(async response => {
      if (response.length) {
        return await InspectionSpacesDBUtils.prepareAddSpacesListObject(
          response,
        );
      }
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const offlineDeleteSpace = async (inspectedSpaceId, userInfo) => {
  let nameValueListForInspectedSpaces = [];
  let nameValueListForInspectedSpaceElement = [];

  nameValueListForInspectedSpaces.push(
    {name: 'deactivatedts', value: Helper.getCurrentUTCDateTime()},
    {
      name: 'inspectedSpaceDeactivatedByUserId',
      value: parseInt(userInfo?.userId),
    },
  );

  nameValueListForInspectedSpaceElement.push({
    name: 'deactivatedts',
    value: Helper.getCurrentUTCDateTime(),
  });

  let where = 'inspectedSpaceId=' + `'${inspectedSpaceId}'`;
  return await Database.updateRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE,
    nameValueListForInspectedSpaces,
    where,
  )
    .then(async () => {
      await Database.updateRecord(
        OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
        nameValueListForInspectedSpaceElement,
        where,
      );
      return 1;
    })
    .catch(function (error) {
      console.log('ERROR ******************', error);
      console.log('offline mode inspected space update in error...', error);
      return 0;
    });
};

const offlineAddSpace = async (
  checklistSpaceTypeId,
  inspectionId,
  governingCodeSourceId,
  userInfo,
) => {
  let values = await InspectionSpacesDBUtils.prepareAddSpaceObject({
    checklistSpaceTypeId: checklistSpaceTypeId,
    inspectionId: inspectionId,
    userId: userInfo?.userId,
  });

  let selectFields = await SyncUtils.getOccInspectedSpacesFields();
  return await Database.insertRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE,
    values,
    selectFields,
  )
    .then(async response => {
      if (response.length) {
        return Database.getOrdinanceAndCategoriesDetails(
          OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS,
          OFFLINE_TABLE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS,
          {
            governingCodeSourceId: governingCodeSourceId,
            checklistSpaceTypeId: checklistSpaceTypeId,
          },
        ).then(async response => {
          return await offlineAddSpaceElement(
            response?.ordinancesList,
            values?.inspectedSpaceId,
            checklistSpaceTypeId,
          );
        });
      }
    })
    .catch(function (error) {
      return 0;
    });
};

const offlineAddSpaceElement = async (
  response,
  inspectedSpaceId,
  checklistSpaceTypeId,
) => {
  let values = await InspectionSpacesDBUtils.prepareAddSpaceElementObject(
    response,
    checklistSpaceTypeId,
    inspectedSpaceId,
  );
  let selectFields = await SyncUtils.getOCcInspectedSpaceElementFields();
  if (response?.length > 0 && !Helper.isUndefined(response)) {
    return await Database.insertList(
      OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
      values,
      selectFields,
    )
      .then(response => {
        if (response?.length > 0) {
          return {inspectedSpaceId: inspectedSpaceId};
        }
      })
      .catch(error => {
        console.log(error);
      });
  }
};

const offlineAddNewInspection = async data => {
  let values = await InspectionDBUtils.prepareAddSNewInspectionObject(data);
  let selectFields = await SyncUtils.getOccInspectionsFields();
  return await Database.insertRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS,
    values,
    selectFields,
  )
    .then(async response => {
      if (response.length) {
        return 1;
      }
    })
    .catch(function (error) {
      return 0;
    });
};

const getSpacesCountAgainstCheckListId = async occChecklistId => {
  let where;
  where =
    'checklistId=' +
    occChecklistId +
    " AND occchecklistspacetype.deactivatedts='' OR occchecklistspacetype.deactivatedts IS NULL";

  return await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE,
    '*',
    where,
  )
    .then(async response => {
      if (response.length) {
        return response.length;
      }
    })
    .catch(function (error) {
      console.log('Getting error while counting the spaces...', error);
      return 0;
    });
};

const offlineUploadInspection = async (inspectionId, data, isChecked) => {
  let nameValueList = [];
  if (isChecked) {
    nameValueList.push(
      {name: 'isInspectionDone', value: 1},
      {name: 'generalComments', value: data?.comment},
    );
  } else {
    nameValueList.push(
      {name: 'determinationDetId', value: data?.determinationDetId},
      {name: 'isInspectionDone', value: 1},
      {name: 'generalComments', value: data?.comment},
      {name: 'determinationTS', value: Helper.getCurrentUTCDateTime()},
      {name: 'determinationStatus', value: data?.determinationStatus},
    );
  }
  let where = 'inspectionId=' + `'${inspectionId}'`;
  return await Database.updateRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS,
    nameValueList,
    where,
  )
    .then(async response => {
      if (response) {
        return 1;
      } else {
        Helper.showFlashMessage('danger', 'Something went wrong');
      }
    })
    .catch(function (error) {
      console.log('offline mode upload inspection error...', error);
      return 0;
    });
};

const offlineAddFindingAgainstOrdinance = async (
  data,
  inspectedSpaceElementId,
) => {
  let nameValueList = [];
  nameValueList.push(
    {name: 'notes', value: data.notes},
    {
      name: 'failureSeverityIntensityClassId',
      value: data.failureSeverityIntensityClassId,
    },
  );
  let where = 'inspectedSpaceElementId=' + `'${inspectedSpaceElementId}'`;
  return await Database.updateRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
    nameValueList,
    where,
  )
    .then(async () => {
      await Database.updateRecord(
        OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
        nameValueList,
        where,
      );
      return 1;
    })
    .catch(function (error) {
      console.log('offline mode inspected space update in error...', error);
      return 0;
    });
};

const updateSortedImages = async (position, inspectionId, bytesId) => {
  let nameValueList = [];
  nameValueList.push({name: 'imageOrder', value: position});
  let where =
    'inspectionId=' + `'${inspectionId}' AND bytesId=` + `'${bytesId}'`;
  return await Database.updateRecord(
    OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
    nameValueList,
    where,
  ).catch(function (error) {
    console.log('offline mode inspected space update in error...', error);
    return 0;
  });
};

const offlineDeleteFindingsPhoto = async bytesId => {
  let nameValueList = [];
  nameValueList.push({
    name: 'deactivatedts',
    value: Helper.getCurrentCSTDateTime('YYYY-MM-DD HH:mm:ss'),
  });
  let where = 'bytesId=' + `'${bytesId}'`;
  return await Database.updateRecord(
    OFFLINE_TABLE_NAME.INSPECTION_INSPECTED_ELEMENT_PHOTO_DOC,
    nameValueList,
    where,
  )
    .then(async => {
      return 1;
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const getPhotosDataAvailableForSync = async () => {
  const isSignedIn = store.getState().auth.isSignedIn;
  if ((await checkHasInternet()) && isSignedIn) {
    let uploadedInspectionId = await AsyncStorage.getItem(
      'uploadedInspectionId',
    );
    let where;
    let join;

    if (!Helper.isUndefined(uploadedInspectionId)) {
      where = ` occinspection.inspectionId='${uploadedInspectionId}' AND occinspection.isSync=${1} AND occinspection.isInspectionDone=${1} AND blobbytes.isSync = 1 AND (blobbytes.deactivatedts='' OR blobbytes.deactivatedts IS NULL)`;
    } else {
      where = `occinspection.isSync=${1} AND occinspection.isInspectionDone=${1} AND blobbytes.isSync = 1 AND (blobbytes.deactivatedts='' OR blobbytes.deactivatedts IS NULL)`;
    }

    join =
      'JOIN occinspection ON blobbytes.inspectionId = occinspection.inspectionId';

    return await Database.getRecords(
      OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
      '*',
      where,
      join,
    )
      .then(async response => {
        if (response.length) {
          let imageData =
            await InspectionDBUtils.prepareInspectionOverallPhotosObjectForSync(
              response,
            );
          Sync.syncInspectionModulesPhotosDataDownToUp(
            imageData,
            uploadedInspectionId,
          );
        }
      })
      .catch(function (error) {
        console.log('offline mode appointment update in error...', error);
        return 0;
      });
  }
};

const getInspectedSpacesAgainstInspection = async (
  inspectionId,
  inspectedSpaceIds,
) => {
  let where;
  where = `occInspectionId='${inspectionId}' AND (occinspectedspaces.deactivatedts='' OR occinspectedspaces.deactivatedts IS NULL)`;

  return await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE,
    'occChecklistSpaceTypeId',
    where,
  )
    .then(async response => {
      if (response?.length) {
        return response.map(element => {
          return `'${element.occChecklistSpaceTypeId}'`;
        });
      } else {
        return [];
      }
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const getPhotoCountAgainstInspection = async (
  inspectionId,
  ordinancePhotosList,
) => {
  let where = '';
  let join;
  let whereQuery;

  let photosIds;
  if (ordinancePhotosList?.length > 0) {
    const quotedElements = ordinancePhotosList.map(
      photo => `'${photo.bytesId}'`,
    );
    photosIds = quotedElements.join(', ');
  }

  whereQuery = `inspectionId='${inspectionId}' AND blobbytes.deactivatedts='' OR blobbytes.deactivatedts IS NULL`;

  if (ordinancePhotosList?.length > 0) {
    where += ' bytesId NOT IN ' + `(${photosIds}) AND ` + whereQuery;
  } else {
    where += whereQuery;
  }

  console.log('WHERE *******', where);

  return await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
    '*',
    where,
    join,
  )
    .then(async response => {
      if (response?.length) {
        return response?.length;
      } else {
        return 0;
      }
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const getInspectedSpacesCountAgainstInspection = async inspectionId => {
  let where;
  let join;

  where = `occInspectionId='${inspectionId}' AND occinspectedspaces.deactivatedts='' OR occinspectedspaces.deactivatedts IS NULL`;

  return await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE,
    '*',
    where,
    join,
  )
    .then(async response => {
      console.log('RESPONSE LENGTH ********', response?.length);
      if (response?.length) {
        return response?.length;
      } else {
        return 0;
      }
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const offlineUpdateInspectionStartDate = async (
  inspectionId,
  inspectionStartDate,
  OFFLINE_TABLE_NAME,
) => {
  let nameValueList = [];
  nameValueList.push(
    {
      name: 'effectiveDate',
      value: inspectionStartDate,
    },
    {
      name: 'isInspectionStarted',
      value: 1,
    },
  );

  let where = 'inspectionId=' + `'${inspectionId}'`;

  return await Database.updateRecord(OFFLINE_TABLE_NAME, nameValueList, where)
    .then(async => {
      return 1;
    })
    .catch(function (error) {
      console.log('offline mode appointment update in error...', error);
      return 0;
    });
};

const deleteInitiatedInspection = async inspectionId => {
  let where = 'inspectionId=' + `'${inspectionId}'`;
  return await Database.deleteRecords(OFFLINE_TABLE_NAME.INSPECTIONS, where)
    .then(async () => {
      return 1;
    })
    .catch(function (error) {
      console.log('delete inspection error...', error);
      return 0;
    });
};

export const offlineService = {
  offlineAddInspectionPhoto,
  offlineDeletePhoto,
  offlineOrdinancePassFail,
  getAvailableSpacesAgainstInspection,
  offlineDeleteSpace,
  offlineUpdateInspectionDetails,
  offlineAddSpace,
  offlineAddNewInspection,
  offlineAddInspectionPhotoAgainstOrdinance,
  getInspectionPhotoAgainstOrdinance,
  getSpacesCountAgainstCheckListId,
  offlineUploadInspection,
  offlineAddFindingAgainstOrdinance,
  updateSortedImages,
  offlineDeleteFindingsPhoto,
  getPhotosDataAvailableForSync,
  getInspectedSpacesAgainstInspection,
  getPhotoCountAgainstInspection,
  getInspectedSpacesCountAgainstInspection,
  offlineUpdateInspectionStartDate,
  deleteInitiatedInspection,
};
