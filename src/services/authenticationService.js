import axiosInstance from '../config/authAxios.config';
import {Helper} from '../utils/Helper';

const login = async payload => {
  return axiosInstance
    .post('/login/user', payload)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      Helper.showFlashMessage('danger', 'Something went wrong');
      throw error;
    });
};

const getAllUserSessions = async () => {
  return axiosInstance
    .post('/login/authPeriod')
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      Helper.showFlashMessage('danger', 'Something went wrong');
      throw error;
    });
};

export const Authentication = {
  login,
  getAllUserSessions,
};
