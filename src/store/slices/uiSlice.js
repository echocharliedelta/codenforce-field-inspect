import {createSlice} from '@reduxjs/toolkit';

const uiSlice = createSlice({
  name: 'UI',
  initialState: {
    isLoading: false,
  },
  reducers: {
    uiStartLoading: (state, action) => {
      return {...state, isLoading: true};
    },
    uiStopLoading: (state, action) => {
      return {...state, isLoading: false};
    },
  },
});
export const {uiStartLoading, uiStopLoading} = uiSlice.actions;
export const selectUiLoading = state => state.ui.isLoading;

export default uiSlice.reducer;
