import {createSlice} from '@reduxjs/toolkit';

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isSignedIn: false,
    token: null,
    userSessions: [],
    userInfo: {},
    selectIndex: 0,
  },
  reducers: {
    signInUser: (state, action) => {
      return {...state, isSignedIn: true, token: action.payload.token};
    },
    signOutUser: (state, action) => {
      return {...state, isSignedIn: false, token: null, userSessions: []};
    },
    setUserSessions: (state, action) => {
      return {...state, userSessions: action.payload};
    },
    setUserInformation: (state, action) => {
      return {...state, userInfo: action.payload};
    },
    setSelectedIndex: (state, action) => {
      return {...state, selectIndex: action.payload};
    },
  },
});
export const {
  signInUser,
  signOutUser,
  setUserSessions,
  setUserInformation,
  setSelectedIndex,
} = authSlice.actions;
export const userSessions = state => state.auth.userSessions;

export default authSlice.reducer;
