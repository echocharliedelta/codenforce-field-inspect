import {uiStartLoading, uiStopLoading} from '../slices/uiSlice';
import {Authentication} from '../../services/authenticationService';
import {setUserSessions, signInUser} from '../slices/authSlice';
import {Helper} from '../../utils/Helper';
import LangStrings from '../../utils/langStrings';

export const authSignIn = payload => {
  return async (dispatch, getState) => {
    dispatch(uiStartLoading());
    return await Authentication.login(payload)
      .then(async response => {
        if (response.status === 200 && !Helper.isUndefined(response?.data)) {
          dispatch(signInUser({token: response.data.token}));
          dispatch(uiStopLoading());
          return true;
        } else {
          Helper.showFlashMessage(
            'danger',
            LangStrings.LBL_INCORRECT_USERNAME_PASSWORD,
          );
          dispatch(uiStopLoading());
          return false;
        }
      })
      .catch(error => {
        dispatch(uiStopLoading());
        Helper.showFlashMessage(
          'danger',
          LangStrings.LBL_INCORRECT_USERNAME_PASSWORD,
        );
        return error;
      });
  };
};

export const getUserSessions = () => {
  return async (dispatch, getState) => {
    dispatch(uiStartLoading());
    return await Authentication.getAllUserSessions()
      .then(async response => {
        if (response.status === 200 && !Helper.isUndefined(response?.data)) {
          dispatch(setUserSessions(response.data));
          dispatch(uiStopLoading());
          return true;
        }
      })
      .catch(error => {
        dispatch(uiStopLoading());
        Helper.showFlashMessage('danger', LangStrings.LBL_SOMETHING_WENT_WRONG);
        return error;
      });
  };
};

export const authSignOut = () => {
  return async (dispatch, getState) => {};
};
