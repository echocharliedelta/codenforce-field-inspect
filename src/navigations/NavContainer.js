import React from 'react';
import {NavigationContainer, useTheme} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  SignInScreen,
  SessionScreen,
  InspectionScreen,
  InspectionDetailScreen,
  AddInspectionScreen,
  SpacesScreen,
  OrdinancesScreen,
  OverallPhotosScreen,
  AddSpaceScreen,
} from '../screens';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {dynamicCommonStyles} from '../assets/styles/CommonStyles';
import {useDispatch} from 'react-redux';
import {signOutUser} from '../store/slices/authSlice';
import Buttons from '../components/atoms/Buttons/Buttons';
import {Platform} from 'react-native';
const defaultScreenOptions = {
  gesturesEnabled: false,
  headerTitleAlign: 'left',
};

const Tab = createBottomTabNavigator();
const App_Stack = createNativeStackNavigator();
const Auth_Stack = createNativeStackNavigator();

const Auth = () => {
  return (
    <Auth_Stack.Navigator
      initialRouteName="SignIn"
      screenOptions={{
        headerShown: false,
      }}>
      <Auth_Stack.Screen name={'SignIn'} component={SignInScreen} />
    </Auth_Stack.Navigator>
  );
};

const screenOptions = ({route}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const Colors = useTheme().colors;
  return {
    tabBarStyle: {
      position: 'absolute',
      bottom: 0,
      paddingBottom: 5,
      paddingRight: 12,
      backgroundColor: Colors.BG_BOTTOM_NAVIGATION,
    },
    tabBarLabelStyle: {
      fontSize: 12,
      color: '#CDE161',
    },
    tabBarActiveBackgroundColor: 'rgba(255, 255, 255, 0.05)',
    headerShown: true,
    lazy: true,
    ...(route.name === 'Sessions' || route.name === 'Add Inspection'
      ? {tabBarStyle: {display: 'none'}}
      : {}),
  };
};

const BottomTabNavigator = () => {
  const commonStyle = dynamicCommonStyles();
  const dispatch = useDispatch();
  return (
    <Tab.Navigator initialRouteName="Sessions" screenOptions={screenOptions}>
      <Tab.Screen
        name="Sessions"
        component={SessionScreen}
        options={{
          headerShadowVisible: false,
          headerTitleAlign: 'left',
          headerStyle: {...commonStyle.hederStyle},
          headerTitleStyle: {...commonStyle.hederTitleStyle},
          tabBarLabel: 'Session',
          tabBarIcon: () => (
            <Icon name={'swap-horizontal'} size={27} color={'#CDE161'} />
          ),
        }}
      />

      <Tab.Screen
        name="Inspection"
        component={InspectionScreen}
        options={{
          headerTitle: 'Inspections',
          headerTitleAlign: 'left',
          headerShadowVisible: false,
          headerStyle: {...commonStyle.hederStyle},
          headerTitleStyle: {...commonStyle.hederTitleStyle},
          tabBarLabel: 'Inspections',
          tabBarIcon: () => (
            <Icon name={'home-search'} size={27} color={'#CDE161'} />
          ),
          headerRight: () => (
            <Buttons
              type={'clear'}
              title={'Logout'}
              titleStyle={[commonStyle.ftSize16, commonStyle.ftWeight700]}
              onPress={() => {
                dispatch(signOutUser());
              }}
            />
          ),
        }}
      />

      <Tab.Screen
        name="Add Inspection"
        component={AddInspectionScreen}
        options={{
          headerShadowVisible: false,
          headerTitleAlign: 'left',
          headerStyle: {...commonStyle.hederStyle},
          headerTitleStyle: {...commonStyle.hederTitleStyle},
          headerTitle: 'Inspections',
          tabBarIcon: () => (
            <Icon name={'home-plus'} size={27} color={'#CDE161'} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const AppStack = () => {
  const commonStyle = dynamicCommonStyles();

  return (
    <App_Stack.Navigator
      initialRouteName="BottomTabNavigator"
      screenOptions={{
        ...defaultScreenOptions,
        headerShown: false,
        headerShadowVisible: false,
        // headerTitleAlign:'center',
        headerTitle: '',
        headerStyle: {...commonStyle.hederStyle},
        headerTitleStyle: {...commonStyle.hederTitleStyle},
      }}>
      <App_Stack.Screen
        name="BottomTabNavigator"
        component={BottomTabNavigator}
        scr
      />
      <App_Stack.Screen
        name="InspectionDetail"
        component={InspectionDetailScreen}
        options={{
          // headerTitle: '906 RITA DR',
          headerTitleAlign: 'left',

          headerShown: true,
          headerTintColor: '#00B4EA',
          headerStyle: {...commonStyle.hederStyle},
          headerTitleStyle: {...commonStyle.hederTitleStyle},
        }}
      />
      <App_Stack.Screen
        name="Spaces"
        component={SpacesScreen}
        options={{
          // headerTitle: '906 RITA DR',
          headerTintColor: '#00B4EA',
          headerStyle: {...commonStyle.hederStyle},
          headerTitleStyle: {...commonStyle.hederTitleStyle},
          headerShown: true,
          // title: 'Sign Up',
        }}
      />
      <App_Stack.Screen
        name="Ordinances"
        component={OrdinancesScreen}
        options={{
          // headerBackVisible: false,
          headerTintColor: '#00B4EA',
          headerShown: true,
        }}
      />
      <App_Stack.Screen
        name="OverallPhotos"
        component={OverallPhotosScreen}
        options={{
          headerTitleAlign: 'left',

          headerTintColor: '#00B4EA',
          headerShown: true,
          // headerTitle: '906 RITA DR',
        }}
      />
      <App_Stack.Screen
        name="AddSpaceScreen"
        component={AddSpaceScreen}
        options={{
          headerBackTitle: Platform.OS === 'ios' && 'Back',
          headerTintColor: '#00B4EA',
          headerShown: true,
          headerTitle: 'Add Space',
        }}
      />
    </App_Stack.Navigator>
  );
};

const NavContainer = ({navRef, signedIn = false, appTheme}) => {
  return (
    <NavigationContainer theme={appTheme} ref={navRef}>
      {signedIn ? <AppStack /> : <Auth />}
    </NavigationContainer>
  );
};
export default NavContainer;
