import React from 'react';
import {Input} from '@rneui/themed';

const TextInput = props => {
  return <Input {...props} />;
};

export default TextInput;
