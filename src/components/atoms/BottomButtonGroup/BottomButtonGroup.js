import React from 'react';
import {ButtonGroup as RNButtonGroup} from '@rneui/themed';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';

const BottomButtonGroup = ({
  isDisabled,
  children,
  onPress,
  status,
  isSpacesDone = false,
  isSpacesInProgress = false,
}) => {
  const commonStyle = dynamicCommonStyles();

  const handleButtonPress = index => {
    // Call the corresponding function from onPress array based on the button index
    if (onPress && onPress[index] && typeof onPress[index] === 'function') {
      onPress[index]();
    }
  };

  return (
    <RNButtonGroup
      selectedIndex={status}
      containerStyle={[
        isSpacesDone
          ? commonStyle.DoneSpacesCardBottomTabs
          : commonStyle.SpacesCardBottomTabs,
      ]}
      selectedButtonStyle={[
        isSpacesDone ? commonStyle.bgBlue : commonStyle.bgTextLight,
        isSpacesInProgress && commonStyle.bgButtonUploadInsp,
      ]}
      textStyle={commonStyle.colorWhite}
      buttons={children}
      onPress={handleButtonPress}
    />
  );
};

export default BottomButtonGroup;
