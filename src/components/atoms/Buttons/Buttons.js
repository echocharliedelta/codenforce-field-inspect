import React from 'react';
import {Button} from '@rneui/themed';

const Buttons = props => {
  return (
    <Button
      {...props}
      title={props.title ?? 'Button'}
      onPress={props.onPress}
    />
  );
};

export default Buttons;
