import React from 'react';
import {Text, View} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {dynamicStyles} from './Style';
import {Input} from '@rneui/themed';

const DateTimePicker = props => {
  const styles = dynamicStyles();
  return (
    <View
      style={[styles.textInput, props.disabled && styles.disabledTextInput]}>
      <Text style={styles.textInputLabel}>{props.textInputLabel}</Text>
      <Input
        {...props}
        onPressIn={props.toggleDatePicker}
        showSoftInputOnFocus={false}
        disabled={props.disabled}
      />
      <DateTimePickerModal
        onPress={props.toggleDatePicker}
        isVisible={props.isVisible && props.disabled !== 1}
        mode={props.mode}
        onConfirm={props.onConfirm}
        onCancel={props.toggleDatePicker}
      />
    </View>
  );
};

export default DateTimePicker;
