import {StyleSheet} from 'react-native';
export const dynamicStyles = () => {
  return StyleSheet.create({
    LoadingStyle: {
      marginLeft: 10,
      fontSize: 16,
    },
    loader: {
      height: 70,
      backgroundColor: 'white',
      marginHorizontal: 50,
      borderRadius: 5,
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 20,
    },
    container: {
      position: 'absolute',
      zIndex: 10,
      backgroundColor: 'rgba(0,0,0,0.5)',
      justifyContent: 'center',
    },
  });
};
