import React from 'react';
import {useWindowDimensions, View, Text, ActivityIndicator} from 'react-native';
import {dynamicStyles} from './Style';
import LangStrings from '../../../utils/langStrings';

const Loader = prop => {
  const styles = dynamicStyles();
  const {width, height} = useWindowDimensions();
  return (
    prop?.isLoading && (
      <View style={[styles.container, {height, width}]}>
        <View style={styles.loader}>
          <ActivityIndicator size="large" color={'blue'} />
          <Text style={styles.LoadingStyle}>{LangStrings.LBL_LOADING}</Text>
        </View>
      </View>
    )
  );
};

export default Loader;
