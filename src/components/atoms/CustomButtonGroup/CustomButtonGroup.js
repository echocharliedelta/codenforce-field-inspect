import React, {useState} from 'react';
import {ButtonGroup as RNButtonGroup} from '@rneui/themed';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';

const CustomButtonGroup = ({
  disabled,
  children,
  status,
  changeInspectionOrdinanceStatus,
  ...props
}) => {
  const [selectedIndex, setSelectedIndex] = useState(status);
  const commonStyle = dynamicCommonStyles();
  return (
    <RNButtonGroup
      disabled={disabled}
      disabledSelectedStyle={commonStyle.bgTextarea}
      innerBorderStyle={commonStyle.textBlue}
      selectedButtonStyle={[commonStyle.bgBlue, commonStyle.colorWhite]}
      containerStyle={[commonStyle.ordinancesCardContainer]}
      textStyle={[
        commonStyle.textBlue,
        commonStyle.ftWeight700,
        commonStyle.ftSize11,
        commonStyle.ftSFpro,
      ]}
      buttons={children}
      selectedIndex={selectedIndex}
      onPress={value => {
        setSelectedIndex(value);
        changeInspectionOrdinanceStatus(value);
      }}
    />
  );
};

export default CustomButtonGroup;
