import React from 'react';
import {CheckBox} from '@rneui/themed';

const CustomCheckbox = props => {
  return (
    <CheckBox
      {...props}
      onPress={props.onPress}
      checked={props.checked}
      containerStyle={props.containerStyle}
    />
  );
};

export default CustomCheckbox;
