import {StyleSheet} from 'react-native';
export const dynamicStyles = () => {
  return StyleSheet.create({
    container: {
      width: '100%',
      height: 30,
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'baseline',
      backgroundColor: '#F06A30',
      flexDirection: 'row',
    },
    text: {
      color: 'white',
      fontSize: 12,
      padding: 5,
    },
  });
};
