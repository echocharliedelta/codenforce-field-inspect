import React, {useState, useEffect} from 'react';
import {useNetInfo} from '@react-native-community/netinfo';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {dynamicStyles} from './Style';
import LangStrings from '../../../utils/langStrings';

const NetInfo = () => {
  const styles = dynamicStyles();
  const [mounted, setMounted] = useState(false);
  const NetWorkInfo = useNetInfo();

  useEffect(() => {
    setMounted(!NetWorkInfo.isConnected);
  }, [NetWorkInfo]);
  if (!mounted) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Text numberOfLines={1} style={styles.text}>
        {LangStrings.LBL_NO_INTERNET}
      </Text>
      <Icon name="alert-triangle" size={12} color="white" />
    </View>
  );
};

export default NetInfo;
