import React from 'react';
import {Text, View} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import {dynamicStyles} from './Style';

const Dropdowns = ({
  title,
  dropdownPosition,
  data,
  currentCauseId,
  onchange,
  disabled = false,
  placeholder = 'Select item',
}) => {
  const commonStyle = dynamicCommonStyles();
  const styles = dynamicStyles();

  return (
    <View style={[styles.textInput, disabled && styles.disabledTextInput]}>
      <Text style={styles.textInputLabel}>{title}</Text>
      <Dropdown
        disable={disabled}
        dropdownPosition={dropdownPosition}
        activeColor={[commonStyle.bgBlue]}
        style={[commonStyle.pdB10]}
        containerStyle={[commonStyle.mL0, commonStyle.bgBottomSheet]}
        placeholderStyle={[
          commonStyle.mL17,
          commonStyle.textDark2,
          disabled && commonStyle.opacity50,
        ]}
        selectedTextStyle={[
          commonStyle.mL16,
          commonStyle.textInput,
          commonStyle.minH50,
          commonStyle.pdT5,
          disabled && commonStyle.opacity50,
        ]}
        data={data}
        search
        itemTextStyle={[commonStyle.ftSize17, commonStyle.textInput]}
        labelField="name"
        valueField="id"
        placeholder={placeholder}
        searchPlaceholder="Search..."
        value={currentCauseId}
        onChange={onchange}
        renderRightIcon={() => (
          <Icon
            name={'chevron-down'}
            size={25}
            style={[commonStyle.mR18, commonStyle.textMain]}
          />
        )}
      />
    </View>
  );
};

export default Dropdowns;
