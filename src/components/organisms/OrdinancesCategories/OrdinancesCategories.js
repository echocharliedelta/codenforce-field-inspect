import React from 'react';
import {View, FlatList, Text} from 'react-native';
import OrdinancesCategory from '../../molecules/OrdinancesCategory/OrdinancesCategory';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import Buttons from '../../atoms/Buttons/Buttons';
import Checkbox from '../../atoms/CustomCheckbox/CustomCheckbox';
import LangStrings from '../../../utils/langStrings';
import {InspectionStatus} from '../../../utils/constants';

const OrdinancesCategories = ({
  selectedIndex,
  tabs,
  onPress,
  onCheckboxClick,
  checkboxValue,
  withTools,
  batchPassedOrdinance,
  index,
}) => {
  const commonStyle = dynamicCommonStyles();
  const renderTab = ({item, index}) => (
    <OrdinancesCategory
      item={item.category}
      index={index}
      selectedIndex={selectedIndex}
      onPress={() => {
        onPress(index, item?.guideEntryId);
      }}
    />
  );

  return (
    <View style={[commonStyle.w140]}>
      <Text
        style={[
          commonStyle.mL10,
          commonStyle.textLight,
          commonStyle.mT19,
          commonStyle.pdB2,
          commonStyle.mB5,
          commonStyle.bBPrimary,
        ]}>
        {LangStrings.LBL_CATEGORIES}
      </Text>
      <FlatList
        data={tabs}
        renderItem={renderTab}
        keyExtractor={item => item?.guideEntryId}
        style={[commonStyle.bgCategory, commonStyle.F1, commonStyle.fGrow1]}
      />
      {withTools && (
        <View
          style={[commonStyle.bgCategory, commonStyle.F1, commonStyle.fGrow1]}>
          <Text
            style={[commonStyle.textLight, commonStyle.mL10, commonStyle.mB5]}>
            {LangStrings.LBL_TOOLS}
          </Text>
          <View style={[commonStyle.dFlex, commonStyle.alignItemsCenter]}>
            <Buttons
              title={LangStrings.LBL_BATCH_PASSED}
              type="outline"
              radius="44"
              buttonStyle={[
                commonStyle.mV5,
                commonStyle.bPrimary,
                {width: '100%'},
                // commonStyle.w125,
              ]}
              titleStyle={[commonStyle.ftSFpro, commonStyle.textBlue]}
              onPress={() => {
                batchPassedOrdinance(true, 'inspectionPassed');
              }}
              disabled={index === InspectionStatus.DONE}
            />
            <Buttons
              title={LangStrings.LBL_BATCH_VIOLATED}
              type="outline"
              radius="44"
              buttonStyle={[
                commonStyle.mV5,
                commonStyle.bPrimary,
                {width: '100%'},
                // commonStyle.w125,
              ]}
              titleStyle={[commonStyle.ftSFpro, commonStyle.textBlue]}
              onPress={() => {
                batchPassedOrdinance(true, 'inspectionViolated');
              }}
              disabled={index === InspectionStatus.DONE}
            />
            <Checkbox
              checked={index === InspectionStatus.DONE ? false : checkboxValue}
              title={LangStrings.LBL_AUTO_SHOW_FINDINGS}
              textStyle={[
                commonStyle.ftWeight500,
                commonStyle.textBlue,
                commonStyle.ftSize15,
                commonStyle.mL5,
              ]}
              containerStyle={[commonStyle.bgCategory]}
              onPress={onCheckboxClick}
              disabled={index === InspectionStatus.DONE}
              iconType="material-community"
              checkedIcon="checkbox-marked"
              uncheckedIcon={'checkbox-blank-outline'}
            />
          </View>
        </View>
      )}
    </View>
  );
};

export default OrdinancesCategories;
