import {View, Keyboard, KeyboardAvoidingView, Platform} from 'react-native';
import React, {useEffect, useState} from 'react';
import {dynamicStyles} from './Style';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import LangStrings from '../../../utils/langStrings';
import Buttons from '../../../components/atoms/Buttons/Buttons';
import {useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import CustomInput from '../CustomInput/CustomInput';
import {Helper} from '../../../utils/Helper';
import {authSignIn} from '../../../store/middleware/auth';
import {useNetInfo} from '@react-native-community/netinfo';
import {useIsFocused} from '@react-navigation/native';

const SignInForm = () => {
  const styles = dynamicStyles();
  const commonStyle = dynamicCommonStyles();
  const dispatch = useDispatch();
  const NetInfo = useNetInfo();
  const isFocused = useIsFocused();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [fieldErrors, setFieldErrors] = useState([]);
  const [isInternetConnected, setIsInternetConnected] = useState(null);

  useEffect(() => {
    if (isFocused) {
      setIsInternetConnected(NetInfo?.isConnected);
    }
  }, [NetInfo, isFocused]);

  const signIn = async () => {
    Keyboard.dismiss();
    if (isInternetConnected) {
      if (await validate()) {
        dispatch(
          authSignIn({username: username.trim(), password: password.trim()}),
        );
      }
    } else {
      Helper.showFlashMessage(
        'danger',
        LangStrings.LBL_LOGIN_INTERNET_CONNECTION_LOST,
      );
    }
  };

  const validate = async () => {
    const fieldErrors = {};
    let result = true;

    if (Helper.isNull(username)) {
      fieldErrors.username = LangStrings.LBL_USERNAME_REQUIRED;
      result = false;
    }

    if (Helper.isNull(password)) {
      fieldErrors.password = LangStrings.LBL_PASSWORD_IS_REQUIRED;
      result = false;
    }
    setFieldErrors(fieldErrors);
    return result;
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      enabled
      keyboardVerticalOffset={70}
      style={styles.signInForm}>
      <View>
        <CustomInput
          containerStyle={[commonStyle.pH0, commonStyle.h45, commonStyle.bR7]}
          inputContainerStyle={[commonStyle.bBTransparent, commonStyle.mL11]}
          inputStyle={[
            commonStyle.h25,
            commonStyle.pdT5,
            commonStyle.ftSize17,
            commonStyle.textInput,
          ]}
          placeholder={LangStrings.LBL_USERNAME_PLACEHOLDER}
          textInputLabel={LangStrings.LBL_USERNAME_LABEL}
          onChangeText={text => setUsername(text)}
          error={Helper.isUndefined(fieldErrors?.username)}
          renderErrorMessage={false}
          errorMessage={
            !Helper.isUndefined(fieldErrors?.username)
              ? fieldErrors?.username
              : ''
          }
        />
      </View>
      <View style={styles.passwordInput}>
        <CustomInput
          containerStyle={[commonStyle.pH0, commonStyle.h45]}
          inputContainerStyle={[commonStyle.bBTransparent, commonStyle.mL11]}
          inputStyle={[
            commonStyle.pdT5,
            commonStyle.h25,
            commonStyle.ftSize17,
            commonStyle.textInput,
          ]}
          rightIconContainerStyle={[
            commonStyle.loginTextInputIcon,
            commonStyle.pdL10,
          ]}
          rightIcon={
            <Icon
              name={showPassword ? 'eye-outline' : 'eye-off'}
              size={25}
              style={[commonStyle.iconColor, commonStyle.opacity50]}
              onPress={() => setShowPassword(!showPassword)}
            />
          }
          placeholder={LangStrings.LBL_PASSWORD_PLACEHOLDER}
          textInputLabel={LangStrings.LBL_PASSWORD_LABEL}
          onChangeText={text => setPassword(text)}
          secureTextEntry={!showPassword}
          error={!Helper.isUndefined(fieldErrors?.password)}
          renderErrorMessage={false}
          errorMessage={
            !Helper.isUndefined(fieldErrors?.password)
              ? fieldErrors?.password
              : ''
          }
        />
      </View>
      <Buttons
        size="lg"
        buttonStyle={[commonStyle.buttonPrimary]}
        titleStyle={commonStyle.buttonPrimaryTitle}
        title={LangStrings.LBL_SIGN_IN}
        onPress={signIn}
      />
    </KeyboardAvoidingView>
  );
};

export default SignInForm;
