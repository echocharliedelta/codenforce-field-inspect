import {StyleSheet} from 'react-native';
export const dynamicStyles = () => {
  return StyleSheet.create({
    signInForm: {
      width: '100%',
      paddingHorizontal: 16,
    },
    passwordInput: {
      marginVertical: 30,
    },
  });
};
