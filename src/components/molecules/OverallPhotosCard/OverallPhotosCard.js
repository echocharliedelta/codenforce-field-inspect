import {Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import {launchImageLibrary, launchCamera} from 'react-native-image-picker';
import {PermissionsAndroid} from 'react-native';
import {Helper} from '../../../utils/Helper';
import LangStrings from '../../../utils/langStrings';
import {useDispatch} from 'react-redux';
import {uiStartLoading, uiStopLoading} from '../../../store/slices/uiSlice';

const OverallPhotosCard = props => {
  const commonStyle = dynamicCommonStyles();
  const dispatch = useDispatch();
  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'App Camera Permission',
          message: 'App needs access to your camera ',
        },
      );
    } catch (err) {
      console.log(err);
    }
  };

  const openImagePicker = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 650,
      maxWidth: 650,
      selectionLimit: 90,
    };

    dispatch(uiStartLoading());

    launchImageLibrary(options, async response => {
      if (response.didCancel) {
        dispatch(uiStopLoading());
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('Image picker error: ', response.error);
        dispatch(uiStopLoading());
      } else {
        let imagesList = await processImagedata(response);
        props.uploadSelectedImages(imagesList);
        dispatch(uiStopLoading());
      }
    });
  };

  const handleCameraLaunch = async () => {
    await requestCameraPermission();
    const options = {
      mediaType: 'photo',
      includeBase64: true,
      quality: 0.9,
      maxWidth: 650,
      maxHeight: 650,
    };

    launchCamera(options, async response => {
      if (response.didCancel) {
        console.log('User cancelled camera');
      } else if (response.error) {
        console.log('Camera Error: ', response.error);
      } else {
        // Process the captured image
        let imagesList = await processImagedata(response);
        props.uploadSelectedImages(imagesList);
      }
    });
  };

  const processImagedata = async response => {
    if (
      !Helper.isUndefined(response) &&
      !Helper.isUndefined(response.assets) &&
      response.assets.length
    ) {
      const images = response.assets.map(image => {
        return {
          image_name: image?.base64,
          file_name: image?.fileName,
          size: image?.fileSize,
          uri: image?.uri,
          type: image?.type,
        };
      });
      return images;
    }
  };
  return (
    <>
      {props.type === 'camera' && (
        <TouchableOpacity
          onPress={handleCameraLaunch}
          style={[commonStyle.overallPhotosCard, commonStyle.bPrimaryLight]}>
          <Icon
            name={'camera'}
            size={27}
            style={[commonStyle.textSecondary, commonStyle.mB5]}
          />
          <Text style={[commonStyle.ftSize11, commonStyle.textSecondary]}>
            {LangStrings.LBL_CAMERA}
          </Text>
        </TouchableOpacity>
      )}
      {props.type === 'gallery' && (
        <TouchableOpacity
          onPress={openImagePicker}
          style={[commonStyle.overallPhotosCard, commonStyle.bPrimaryDark]}>
          <Icon
            name={'image-multiple'}
            size={27}
            style={[commonStyle.textSecondary, commonStyle.mB5]}
          />
          <Text style={[commonStyle.ftSize11, commonStyle.textSecondary]}>
            {LangStrings.LBL_GALLERY}
          </Text>
        </TouchableOpacity>
      )}
      {props.type === 'overall-photos' && (
        <TouchableOpacity
          style={[
            commonStyle.overallPhotosCard,
            props.disabled && commonStyle.opacity50,
          ]}
          disabled={props.disabled}
          onPress={props.navigateToOverallPhotos}>
          <Icon
            name={'camera-burst'}
            size={27}
            style={[commonStyle.textSecondary, commonStyle.mB5]}
          />
          <Text style={[commonStyle.ftSize11, commonStyle.textSecondary]}>
            {LangStrings.LBL_OVERALL_PHOTOS}
          </Text>
        </TouchableOpacity>
      )}
    </>
  );
};

export default OverallPhotosCard;
