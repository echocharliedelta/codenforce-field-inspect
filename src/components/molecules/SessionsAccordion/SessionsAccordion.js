import {View, Text} from 'react-native';
import {React, useState} from 'react';
import {ListItem} from '@rneui/themed';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import Buttons from '../../../components/atoms/Buttons/Buttons';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
import LangStrings from '../../../utils/langStrings';
import moment from 'moment';
import {Helper} from '../../../utils/Helper';
import {Sync} from '../../../utils/Sync';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';
import {uiStartLoading, uiStopLoading} from '../../../store/slices/uiSlice';
import {
  setSelectedIndex,
  setUserInformation,
} from '../../../store/slices/authSlice';

const LoadButton = ({
  recordDeactivatedTS,
  muniAuthPeriodId,
  muniCode,
  userId,
  userName,
}) => {
  const navigation = useNavigation();
  const commonStyle = dynamicCommonStyles();
  const dispatch = useDispatch();

  const navigateToInspection = async () => {
    try {
      await AsyncStorage.setItem('municipality_code', muniCode.toString());
      await AsyncStorage.setItem('auth_period', muniAuthPeriodId.toString());
      dispatch(setSelectedIndex(0));
      dispatch(setUserInformation({userId: userId, userName: userName}));
      dispatch(uiStartLoading());
      await Sync.syncData({userId: userId});
      dispatch(uiStopLoading());
      navigation.navigate('Inspection', {setIndexToOne: false});
    } catch (error) {
      dispatch(uiStopLoading());
    }
  };

  return (
    <Buttons
      onPress={navigateToInspection}
      title={LangStrings.LBL_LOAD}
      type="outline"
      containerStyle={[commonStyle.mT16, commonStyle.bPrimary]}
      buttonStyle={[commonStyle.w100]}
      titleStyle={[commonStyle.ftSFpro, commonStyle.textBlue]}
      radius="44"
      disabled={!Helper.isUndefined(recordDeactivatedTS)}
    />
  );
};

const SessionsAccordion = ({item}) => {
  const {
    assignmentRank,
    muniAuthPeriodId,
    accessGrantedDateStart,
    accessGrantedDateStop,
    recordDeactivatedTS = '',
    muniCode,
    muniName,
    codeOfficer,
    userId,
    userName,
  } = item;
  const commonStyle = dynamicCommonStyles();
  const [accordionState, setAccordionState] = useState({});

  return (
    <ListItem.Accordion
      icon={
        <Icon
          name={'chevron-down'}
          size={35}
          style={[commonStyle.textSecondary, commonStyle.opacity20]}
        />
      }
      Text={commonStyle.textPrimary}
      containerStyle={[commonStyle.bgSecondary]}
      bottomDivider={!accordionState[muniAuthPeriodId]}
      content={
        <ListItem.Content>
          <ListItem.Title>
            <Text
              style={[
                commonStyle.ftSize17,
                commonStyle.ftWeight700,
                commonStyle.textSecondary,
                commonStyle.ftSFpro,
              ]}>
              {muniName}
            </Text>
          </ListItem.Title>
          <Text style={[commonStyle.ftSize12, commonStyle.textDark2]}>
            {`Code Officer (Rank - ${assignmentRank})`}
          </Text>
          {accordionState[muniAuthPeriodId] ? null : (
            <LoadButton
              recordDeactivatedTS={recordDeactivatedTS}
              muniAuthPeriodId={muniAuthPeriodId}
              muniCode={muniCode}
              userId={userId}
              userName={userName}
            />
          )}
        </ListItem.Content>
      }
      isExpanded={accordionState[muniAuthPeriodId]}
      onPress={() => {
        setAccordionState(prevState => ({
          ...prevState,
          [muniAuthPeriodId]: !prevState[muniAuthPeriodId],
        }));
      }}>
      <ListItem
        bottomDivider
        containerStyle={[
          commonStyle.dFlex,
          commonStyle.bgSecondary,
          commonStyle.p0,
          commonStyle.mL15,
          commonStyle.textPrimary,
          commonStyle.mT10,
        ]}>
        <ListItem.Content style={commonStyle.F1}>
          <Text style={[commonStyle.pdB8, commonStyle.textMain]}>
            {LangStrings.LBL_PERIOD_ID}
          </Text>
          <Text style={[commonStyle.pdB8, commonStyle.textMain]}>
            {LangStrings.LBL_ACCESS_START}
          </Text>
          <Text style={[commonStyle.pdB8, commonStyle.textMain]}>
            {LangStrings.LBL_ACCESS_STOP}
          </Text>
          <Text style={[commonStyle.pdB8, commonStyle.textMain]}>
            {LangStrings.LBL_CODE_OFFICER}
          </Text>
          <Text style={[commonStyle.pdB8, commonStyle.textMain]}>
            {LangStrings.LBL_DEACTIVATED_TS}
          </Text>
          <View style={[commonStyle.mB20, commonStyle.textMain]}>
            <LoadButton
              recordDeactivatedTS={recordDeactivatedTS}
              muniAuthPeriodId={muniAuthPeriodId}
              muniCode={muniCode}
              userId={userId}
              userName={userName}
            />
          </View>
        </ListItem.Content>
        <ListItem.Content style={[commonStyle.mB77, commonStyle.F2]}>
          <Text style={[commonStyle.pdB8, commonStyle.textDark1]}>
            {muniAuthPeriodId}
          </Text>
          <Text style={[commonStyle.pdB8, commonStyle.textDark1]}>
            {moment(accessGrantedDateStart).format('ddd DD MMM YYYY, HH:mm')}
          </Text>
          <Text style={[commonStyle.pdB8, commonStyle.textDark1]}>
            {moment(accessGrantedDateStop).format('ddd DD MMM YYYY, HH:mm')}
          </Text>
          <Text style={[commonStyle.pdB8, commonStyle.textDark1]}>
            {codeOfficer ? 'Yes' : 'No'}
          </Text>
          <Text style={[commonStyle.pdB8, commonStyle.textDark1]}>
            {Helper.isUndefined(recordDeactivatedTS)
              ? LangStrings.LBL_VALID_CREDENTIAL
              : recordDeactivatedTS}
          </Text>
        </ListItem.Content>
      </ListItem>
    </ListItem.Accordion>
  );
};

export default SessionsAccordion;
