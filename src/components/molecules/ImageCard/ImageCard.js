import {View, Text, TouchableOpacity, Image} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import CustomDialog from '../CustomDialog/CustomDialog';
import LangStrings from '../../../utils/langStrings';
import useAppearance from '../../../utils/hooks/useAppearance';

const ImageCard = ({
  image,
  deleteSelectedPhoto,
  showNumber,
  isOrdinanceSelectedPhoto,
  isFromFindings = false,
  deletable = 0,
}) => {
  const [isShowConfirmDeleteDialog, setIsShowConfirmDeleteDialog] =
    useState(false);
  const commonStyle = dynamicCommonStyles();
  let base64Image = image.blob;

  const onBackdropPress = () => {
    setIsShowConfirmDeleteDialog(false);
  };

  const deletePhoto = () => {
    deleteSelectedPhoto(image?.bytesId, image.file_name);
    setIsShowConfirmDeleteDialog(false);
  };
  const isDarkMode = useAppearance();

  return (
    <View style={[commonStyle.imageCard]}>
      <Image
        source={{uri: base64Image}}
        style={{
          width: 80,
          height: 80,
          resizeMode: 'cover',
          borderRadius: 8,
          borderWidth: isOrdinanceSelectedPhoto && isFromFindings ? 4 : 0,
          borderColor: isDarkMode ? '#CCDFE4' : '#00465C',
        }}
      />
      {deletable === 0 && (
        <TouchableOpacity style={commonStyle.imageCardClose}>
          <Icon
            name={'close'}
            size={14}
            onPress={() => {
              setIsShowConfirmDeleteDialog(true);
            }}
          />
        </TouchableOpacity>
      )}
      <Text style={commonStyle.imageCardBadge}>{showNumber}</Text>
      <CustomDialog
        isVisible={isShowConfirmDeleteDialog}
        onBackdropPress={onBackdropPress}
        firstAction={deletePhoto}
        secondAction={setIsShowConfirmDeleteDialog}
        title={LangStrings.LBL_DELETE_PHOTO}
      />
    </View>
  );
};
export default ImageCard;
