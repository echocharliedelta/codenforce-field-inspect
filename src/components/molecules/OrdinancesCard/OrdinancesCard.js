import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import CustomButtonGroup from '../../atoms/CustomButtonGroup/CustomButtonGroup';
import LangStrings from '../../../utils/langStrings';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import {Helper} from '../../../utils/Helper';
import {InspectionStatus} from '../../../utils/constants';
import HTML from 'react-native-render-html';

const OrdinancesCard = ({
  data,
  Checked,
  viewOnly,
  onPress,
  batchPassedOrdinance,
  index,
}) => {
  const commonStyle = dynamicCommonStyles();
  const [showFullText, setShowFullText] = useState(false);
  const toggleShowFullText = () => {
    setShowFullText(!showFullText);
  };
  const toggleShowLessText = () => {
    setShowFullText(false);
  };
  const truncatedDescription =
    data?.ordTechnicalText && Helper.truncateString(data?.ordTechnicalText, 60);
  const changeInspectionOrdinanceStatus = value => {
    batchPassedOrdinance(
      false,
      value === 1
        ? 'inspectionPassed'
        : value === 2
        ? 'inspectionViolated'
        : 'inspectionNotInspected',
      data?.inspectedSpaceElementId,
      data,
    );
  };
  return (
    <View style={[commonStyle.bB1, commonStyle.bBPrimary, commonStyle.pdB11]}>
      <View
        style={[
          commonStyle.ftWeight600,
          commonStyle.ftSize13,
          commonStyle.textDark2,
          commonStyle.lH20,
          commonStyle.mT10,
          commonStyle.mL10,
          commonStyle.mR10,
        ]}>
        <HTML
          baseStyle={commonStyle.textDark2}
          source={{html: data?.headerStringStatic}}
          contentWidth={100}
        />
      </View>
      {viewOnly ? (
        <View>
          <View style={[commonStyle.mH10]}>
            <Text
              style={[
                commonStyle.pdT5,
                commonStyle.textAlignJustify,
                commonStyle.textSecondary,
              ]}>
              {showFullText
                ? data?.ordTechnicalText
                : truncatedDescription + '...'}
            </Text>
            {!showFullText && (
              <TouchableOpacity onPress={toggleShowFullText}>
                <Text style={[commonStyle.textBlue]}>
                  {LangStrings.LBL_SEE_MORE}
                </Text>
              </TouchableOpacity>
            )}
            {showFullText && (
              <TouchableOpacity onPress={toggleShowLessText}>
                <Text style={[commonStyle.textBlue]}>
                  {LangStrings.LBL_SEE_LESS}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      ) : (
        <View>
          <View
            style={[
              commonStyle.fRow,
              commonStyle.mL10,
              commonStyle.alignItemsCenter,
              commonStyle.justifyContentEnd,
            ]}>
            {!Checked || index === InspectionStatus.DONE ? (
              <TouchableOpacity
                disabled={Helper.getOrdinanceInspectionStatus(data) === 0}
                onPress={() => onPress(data.inspectedSpaceElementId, data)}
                style={[
                  commonStyle.mT5,
                  commonStyle.mR10,
                  commonStyle.justifyContentCenter,
                  commonStyle.fRow,
                  commonStyle.alignItemsCenter,
                  Helper.getOrdinanceInspectionStatus(data) === 0
                    ? {opacity: 0.5}
                    : null,
                ]}>
                <Icon
                  name={'camera'}
                  size={17}
                  style={[commonStyle.mR5, commonStyle.textBlue]}
                />
                <Text style={commonStyle.textBlue}>
                  {LangStrings.LBL_FINDINGS}
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
          <CustomButtonGroup
            disabled={index === InspectionStatus.DONE}
            status={Helper.getOrdinanceInspectionStatus(data)}
            changeInspectionOrdinanceStatus={changeInspectionOrdinanceStatus}>
            <Text>{LangStrings.LBL_NOT_INSPECTED}</Text>
            <Text>{LangStrings.LBL_PASSED}</Text>
            <Text>{LangStrings.LBL_VIOLATED}</Text>
          </CustomButtonGroup>
        </View>
      )}
    </View>
  );
};

export default OrdinancesCard;
