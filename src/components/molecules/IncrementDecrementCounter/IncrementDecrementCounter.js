import React from 'react';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import CustomInput from '../CustomInput/CustomInput';

const IncrementDecrementCounter = ({
  onPressInc,
  onPressDec,
  count,
  title,
  onChangeText,
  disabled,
}) => {
  const commonStyle = dynamicCommonStyles();
  return (
    <CustomInput
      renderErrorMessage={false}
      multiline={true}
      containerStyle={[commonStyle.pH0, commonStyle.bR7]}
      inputContainerStyle={[commonStyle.bBTransparent, commonStyle.mL11]}
      inputStyle={[commonStyle.customInputStyle]}
      textInputLabel={title}
      disabled={disabled}
      keyboardType="numeric"
      onChangeText={onChangeText}
      value={`${count}`}
      rightIcon={
        <Icon
          name={'plus'}
          size={20}
          style={[
            commonStyle.iconColor,
            commonStyle.opacity50,
            commonStyle.incrementDecrementRightIcon,
          ]}
          onPress={onPressInc}
          disabled={disabled ? true : false}
        />
      }
      leftIcon={
        <Icon
          name={'minus'}
          size={20}
          onPress={onPressDec}
          disabled={disabled ? true : false}
          style={[
            commonStyle.iconColor,
            commonStyle.opacity50,
            commonStyle.incrementDecrementLeftIcon,
          ]}
        />
      }
    />
  );
};

export default IncrementDecrementCounter;
