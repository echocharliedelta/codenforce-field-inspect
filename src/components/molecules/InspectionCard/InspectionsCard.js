import {View, Text, TouchableOpacity, Platform} from 'react-native';
import React, {useState} from 'react';
import Buttons from '../../../components/atoms/Buttons/Buttons';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import LangStrings from '../../../utils/langStrings';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import CustomDialog from '../CustomDialog/CustomDialog';
import moment from 'moment/moment';
import {InspectionStatus} from '../../../utils/constants';
import {Helper} from '../../../utils/Helper';
import {offlineService} from '../../../services/offlineService';
import {OFFLINE_TABLE_NAME} from '../../../config/config';

const InspectionsCard = ({data, index, deleteInitiatedInspection}) => {
  const commonStyle = dynamicCommonStyles();
  const navigation = useNavigation();

  const [isShowDialogForAddingSpace, setIsShowDialogForAddingSpace] =
    useState(false);
  const [isConfirmDeleteInspection, setIsConfirmDeleteInspection] =
    useState(false);

  const truncatedAddress =
    data?.propertyAddress && Helper.truncateString(data?.propertyAddress, 15);

  let inspectButtonTitle = LangStrings.LBL_INSPECT;
  let detailsButtonTitle = LangStrings.LBL_DETAILS;
  let detailsButtonPath = 'InspectionDetail';

  if (index === 1) {
    inspectButtonTitle = LangStrings.LBL_VIEW;
  }

  const navigateToSpacesScreen = () => {
    if (data?.inspectedSpacesCount > 0) {
      updateInspectionStartDateTime();
      navigation.navigate('Spaces', {
        propertyAddress: data?.propertyAddress,
        unitNumber: data?.unitNumber,
        inspectionId: data?.inspectionId,
        occChecklistId: data?.occChecklistId,
        governingCodeSourceId: data?.governingCodeSourceId,
        index: index,
      });
    } else {
      setIsShowDialogForAddingSpace(true);
    }
  };

  const updateInspectionStartDateTime = () => {
    if (!data?.isInspectionStarted) {
      //Update Inspection Effective Date only first time
      offlineService.offlineUpdateInspectionStartDate(
        data?.inspectionId,
        Helper.getCurrentUTCDateTime(),
        OFFLINE_TABLE_NAME.INSPECTIONS,
      );
    }
  };

  const onBackdropPress = () => {
    setIsShowDialogForAddingSpace(false);
  };

  const navigateToAddSpaceScreen = () => {
    navigation.navigate('AddSpaceScreen', {
      propertyAddress: data?.propertyAddress,
      unitNumber: data?.unitNumber,
      inspectionId: data?.inspectionId,
      occChecklistId: data?.occChecklistId,
      governingCodeSourceId: data?.governingCodeSourceId,
      index: index,
    });
    updateInspectionStartDateTime();
    setIsShowDialogForAddingSpace(false);
  };

  const toggleInspectionDelete = () => {
    setIsConfirmDeleteInspection(!isConfirmDeleteInspection);
  };

  return (
    <View style={[commonStyle.mT16, commonStyle.bB1, commonStyle.bBprimary]}>
      <View style={[commonStyle.fRow, {width: '75%'}]}>
        <Text style={[commonStyle.mL18, commonStyle.inspectionCardTitle]}>
          {truncatedAddress}
        </Text>
        {index === InspectionStatus.DONE && (
          <View style={[commonStyle.inspectionCardCheck]}>
            <Icon
              name={'check'}
              size={18}
              style={[
                commonStyle.colorWhite,
                data.isSync ? commonStyle.bgGreen : commonStyle.bgSynch,
              ]}
            />
          </View>
        )}
      </View>
      <View style={[commonStyle.mL18, commonStyle.dFlex, commonStyle.fRow]}>
        <View style={commonStyle.F1}>
          <Text style={[commonStyle.inspectionCardNames, commonStyle.textMain]}>
            {LangStrings.LBL_UNIT_NUMBER}
          </Text>
          <Text style={[commonStyle.inspectionCardNames, commonStyle.textMain]}>
            {LangStrings.LBL_INSPECTOR}
          </Text>
          {index === InspectionStatus.IN_PROGRESS ? (
            <Text
              style={[commonStyle.inspectionCardNames, commonStyle.textMain]}>
              {LangStrings.LBL_CHECKLIST}
            </Text>
          ) : (
            <Text
              style={[commonStyle.inspectionCardNames, commonStyle.textMain]}>
              {LangStrings.LBL_STATUS}
            </Text>
          )}

          <Text style={[commonStyle.inspectionCardNames, commonStyle.textMain]}>
            {LangStrings.LBL_EFFECTIVE_DATE}
          </Text>
        </View>
        <View style={commonStyle.F2}>
          <Text
            style={[commonStyle.inspectionCardNames, commonStyle.textDark1]}>
            {Helper.isNull(data.unitNumber)
              ? LangStrings.LBL_PRIMARY
              : data.unitNumber}
          </Text>
          <Text
            style={[commonStyle.inspectionCardNames, commonStyle.textDark1]}>
            {data?.inspectorName}
          </Text>
          {index === InspectionStatus.IN_PROGRESS ? (
            <Text
              style={[commonStyle.inspectionCardNames, commonStyle.textDark1]}>
              {data.checklistName}
            </Text>
          ) : (
            <Text
              style={[commonStyle.inspectionCardNames, commonStyle.textDark1]}>
              {data.determinationStatus}
            </Text>
          )}

          <Text
            style={[commonStyle.inspectionCardNames, commonStyle.textDark1]}>
            {data?.isInspectionStarted
              ? moment(data.effectiveDate).format('ddd DD MMM YYYY, HH:mm')
              : '-'}
          </Text>
        </View>
      </View>
      <View
        style={[
          commonStyle.mL18,
          commonStyle.mB20,
          commonStyle.dFlex,
          commonStyle.fRow,
          commonStyle.mT16,
        ]}>
        <Buttons
          title={inspectButtonTitle}
          type="outline"
          radius="44"
          containerStyle={[commonStyle.w100, commonStyle.bPrimary]}
          buttonStyle={[commonStyle.w100]}
          titleStyle={[commonStyle.ftSFpro, commonStyle.textBlue]}
          onPress={() => {
            navigateToSpacesScreen();
          }}
        />
        <Buttons
          title={detailsButtonTitle}
          type="outline"
          radius="44"
          containerStyle={[
            commonStyle.w100,
            commonStyle.mL10,
            commonStyle.bPrimary,
          ]}
          buttonStyle={[commonStyle.w100]}
          titleStyle={[commonStyle.ftSFpro, commonStyle.textBlue]}
          onPress={() => {
            navigation.navigate(detailsButtonPath, {
              inspectionId: data?.inspectionId,
              isFormAddInspectionScreen: false,
              isFromDone: index,
              propertyAddress: data?.propertyAddress,
            });
          }}
        />
      </View>
      <TouchableOpacity
        style={[commonStyle.inspectionCardIconContainer]}
        onPress={() => {
          navigation.navigate('OverallPhotos', {
            propertyAddress: data.propertyAddress,
            inspectionId: data.inspectionId,
            unitNumber: data.unitNumber,
            index: index,
          });
        }}>
        <Icon
          name={'image-multiple'}
          size={32}
          style={commonStyle.inspectionCardIcon}
        />
        <Text
          style={
            Platform.OS === 'ios'
              ? commonStyle.inspectionCardBedgeIOS
              : commonStyle.inspectionCardBedge
          }>
          {data?.overallPhotosCount > 99 ? '99+' : data?.overallPhotosCount}
          {/* {overallPhotosCount} */}
        </Text>
      </TouchableOpacity>

      {data?.isInspectionCreatedOnMobile && index === 0 ? (
        <TouchableOpacity
          style={[commonStyle.inspectionCardDeleteContainer]}
          onPress={toggleInspectionDelete}>
          <Icon
            name={'delete'}
            size={32}
            style={commonStyle.inspectionCardDeleteIcon}
          />
        </TouchableOpacity>
      ) : null}

      {isShowDialogForAddingSpace && (
        <CustomDialog
          isVisible={isShowDialogForAddingSpace}
          onBackdropPress={onBackdropPress}
          firstAction={navigateToAddSpaceScreen}
          secondAction={setIsShowDialogForAddingSpace}
          title={LangStrings.LBL_NO_SPACES_AGAINST_INSPECTION}
          firstActionTitle={LangStrings.LBL_ADD_SPACE}
          secondActionTitle={LangStrings.LBL_CANCEL}
        />
      )}

      {isConfirmDeleteInspection && (
        <CustomDialog
          isVisible={isConfirmDeleteInspection}
          onBackdropPress={toggleInspectionDelete}
          firstAction={() => {
            deleteInitiatedInspection(
              data?.inspectionId,
              data?.propertyAddress,
            );
          }}
          secondAction={toggleInspectionDelete}
          title={`${LangStrings.LBL_CONFIRM_DELETE_INSPECTION} "${data?.propertyAddress}"?`}
        />
      )}
    </View>
  );
};

export default InspectionsCard;
