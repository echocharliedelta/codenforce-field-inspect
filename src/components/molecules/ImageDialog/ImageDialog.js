import React, {useState, useEffect} from 'react';
import {Image, FlatList, Dimensions, TouchableOpacity} from 'react-native';
import {Overlay} from '@rneui/themed';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

const ImageDialog = ({
  isVisible,
  onBackdropPress,
  image,
  currentImageIndex,
}) => {
  const {width, height: windowHeight} = Dimensions.get('window');
  const commonStyle = dynamicCommonStyles();
  const [imageSizes, setImageSizes] = useState([]);
  const [overlayHeight, setOverlayHeight] = useState(250); // Default overlay height

  useEffect(() => {
    const getImageSizes = async () => {
      const sizes = [];
      for (const img of image) {
        const size = await getImageSize(img.blob);
        sizes.push(size);
      }
      setImageSizes(sizes);
      // Calculate the maximum height and set the overlay height
      const maxHeight = Math.max(...sizes.map(size => size.height - 100), 250); // Use 250 as a minimum height
      setOverlayHeight(maxHeight);
    };
    getImageSizes();
  }, [image]);

  const getImageSize = uri => {
    return new Promise((resolve, reject) => {
      Image.getSize(
        uri,
        (width, height) => {
          resolve({width, height});
        },
        error => {
          console.error('Error getting image size:', error);
          reject(error);
        },
      );
    });
  };

  const renderItem = ({item, index}) => {
    const imageSize = imageSizes[index];
    return (
      <Image
        resizeMode="contain"
        source={{uri: item.blob}}
        style={{
          width: width,
        }}
      />
    );
  };

  const getItemLayout = (data, index) => ({
    length: width,
    offset: width * index,
    index,
  });

  return (
    <Overlay
      isVisible={isVisible}
      overlayStyle={[
        commonStyle.bgSecondary,
        {
          paddingHorizontal: 0,
          borderRadius: 0,
          height: overlayHeight,
          paddingBottom: 10,
          paddingTop: 25,
        },
      ]}
      onBackdropPress={onBackdropPress}>
      <TouchableOpacity
        style={{position: 'absolute', top: 4, right: 15}}
        onPress={onBackdropPress}>
        <Icon
          name={'close'}
          size={16}
          style={[commonStyle.textDark1, commonStyle.ftWeight700]}
        />
      </TouchableOpacity>
      <FlatList
        data={image}
        renderItem={(item, index) => renderItem(item, index)}
        horizontal
        pagingEnabled
        snapToAlignment="center"
        getItemLayout={getItemLayout}
        initialScrollIndex={currentImageIndex} // Set the initial index here
      />
    </Overlay>
  );
};

export default ImageDialog;
