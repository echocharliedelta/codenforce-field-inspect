import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import BottomButtonGroup from '../../../components/atoms/BottomButtonGroup/BottomButtonGroup';
import LangStrings from '../../../utils/langStrings';
import CustomBottomSheet from '../../../components/molecules/CustomBottomSheet/CustomBottomSheet';
import CustomInput from '../../../components/molecules/CustomInput/CustomInput';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../../../config/config';
import {uiStartLoading, uiStopLoading} from '../../../store/slices/uiSlice';
import {Helper} from '../../../utils/Helper';
import Database from '../../../database/Database';
import {offlineService} from '../../../services/offlineService';
import OverallPhotosCard from '../../../components/molecules/OverallPhotosCard/OverallPhotosCard';
import ImageCard from '../../../components/molecules/ImageCard/ImageCard';
import Dropdowns from '../../../components/atoms/Dropdowns/Dropdowns';
import {useDispatch, useSelector} from 'react-redux';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import CustomDialog from '../CustomDialog/CustomDialog';
import {AutoDragSortableView} from 'react-native-drag-sort';
import ImageDialog from '../ImageDialog/ImageDialog';
import Buttons from '../../atoms/Buttons/Buttons';
import {MasterDBUtils} from '../../../utils/DBUtils/MasterDBUtils';

const defaultInspectionDetails = {
  notes: '',
  failureSeverityIntensityClassId: 0,
};
const FindingsBottomSheet = ({
  index,
  isVisible,
  onBackdropPress,
  inspectedSpaceElementId,
  inspectionId,
  currentOrdinanceDetails,
  showViolationSeverity = true,
}) => {
  const commonStyle = dynamicCommonStyles();
  const dispatch = useDispatch();
  const {width} = Dimensions.get('window');
  const {userInfo} = useSelector(state => state?.auth);

  const [ordinancePhotosList, setOrdinancePhotoList] = useState([]);
  const [violationSeverity, setViolationSeverity] = useState([]);
  const [ordinancesDetails, setOrdinancesDetails] = useState(
    defaultInspectionDetails,
  );
  const [isShowConfirmDialog, setIsShowConfirmDialog] = useState(false);
  const [overAllPhotosData, setOverAllPhotosData] = useState([]);
  const [ordinanceSelectedPhotos, setOrdinanceSelectedPhotos] = useState([]);
  const [overallPhotosCount, setOverAllPhotosCount] = useState(0);
  const [imageDialogVisible, setImageDialogVisible] = useState(false);
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const [discardedImages, setDiscardedImages] = useState([]);
  const [showOverallPhotos, setShowOverallPhotos] = useState(false);
  const [draftImages, setDraftImages] = useState([]);

  const parentWidth = width - 8;
  const childrenWidthPercentage = 0.22; // 20% of the screen width
  const childrenWidth = width * childrenWidthPercentage;
  const childrenHeight = 88;
  const headerViewHeight = 100;
  const bottomViewHeight = 40;

  useEffect(() => {
    if (inspectedSpaceElementId && isVisible) {
      getViolationSeverity();
      fetchPhotosAgainstOrdinance(inspectedSpaceElementId);
      getOrdinancesDetails(inspectedSpaceElementId);
    }
    return () => {
      setOverAllPhotosData([]), setOrdinancePhotoList([]);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inspectedSpaceElementId, isVisible]);

  useEffect(() => {
    if (showOverallPhotos) {
      getInspectionsPhotos();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showOverallPhotos]);

  // handling custom dialog
  const customDialogBackdropPress = () => {
    setIsShowConfirmDialog(false);
  };
  // rendering images at bottomSheet
  const renderItem = (item, imgIndex) => {
    return (
      <TouchableOpacity
        onPress={() => {
          expandedImage(imgIndex);
        }}>
        <ImageCard
          image={item}
          deletable={index}
          showNumber={imgIndex + 1}
          deleteSelectedPhoto={deletePhotoFromFindings}
          isFromFindings={true}
          index
        />
      </TouchableOpacity>
    );
  };

  // handling violation severity
  const handleViolationSeverity = selectedValue => {
    setOrdinancesDetails({
      ...ordinancesDetails,
      failureSeverityIntensityClassId: selectedValue.id,
    });
  };

  const discardChanges = async () => {
    () => {
      getOrdinancesDetails(inspectedSpaceElementId);
    };
    setIsShowConfirmDialog(false);
    onBackdropPress();
    dispatch(uiStopLoading());
    try {
      if (discardedImages?.length) {
        Database.deleteDiscardedPhotosFromFindings(discardedImages).then(
          setDiscardedImages([]),
        );
      } else {
        // Helper.showFlashMessage('danger', 'No inspection details found');
        console.log('No inspection details found');
        dispatch(uiStopLoading());
      }
    } catch (error) {
      Helper.showFlashMessage('danger', error);
    }
  };

  // getting ordinances details from local db
  const getOrdinancesDetails = async inspectedSpaceElementId => {
    try {
      dispatch(uiStartLoading());
      const ordinanceDetailsResponse = await Database.getRecords(
        OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
        '*',
        `inspectedSpaceElementId = '${inspectedSpaceElementId}'`,
      );
      if (ordinanceDetailsResponse?.length) {
        const formattedDetails = {
          ...ordinanceDetailsResponse[0],
          notes: ordinanceDetailsResponse[0].notes,
          failureSeverityIntensityClassId:
            ordinanceDetailsResponse[0].failureSeverityIntensityClassId,
        };
        setOrdinancesDetails(formattedDetails);
      } else {
        Helper.showFlashMessage('danger', 'No inspection details found');
      }
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage('danger', error);
    }
  };

  // getting violation severity for respective ordinance
  const getViolationSeverity = async () => {
    try {
      dispatch(uiStartLoading());
      const violationSeverity = await Database.getRecords(
        OFFLINE_TABLE_NAME.INSPECTION_INTENSITY_CLASS,
      );
      if (violationSeverity?.length) {
        let formattedData =
          await MasterDBUtils.prepareOrdinanceViolationSeverity(
            violationSeverity,
          );
        let activeData = formattedData.data.filter(obj => obj.active === 1);
        setViolationSeverity(activeData);
      } else {
        Helper.showFlashMessage(
          'danger',
          LangStrings.LBL_NO_INSP_DETAILS_FOUND,
        );
      }
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      console.log(error);
      Helper.showFlashMessage('danger', error);
    }
  };

  const fetchPhotosAgainstOrdinance = async inspectedSpaceElementId => {
    let temp = [];
    try {
      dispatch(uiStartLoading());
      let ordinancePhotos =
        await offlineService.getInspectionPhotoAgainstOrdinance(
          inspectedSpaceElementId,
        );
      if (ordinancePhotos?.data?.length) {
        temp = [
          ...ordinancePhotosList,
          ...(ordinancePhotos?.data || []).filter(
            item =>
              !ordinancePhotosList.some(
                existingItem => existingItem.bytesId === item.bytesId,
              ),
          ),
        ];
        setOrdinancePhotoList(temp);
        setDiscardedImages(discardedImages => [
          ...discardedImages,
          ...ordinanceSelectedPhotos,
        ]);
      } else {
        setOrdinancePhotoList([]);
      }
      await fetchOverAllPhotoCountAgainstOrdinance(temp);
      dispatch(uiStopLoading());
    } catch (error) {
      console.log('ERROR', error);
      dispatch(uiStopLoading());
    }
  };

  const fetchOverAllPhotoCountAgainstOrdinance = async temp => {
    let response = await offlineService.getPhotoCountAgainstInspection(
      inspectionId,
      temp,
    );
    setOverAllPhotosCount(response);
  };

  // uploading selected image against respective ordinance
  const uploadSelectedImages = async images => {
    const promises = [];
    if (images?.length) {
      for (let i = 0; i < images.length; i++) {
        promises.push(
          offlineService.offlineAddInspectionPhoto(
            images[i],
            inspectionId,
            userInfo,
            inspectedSpaceElementId,
            true,
          ),
        );
        setDiscardedImages(discardedImages => [
          ...discardedImages,
          ...ordinanceSelectedPhotos,
        ]);
      }
      Promise.all(promises)
        .then(() => {
          // All operations inside the loop have completed
          fetchPhotosAgainstOrdinance(inspectedSpaceElementId);
        })
        .catch(error => {
          // Handle errors if any of the promises reject
          console.error(error);
        });
    }
  };

  // saving updated data from bottomSheet
  const saveUpdatedData = async () => {
    let isSuccess = offlineService.offlineAddFindingAgainstOrdinance(
      {
        ...ordinancesDetails,
        notes: ordinancesDetails.notes,
        failureSeverityIntensityClassId:
          ordinancesDetails.failureSeverityIntensityClassId,
      },
      inspectedSpaceElementId,
    );
    if (isSuccess) {
      uploadSelectedImages(draftImages);
      setDiscardedImages([]);
      setOrdinanceSelectedPhotos([]);
      onBackdropPress();
    }
  };

  // overall photos operations
  const getInspectionsPhotos = async () => {
    try {
      dispatch(uiStartLoading());
      let inspectionOverallPhotoList =
        await Database.getInspectionOverallPhotosListData(
          OFFLINE_MODULE_NAME.INSPECTIONS_BLOB_BYTE,
          OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
          {inspectionId: inspectionId, ordinancePhotosIds: ordinancePhotosList},
        );
      if (
        !Helper.isUndefined(inspectionOverallPhotoList.data) &&
        inspectionOverallPhotoList.data?.length > 0
      ) {
        setOverAllPhotosData(inspectionOverallPhotoList.data);
        setOverAllPhotosCount(inspectionOverallPhotoList.data.length);
      }
      dispatch(uiStopLoading());
    } catch (error) {
      console.log('ERROR ********', error);
      dispatch(uiStopLoading());
      Helper.showFlashMessage(
        'danger',
        LangStrings.LBL_SOMETHING_WRONG_IN_INSP,
      );
    }
  };

  const renderImageCard = (item, index) => {
    const isSelected = ordinanceSelectedPhotos.includes(item.bytesId);
    return (
      <ImageCard
        image={item}
        showNumber={index + 1}
        isOrdinanceSelectedPhoto={isSelected}
        isFromFindings={true}
        deletable={1}
      />
    );
  };

  const deletePhotoFromFindings = async (bytesId, file_name) => {
    if (bytesId) {
      let isSuccess = await offlineService.offlineDeleteFindingsPhoto(bytesId);
      if (isSuccess) {
        // Remove the photo with the specified bytesId from ordinancePhotoList
        let filterDataForPhotos = ordinancePhotosList.filter(
          item => item.bytesId !== bytesId,
        );
        setOrdinancePhotoList(filterDataForPhotos);
        fetchOverAllPhotoCountAgainstOrdinance(filterDataForPhotos);
      }
    } else {
      // Remove photos with the specified file_name from both ordinancePhotoList and draftImages
      setOrdinancePhotoList(prevList =>
        prevList.filter(item => item.file_name !== file_name),
      );
      setDraftImages(prevList =>
        prevList.filter(item => item.file_name !== file_name),
      );
    }
  };

  const offlineAddInspectionPhotoAgainstOrdinance = async () => {
    try {
      dispatch(uiStartLoading());
      const promises = ordinanceSelectedPhotos.map(async bytesId => {
        await offlineService.offlineAddInspectionPhotoAgainstOrdinance(
          bytesId,
          inspectedSpaceElementId,
        );
      });
      await Promise.all(promises);
      dispatch(uiStopLoading());
      setShowOverallPhotos(!overAllPhotosData);
      fetchPhotosAgainstOrdinance(inspectedSpaceElementId);
      setOrdinanceSelectedPhotos([]);
    } catch (error) {
      console.log('ERROR', error);
      dispatch(uiStopLoading());
      Helper.showFlashMessage(
        'danger',
        LangStrings.LBL_SOMETHING_WRONG_IN_SPACES,
      );
    }
  };

  const expandedImage = index => {
    console.log('hello');
    setCurrentImageIndex(index);
    setImageDialogVisible(true);
  };

  const saveImagesWithDetails = images => {
    // Using map to extract the 'image_name' property from each object
    const imageNames = images.map((obj, index) => ({
      index,
      file_name: obj.file_name,
      blob: 'data:image/jpeg;base64,' + obj.image_name,
    }));
    setOrdinancePhotoList(prevList => [...prevList, ...imageNames]);
    setDraftImages(draftImages => [...draftImages, ...images]);
    // blob: 'data:image/jpeg;base64,' + photos?.blob,
  };

  const selectedViolationSeverity =
    violationSeverity.find(
      obj => obj.id === ordinancesDetails.failureSeverityIntensityClassId,
    )?.name || null;

  return (
    <>
      <CustomBottomSheet
        isVisible={isVisible}
        onBackdropPress={() => {
          setShowOverallPhotos(false);
          onBackdropPress();
        }}>
        {showOverallPhotos ? (
          <View
            style={[
              commonStyle.bgBottomSheet,
              commonStyle.pd16,
              commonStyle.pdB20,
              commonStyle.h600,
            ]}>
            <Text
              style={[
                commonStyle.ftSize17,
                commonStyle.ftWeight600,
                commonStyle.textSecondary,
              ]}>
              {`${currentOrdinanceDetails.ordSubSecNum} ${currentOrdinanceDetails.ordSubSecTitle}`}
            </Text>
            <View
              style={[
                commonStyle.bB1,
                commonStyle.bBPrimary,
                commonStyle.pdB20,
                commonStyle.F1,
                commonStyle.mB20,
              ]}>
              <AutoDragSortableView
                sortable={false}
                dataSource={overAllPhotosData}
                parentWidth={parentWidth}
                childrenWidth={childrenWidth}
                childrenHeight={childrenHeight}
                marginChildrenBottom={9}
                marginChildrenRight={18}
                marginChildrenLeft={13}
                onClickItem={(index, item) => {
                  if (ordinanceSelectedPhotos.includes(item.bytesId)) {
                    // If already selected, remove from the array

                    setOrdinanceSelectedPhotos(
                      ordinanceSelectedPhotos.filter(id => id !== item.bytesId),
                    );
                  } else {
                    // If not selected, add to the array
                    setOrdinanceSelectedPhotos([
                      ...ordinanceSelectedPhotos,
                      item.bytesId,
                    ]);
                    // setDiscardedImages(discardedImages => [...discardedImages, item.bytesId]);
                  }
                }}
                keyExtractor={(item, index) => item.bytesId}
                renderItem={(item, index) => {
                  return renderImageCard(item, index);
                }}
                headerViewHeight={headerViewHeight}
                bottomViewHeight={bottomViewHeight}
              />
            </View>
          </View>
        ) : (
          <View
            style={[
              commonStyle.bgBottomSheet,
              commonStyle.pd16,
              commonStyle.pdB20,
            ]}>
            <Text
              style={[
                commonStyle.ftSize17,
                commonStyle.ftWeight600,
                commonStyle.textSecondary,
              ]}>
              {`${currentOrdinanceDetails.ordSubSecNum} ${currentOrdinanceDetails.ordSubSecTitle}`}
            </Text>
            <View style={[commonStyle.mV20]}>
              {index === 0 ? (
                <CustomInput
                  value={ordinancesDetails.notes}
                  onChangeText={text =>
                    setOrdinancesDetails({...ordinancesDetails, notes: text})
                  }
                  renderErrorMessage={false}
                  multiline={true}
                  numberOfLines={3}
                  containerStyle={[commonStyle.pH0, commonStyle.bR7]}
                  inputContainerStyle={[
                    commonStyle.bBTransparent,
                    Platform.OS === 'ios'
                      ? commonStyle.pdL15
                      : commonStyle.mL12,
                  ]}
                  inputStyle={[
                    commonStyle.textAlignVerticalTop,
                    commonStyle.pdT5,
                    commonStyle.ftSize17,
                    commonStyle.textInput,
                  ]}
                  placeholder={
                    showViolationSeverity
                      ? LangStrings.LBL_DEFAULT_VIOLATION_DESCRIPTION
                      : LangStrings.LBL_DEFAULT_PASS_DESCRIPTION
                  }
                  textInputLabel={LangStrings.LBL_FINDINGS}
                />
              ) : (
                <>
                  <Text style={[commonStyle.textDark1]}>
                    {LangStrings.LBL_FINDINGS}
                  </Text>
                  {ordinancesDetails.notes.length ? (
                    <Text style={[commonStyle.textDark1]}>
                      {ordinancesDetails.notes}
                    </Text>
                  ) : showViolationSeverity ? (
                    <Text style={[commonStyle.textDark1]}>
                      {LangStrings.LBL_VIOLATION_DESC_NOT_FOUND}
                    </Text>
                  ) : (
                    <Text style={[commonStyle.textDark1]}>
                      {LangStrings.LBL_DESC_NOT_FOUND}
                    </Text>
                  )}
                </>
              )}
            </View>
            <Text style={[commonStyle.textSecondary]}>
              {LangStrings.LBL_PHOTOS}
            </Text>
            {index === 0 && (
              <View style={[commonStyle.fRow, commonStyle.mT10]}>
                <OverallPhotosCard
                  uploadSelectedImages={saveImagesWithDetails}
                  type="camera"
                />
                <OverallPhotosCard
                  uploadSelectedImages={saveImagesWithDetails}
                  type="gallery"
                />
                <OverallPhotosCard
                  disabled={overallPhotosCount > 0 ? false : true}
                  navigateToOverallPhotos={() => {
                    setShowOverallPhotos(!showOverallPhotos);
                  }}
                  type="overall-photos"
                />
              </View>
            )}
            {ordinancePhotosList?.length ? (
              <ScrollView
                style={[
                  index === 1 && !showViolationSeverity
                    ? commonStyle.pdB40
                    : null,
                ]}
                horizontal={true}>
                {ordinancePhotosList.map((item, index) =>
                  renderItem(item, index),
                )}
              </ScrollView>
            ) : (
              <Text
                style={[
                  commonStyle.textDark1,
                  !showViolationSeverity && commonStyle.pdB40,
                ]}>
                {LangStrings.LBL_PHOTOS_NOT_AVAILABLE}
              </Text>
            )}
            {showViolationSeverity && index === 0 ? (
              <View style={commonStyle.mV20}>
                <Dropdowns
                  data={violationSeverity}
                  title={LangStrings.LBL_VIOLATION_SEVERITY}
                  dropdownPosition="top"
                  onchange={handleViolationSeverity}
                  currentCauseId={
                    ordinancesDetails.failureSeverityIntensityClassId
                  }
                />
              </View>
            ) : showViolationSeverity && index === 1 ? (
              <>
                <Text
                  style={[
                    commonStyle.pdT20,
                    commonStyle.pdB5,
                    commonStyle.textSecondary,
                    commonStyle.ftSize14,
                  ]}>
                  {LangStrings.LBL_VIOLATION_SEVERITY}
                </Text>
                {selectedViolationSeverity !== null ? (
                  <Text style={[commonStyle.pdB40, commonStyle.textDark1]}>
                    {selectedViolationSeverity}
                  </Text>
                ) : (
                  <Text style={[commonStyle.pdB40, commonStyle.textDark1]}>
                    {LangStrings.LBL_VIOLATION_SEVERITY_NOT_AVAILABLE}
                  </Text>
                )}
              </>
            ) : null}
          </View>
        )}
        {index === 0 ? (
          <BottomButtonGroup
            status={0}
            onPress={[
              () => {
                showOverallPhotos
                  ? setShowOverallPhotos(!showOverallPhotos)
                  : setIsShowConfirmDialog(true);
              },
              () => {
                showOverallPhotos
                  ? offlineAddInspectionPhotoAgainstOrdinance()
                  : saveUpdatedData(inspectedSpaceElementId);
              },
            ]}>
            <Text style={commonStyle.colorWhite}>{LangStrings.LBL_CANCEL}</Text>
            <Text style={commonStyle.colorWhite}>
              {showOverallPhotos
                ? LangStrings.LBL_UPLOAD
                : LangStrings.LBL_SAVE}
            </Text>
          </BottomButtonGroup>
        ) : (
          <Buttons
            title="Cancel"
            containerStyle={[commonStyle.addSpaceButton]}
            buttonStyle={[commonStyle.bgTextLight]}
            titleStyle={[commonStyle.ftSFpro]}
            onPress={() => {
              onBackdropPress();
            }}
          />
        )}
        {isShowConfirmDialog && (
          <CustomDialog
            isVisible={isShowConfirmDialog}
            onBackdropPress={customDialogBackdropPress}
            firstAction={() => {
              discardChanges();
            }}
            secondAction={() => {
              setIsShowConfirmDialog(false);
            }}
            title={LangStrings.LBL_CONFIRM_DISCARD_CHANGES}
          />
        )}
        <ImageDialog
          currentImageIndex={currentImageIndex}
          image={ordinancePhotosList}
          isVisible={imageDialogVisible}
          onBackdropPress={() => {
            setImageDialogVisible(false);
          }}
        />
      </CustomBottomSheet>
    </>
  );
};

export default FindingsBottomSheet;
