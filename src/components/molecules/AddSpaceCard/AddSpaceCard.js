import {View, Text} from 'react-native';
import React from 'react';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import Buttons from '../../atoms/Buttons/Buttons';
import {Helper} from '../../../utils/Helper';
import {useNavigation} from '@react-navigation/native';
import {offlineService} from '../../../services/offlineService';
import {useSelector} from 'react-redux';
import LangStrings from '../../../utils/langStrings';

const AddSpaceCard = ({
  data,
  governingCodeSourceId,
  inspectionId,
  propertyAddress,
  index,
  occChecklistId,
}) => {
  const commonStyle = dynamicCommonStyles();
  const navigation = useNavigation();
  const {userInfo} = useSelector(state => state?.auth);

  const navigateToOrdinance = checklistSpaceTypeId => {
    navigation.navigate('Ordinances', {
      governingCodeSourceId: governingCodeSourceId,
      viewOnly: true,
      propertyAddress: propertyAddress,
      inspectionId: inspectionId,
      occChecklistId: occChecklistId,
      spaceTitle: data?.spaceTitle,
      checklistSpaceTypeId: checklistSpaceTypeId,
    });
  };

  const addSelectedSpaceAgainstInspection = async checklistSpaceTypeId => {
    let response = await offlineService.offlineAddSpace(
      checklistSpaceTypeId,
      inspectionId,
      governingCodeSourceId,
      userInfo,
    );
    navigation.navigate('Ordinances', {
      inspectedSpaceId: response?.inspectedSpaceId,
      spaceTitle: data?.spaceTitle,
      index: 0,
      inspectionId: inspectionId,
      occChecklistId: occChecklistId,
      propertyAddress: propertyAddress,
      governingCodeSourceId: governingCodeSourceId,
      checklistSpaceTypeId: checklistSpaceTypeId,
    });
  };

  return (
    <View style={[commonStyle.bB1, commonStyle.pdB15, commonStyle.bBPrimary]}>
      <View style={[commonStyle.mH18, commonStyle.mT16]}>
        <Text
          style={[
            commonStyle.ftSize17,
            commonStyle.ftWeight700,
            commonStyle.mB10,
            commonStyle.textSecondary,
          ]}>
          {data?.spaceTitle}
        </Text>
        <View style={[commonStyle.fRow, commonStyle.mB10]}>
          <Text
            style={[
              commonStyle.w100,
              commonStyle.textMain,
              commonStyle.ftSize15,
              commonStyle.textCapital,
            ]}>
            {LangStrings.LBL_DESCRIPTION}
          </Text>
          <Text style={[commonStyle.textDark1, commonStyle.ftSize15]}>
            {!Helper.isUndefined(data.description) ? data?.description : ''}
          </Text>
        </View>
        <View style={[commonStyle.fRow, commonStyle.mB10]}>
          <Text
            style={[
              commonStyle.w100,
              commonStyle.textMain,
              commonStyle.ftSize15,
            ]}>
            {LangStrings.LBL_REQUIRED}
          </Text>
          <Text style={[commonStyle.textDark1, commonStyle.ftSize15]}>
            {Helper.capitalizeFirstLetter(data?.required.toString())}
          </Text>
        </View>

        <View style={commonStyle.fRow}>
          <Buttons
            title="Select"
            type="outline"
            radius="44"
            containerStyle={[
              commonStyle.bPrimary,
              commonStyle.w100,
              commonStyle.mT10,
              commonStyle.mB10,
            ]}
            buttonStyle={[commonStyle.w100, commonStyle.pdB10]}
            titleStyle={[commonStyle.ftSFpro, commonStyle.textBlue]}
            onPress={() => {
              addSelectedSpaceAgainstInspection(data?.checklistSpaceTypeId);
            }}
          />
          <Buttons
            title="Details"
            type="outline"
            radius="44"
            containerStyle={[
              commonStyle.w100,
              commonStyle.mT10,
              commonStyle.mB10,
              commonStyle.mL18,
              commonStyle.bPrimary,
            ]}
            buttonStyle={[commonStyle.w100, commonStyle.pdB10]}
            titleStyle={[commonStyle.ftSFpro, commonStyle.textBlue]}
            onPress={() => {
              navigateToOrdinance(data?.checklistSpaceTypeId);
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default AddSpaceCard;
