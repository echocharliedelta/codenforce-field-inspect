import {View, Text, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import Buttons from '../../atoms/Buttons/Buttons';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import LangStrings from '../../../utils/langStrings';
import {useNavigation} from '@react-navigation/native';
import {Helper} from '../../../utils/Helper';
import CustomDialog from '../CustomDialog/CustomDialog';
import FindingsBottomSheet from '../FindingsBottomsheet/FindingsBottomsheet';
import {InspectionStatus} from '../../../utils/constants';

const SpacesCard = ({
  data,
  deleteInspectionSpace,
  propertyAddress,
  index,
  unitNumber,
  inspectionId,
  occChecklistId,
  governingCodeSourceId,
}) => {
  const navigation = useNavigation();
  const commonStyle = dynamicCommonStyles();

  const [isOpen, setIsOpen] = useState(false);
  const [violatedData, setViolatedData] = useState([]);
  const [isVisible, setIsVisible] = useState(false);
  const [isShowConfirmDeleteDialog, setIsShowConfirmDeleteDialog] =
    useState(false);
  const [isBottomSheetVisible, setIsBottomSheetVisible] = useState(false);
  const [currentOrdinanceDetails, setCurrentOrdinanceDetails] = useState([]);

  const toggleDialog = () => {
    setIsVisible(!isVisible);
  };

  const handleBottomSheet = () => {
    setIsBottomSheetVisible(!isBottomSheetVisible);
  };

  const getViolatedCount = () => {
    if (data?.occInspectedSpaceElementList?.length) {
      let violatedCount = data?.occInspectedSpaceElementList.filter(element => {
        return (
          Helper.isNull(element?.complianceGrantedTS) &&
          !Helper.isNull(element?.lastInspectedTS)
        );
      });
      return violatedCount?.length;
    }
    return 0;
  };

  const getPassedCount = () => {
    if (data?.occInspectedSpaceElementList?.length) {
      let passedCount = data?.occInspectedSpaceElementList.filter(element => {
        return !Helper.isNull(element?.complianceGrantedTS);
      });
      return passedCount?.length;
    }
    return 0;
  };

  const getNotInspectedCount = () => {
    if (data?.occInspectedSpaceElementList?.length) {
      let notInspectedCount = data?.occInspectedSpaceElementList.filter(
        element => {
          return (
            Helper.isNull(element?.complianceGrantedTS) &&
            Helper.isNull(element?.lastInspectedTS)
          );
        },
      );
      return notInspectedCount?.length;
    }
    return 0;
  };

  const getViolatedDataAgainstSpaces = () => {
    setIsOpen(!isOpen);
    let violatedData = data?.occInspectedSpaceElementList.filter(element => {
      return (
        Helper.isNull(element?.complianceGrantedTS) &&
        !Helper.isNull(element?.lastInspectedTS)
      );
    });
    setViolatedData(violatedData);
  };

  const deleteSpace = () => {
    deleteInspectionSpace(data?.inspectedSpaceId);
    setIsShowConfirmDeleteDialog(false);
  };

  const onBackdropPress = () => {
    setIsShowConfirmDeleteDialog(false);
  };

  const onPress = item => {
    handleBottomSheet();
    setCurrentOrdinanceDetails(item);
  };

  return (
    <View style={[commonStyle.bB1, commonStyle.bBPrimary, commonStyle.mB15]}>
      <View style={[commonStyle.dFlex, commonStyle.fRow, commonStyle.mL18]}>
        <View>
          <Text
            style={[
              commonStyle.mV10,
              commonStyle.textDark2,
              commonStyle.textUpper,
            ]}>
            {data.spaceTitle}
          </Text>
          <TouchableOpacity
            disabled={getViolatedCount() === 0}
            onPress={() => getViolatedDataAgainstSpaces()}
            style={[commonStyle.dFlex, commonStyle.fRow]}>
            <Text
              style={[
                commonStyle.ftSize15,
                commonStyle.ftSFpro,
                commonStyle.textBlue,
              ]}>
              {LangStrings.LBL_VIOLATED}
            </Text>
            <Text
              style={[
                commonStyle.mL65,
                commonStyle.ftSFpro,
                commonStyle.ftSize15,
                commonStyle.textBlue,
              ]}>
              {getViolatedCount()}
            </Text>
          </TouchableOpacity>
          <View style={[commonStyle.dFlex, commonStyle.fRow, commonStyle.mV10]}>
            <Text
              style={[
                commonStyle.ftSize15,
                commonStyle.ftSFpro,
                commonStyle.textMain,
              ]}>
              {LangStrings.LBL_PASSED}
            </Text>
            <Text
              style={[
                commonStyle.mL70,
                commonStyle.ftSFpro,
                commonStyle.ftSize15,
                commonStyle.textSecondary,
              ]}>
              {getPassedCount()}
            </Text>
          </View>
        </View>
        <View
          style={[
            commonStyle.dFlex,
            commonStyle.fRow,
            commonStyle.mV10,
            commonStyle.mL60,
            commonStyle.alignItemsEnd,
          ]}>
          <Text
            style={[
              commonStyle.ftSize15,
              commonStyle.ftSFpro,
              commonStyle.textMain,
            ]}>
            {LangStrings.LBL_NOT_INSPECTED}
          </Text>
          <Text
            style={[
              commonStyle.mL20,
              commonStyle.ftSFpro,
              commonStyle.ftSize15,
              commonStyle.textSecondary,
            ]}>
            {getNotInspectedCount()}
          </Text>
        </View>
      </View>
      {isOpen ? (
        <View
          style={[
            commonStyle.bT1,
            commonStyle.bTprimary,
            commonStyle.mT10,
            commonStyle.pdT5,
          ]}>
          {violatedData.map(item => (
            <View
              style={[
                commonStyle.fRow,
                commonStyle.mT10,
                commonStyle.minH35,
                commonStyle.mL18,
              ]}
              key={item?.elementId}>
              <Text style={[commonStyle.mR3, commonStyle.textSecondary]}>
                •
              </Text>
              <Text
                style={[
                  commonStyle.mR70,
                  commonStyle.textSecondary,
                ]}>{` ${item?.name} (${item?.year}) : ${item?.ordSubSecNum}: ${item?.ordSubSecTitle}`}</Text>
              <TouchableOpacity
                onPress={() => {
                  onPress(item);
                }}
                style={[commonStyle.spacesCardIconContainer]}>
                <Icon
                  name={index === 0 ? 'pencil' : 'eye'}
                  size={18}
                  style={commonStyle.spacesCardPencilIcon}
                />
              </TouchableOpacity>
            </View>
          ))}
        </View>
      ) : null}
      <Buttons
        title="Go"
        type="outline"
        radius="44"
        containerStyle={[
          commonStyle.bPrimary,
          commonStyle.w100,
          commonStyle.mT10,
          commonStyle.mB10,
          commonStyle.mL18,
        ]}
        buttonStyle={[commonStyle.w100, commonStyle.pdB10]}
        titleStyle={[commonStyle.ftSFpro, commonStyle.textBlue]}
        onPress={() => {
          navigation.navigate('Ordinances', {
            inspectedSpaceId: data?.appInspectedSpaceId,
            inspectionId: inspectionId,
            spaceTitle: data?.spaceTitle,
            index: index,
            unitNumber: unitNumber,
            propertyAddress: propertyAddress,
            occChecklistId: occChecklistId,
            governingCodeSourceId: governingCodeSourceId,
          });
        }}
      />
      {index != InspectionStatus.DONE && (
        <TouchableOpacity
          style={[commonStyle.inspectionCardIconContainer]}
          onPress={toggleDialog}>
          <Icon
            name={'delete'}
            size={24}
            style={commonStyle.spacesCardIcon}
            onPress={() => {
              setIsShowConfirmDeleteDialog(true);
            }}
            disabled={index === InspectionStatus.DONE}
          />
        </TouchableOpacity>
      )}
      {isShowConfirmDeleteDialog && (
        <CustomDialog
          isVisible={isShowConfirmDeleteDialog}
          onBackdropPress={onBackdropPress}
          firstAction={deleteSpace}
          secondAction={setIsShowConfirmDeleteDialog}
          title={LangStrings.formatString(LangStrings.LBL_DELETE_SPACE, [
            data.spaceTitle,
          ])}
        />
      )}
      <FindingsBottomSheet
        index={index}
        isVisible={isBottomSheetVisible}
        onBackdropPress={handleBottomSheet}
        inspectedSpaceElementId={
          currentOrdinanceDetails.inspectedSpaceElementId
        }
        inspectionId={data?.occInspectionId}
        inspectedSpaceId={data?.inspectedSpaceId}
        currentOrdinanceDetails={currentOrdinanceDetails}
        unitNumber={unitNumber}
        propertyAddress={propertyAddress}
      />
    </View>
  );
};

export default SpacesCard;
