import React from 'react';
import {ListItem} from '@rneui/themed';
import {useTheme} from '@react-navigation/native';

const OrdinancesCategory = ({item, index, selectedIndex, onPress}) => {
  const {colors} = useTheme();
  return (
    <ListItem
      containerStyle={{
        borderBottomWidth: 0.5,
        backgroundColor:
          selectedIndex === index ? colors.BG_SECONDARY : colors.BG_CATEGORY,
        padding: 10,
        margin: 0.5,
      }}
      onPress={() => onPress(index)}>
      <ListItem.Content>
        <ListItem.Title
          style={{
            color:
              selectedIndex === index
                ? colors.TEXT_SECONDARY
                : colors.TEXT_DARK_2,
          }}>
          {item}
        </ListItem.Title>
      </ListItem.Content>
    </ListItem>
  );
};

export default OrdinancesCategory;
