import {KeyboardAvoidingView, Platform} from 'react-native';
import React from 'react';
import {BottomSheet} from '@rneui/themed';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import useKeyboard from '../../../utils/hooks/keyboardHook';

const CustomBottomSheet = ({isVisible, onPress, onBackdropPress, children}) => {
  const commonStyle = dynamicCommonStyles();
  const isKeyboardOpen = useKeyboard();

  return (
    <BottomSheet
      containerStyle={{
        paddingBottom:
          isKeyboardOpen.isKeyboardVisible && Platform.OS === 'ios'
            ? isKeyboardOpen.keyboardHeight
            : 0,
      }}
      isVisible={isVisible}
      onBackdropPress={onBackdropPress}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={[commonStyle.F1]}>
        {children}
      </KeyboardAvoidingView>
    </BottomSheet>
  );
};

export default CustomBottomSheet;
