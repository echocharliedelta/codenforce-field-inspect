import {StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';

export const dynamicStyles = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const Colors = useTheme().colors;

  return StyleSheet.create({
    textInput: {
      backgroundColor: Colors.BG_TEXTAREA,
      borderRadius: 8,
    },
    disabledTextInput: {
      backgroundColor: Colors.BG_TEXTAREA, // Set this color as per your design
      opacity: 0.5,
    },
    textInputLabel: {
      color: Colors.TEXT_INPUT,
      opacity: 0.8,
      marginTop: 8,
      fontSize: 14,
      marginLeft: 16,
    },
  });
};
