import {View, Text} from 'react-native';
import React from 'react';
import {dynamicStyles} from './Style';
import {Input} from '@rneui/themed';
const CustomInput = props => {
  const styles = dynamicStyles();
  const {disabled} = props;

  return (
    <View style={[styles.textInput, disabled && styles.disabledTextInput]}>
      <Text style={styles.textInputLabel}>{props.textInputLabel}</Text>
      <Input {...props} autoCorrect={false} />
    </View>
  );
};

export default CustomInput;
