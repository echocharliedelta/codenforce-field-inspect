import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import CustomCheckbox from '../../atoms/CustomCheckbox/CustomCheckbox';
import LangStrings from '../../../utils/langStrings';

const AddInspectionCard = ({item, isSelected, onCardClick}) => {
  const commonStyle = dynamicCommonStyles();
  return (
    <TouchableOpacity
      onPress={onCardClick}
      style={[commonStyle.bBPrimary, commonStyle.bB1]}>
      <View style={[commonStyle.mH18]}>
        <View
          style={[
            commonStyle.fRow,
            commonStyle.alignItemsCenter,
            commonStyle.justifyContentSpaceBetween,
          ]}>
          <Text
            style={[
              commonStyle.mT10,
              commonStyle.textDark2,
              commonStyle.ftSize17,
              commonStyle.ftWeight600,
            ]}>
            {item.title}
          </Text>
          <CustomCheckbox
            checked={isSelected}
            containerStyle={[
              commonStyle.p0,
              commonStyle.bgSecondary,
              commonStyle.mB0,
            ]}
            onPress={onCardClick}
            wrapperStyle={[commonStyle.mT5]}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
          />
        </View>
        <Text
          style={[
            commonStyle.mT10,
            commonStyle.textMain,
            commonStyle.ftSize15,
          ]}>
          {LangStrings.LBL_DESCRIPTION}
        </Text>
        <Text
          style={[
            commonStyle.ftSize15,
            commonStyle.textDark1,
            commonStyle.mB20,
            commonStyle.mR35,
          ]}>
          {item.description}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default AddInspectionCard;
