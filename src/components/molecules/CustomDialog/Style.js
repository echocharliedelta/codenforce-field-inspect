import {StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';

export const dynamicStyles = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const Colors = useTheme().colors;

  return StyleSheet.create({
    containerStyle: {
      width: '50%',
      borderRadius: 0,
      margin: 0,
      padding: 0,
      borderBottomEndRadius: 8,
      backgroundColor: '#00B4EA',
    },
  });
};
