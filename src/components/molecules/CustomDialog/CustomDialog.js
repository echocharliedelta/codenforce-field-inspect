import {Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {Dialog} from '@rneui/themed';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {dynamicStyles} from './Style';

const CustomDialog = ({
  isVisible,
  onBackdropPress,
  firstAction,
  secondAction,
  title,
  firstActionTitle = 'Yes',
  secondActionTitle = 'No',
}) => {
  const commonStyle = dynamicCommonStyles();
  const styles = dynamicStyles();
  return (
    <Dialog
      isVisible={isVisible}
      overlayStyle={{paddingBottom: 0, paddingHorizontal: 0, borderRadius: 8}}
      onBackdropPress={onBackdropPress}>
      <Text style={[commonStyle.pH20, commonStyle.pdB15, commonStyle.pdT15]}>
        {title}
      </Text>
      <Dialog.Actions>
        <Dialog.Button
          type="solid"
          containerStyle={styles.containerStyle}
          buttonStyle={{borderRadius: 0, backgroundColor: '#00B4EA'}}
          title={firstActionTitle}
          onPress={() => {
            firstAction();
          }}
        />
        <Dialog.Button
          type="solid"
          containerStyle={{
            width: '50%',
            borderRadius: 0,
            margin: 0,
            padding: 0,
            borderBottomStartRadius: 8,
            backgroundColor: '#969696',
          }}
          buttonStyle={{borderRadius: 0, backgroundColor: '#969696'}}
          title={secondActionTitle}
          onPress={() => secondAction(false)}
        />
      </Dialog.Actions>
      <TouchableOpacity
        style={{position: 'absolute', top: 10, right: 10}}
        onPress={() => onBackdropPress()}>
        <Icon name={'close'} size={16} style={[commonStyle.dialogIconColor]} />
      </TouchableOpacity>
    </Dialog>
  );
};

export default CustomDialog;
