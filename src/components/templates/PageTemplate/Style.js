import {useTheme} from '@react-navigation/native';
import {StyleSheet} from 'react-native';
export const dynamicStyles = () => {
  const Colors = useTheme().colors;
  return StyleSheet.create({
    pageTemplate: {
      flex: 1,
      backgroundColor: Colors.BG_SECONDARY,
    },
    barStyle: {
      color: 'black',
    },
    pageTemplateHeader: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      paddingHorizontal: 3,
      marginTop: 10,
    },
    pageTemplateTitle: {
      color: Colors.TEXT_SECONDARY,
      fontSize: 34,
      fontWeight: '700',
      marginLeft: 18,
      marginTop: 3,
      fontFamily: 'SF Pro',
    },
    pageTemplateText: {
      textTransform: 'uppercase',
      marginTop: 8,
      marginLeft: 22,
      color: Colors.TEXT_LIGHT,
    },
    BackBtn: {
      flex: 1,
      display: 'flex',
      alignItems: 'flex-end',
    },
  });
};
