import {View, Text, StatusBar} from 'react-native';
import React from 'react';
import {dynamicStyles} from './Style';
import {dynamicCommonStyles} from '../../../assets/styles/CommonStyles';

const PageTemplate = ({children, ...props}) => {
  const styles = dynamicStyles();
  const commonStyle = dynamicCommonStyles();
  return (
    <View style={styles.pageTemplate}>
      {props.pageTitle ? (
        <Text
          style={[
            commonStyle.textUpper,
            commonStyle.mL18,
            commonStyle.textLight,
            commonStyle.ftSize13,
            commonStyle.mB5,
          ]}>
          {props.pageTitle}
        </Text>
      ) : null}

      {children}
    </View>
  );
};

export default PageTemplate;
