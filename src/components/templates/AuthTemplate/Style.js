import {useTheme} from '@react-navigation/native';
import {StyleSheet} from 'react-native';
export const dynamicStyles = () => {
  const Colors = useTheme().colors;
  return StyleSheet.create({
    authTemplate: {
      flex: 1,
      backgroundColor: Colors.BG_PRIMARY,
      justifyContent: 'center',
      alignItems: 'center',
    },
    statusBar: {
      backgroundColor: Colors.BG_PRIMARY,
    },
    logoWrap: {
      height: 31,
    },
    cnfLogo: {
      flex: 1,
    },
    authHeading: {
      color: Colors.TEXT_PRIMARY,
      fontSize: 18,
      fontWeight: '500',
      marginVertical: 30,
    },
  });
};
