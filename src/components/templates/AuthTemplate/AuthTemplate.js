import {View, Text, StatusBar, Image} from 'react-native';
import React from 'react';
import {dynamicStyles} from './Style';
import {useTheme} from '@react-navigation/native';

const AuthTemplate = ({children, ...props}) => {
  const styles = dynamicStyles();
  const Colors = useTheme().colors;

  return (
    <View style={styles.authTemplate}>
      <StatusBar
        barStyle={'light-content'}
        backgroundColor={Colors.BG_PRIMARY}
      />
      <View style={styles.logoWrap}>
        <Image
          style={styles.cnfLogo}
          source={require('../../../assets/images/cnfLogo.png')}
        />
      </View>
      <Text style={styles.authHeading}>{props.AuthTemplateHeading}</Text>
      {children}
    </View>
  );
};

export default AuthTemplate;
