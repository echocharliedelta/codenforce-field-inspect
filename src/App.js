import React, {useEffect} from 'react';
import {
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  View,
  Text,
} from 'react-native';
import NavContainer from './navigations/NavContainer';
import NavigationService from './navigations/NavigationService';
import {NotifierWrapper} from 'react-native-notifier';
import {useSelector, useDispatch} from 'react-redux';
import NetInfo from './components/atoms/NetInfo/NetInfo';
import {getDispatch} from './config/authAxios.config';
import useAppearance from './utils/hooks/useAppearance';
import {LightTheme, DarkTheme} from './theme/Theme';
import SplashScreen from 'react-native-splash-screen';
import Loader from './components/atoms/Loader';
import BackgroundFetch from 'react-native-background-fetch';
import {Sync} from './utils/Sync';
import {offlineService} from './services/offlineService';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

const App = () => {
  const {isSignedIn, userInfo} = useSelector(state => state?.auth);
  const {isLoading} = useSelector(state => state?.ui);
  const dispatch = useDispatch();
  const isDarkMode = useAppearance();
  const statusBarHeight = getStatusBarHeight();
  // to dispatch action from interceptors
  getDispatch(dispatch);

  // Override Text scaling
  if (Text.defaultProps) {
    Text.defaultProps.allowFontScaling = false;
  } else {
    Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
  }

  // Override Text scaling in input fields
  if (TextInput.defaultProps) {
    TextInput.defaultProps.allowFontScaling = false;
  } else {
    TextInput.defaultProps = {};
    TextInput.defaultProps.allowFontScaling = false;
  }

  useEffect(() => {
    SplashScreen.hide();
    BackgroundFetch.configure(
      {
        minimumFetchInterval: 15, // <-- minutes (15 is minimum allowed)
        // Android options
        stopOnTerminate: false,
        startOnBoot: true,
        enableHeadless: false,
        requiredNetworkType: BackgroundFetch.NETWORK_TYPE_ANY, // Default
        requiresCharging: false, // Default
        requiresDeviceIdle: false, // Default
        requiresBatteryNotLow: false, // Default
        requiresStorageNotLow: false, // Default
      },
      () => {
        Sync.syncData();
        Sync.syncInspectionModulesDataDownToUp(userInfo?.userId);
        offlineService.getPhotosDataAvailableForSync();
        // If you fail to do this, the OS can terminate your app
        // or assign battery-blame for consuming too much background-time
        BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
      },
      error => {
        console.log('[js] RNBackgroundFetch failed to start:', error);
      },
    );

    // Optional: Query the authorization status.
    BackgroundFetch.status(status => {
      switch (status) {
        case BackgroundFetch.STATUS_RESTRICTED:
          console.log('BackgroundFetch restricted');
          break;
        case BackgroundFetch.STATUS_DENIED:
          console.log('BackgroundFetch denied');
          break;
        case BackgroundFetch.STATUS_AVAILABLE:
          console.log('BackgroundFetch is enabled');
          break;
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const STATUS_BAR_HEIGHT = Platform.OS === 'ios' && statusBarHeight;

  function StatusBarPlaceHolder() {
    return (
      <View
        style={{
          width: '100%',
          height: STATUS_BAR_HEIGHT,
          backgroundColor: !isSignedIn
            ? isDarkMode
              ? '#2F2F2F'
              : '#00465C'
            : isDarkMode
            ? '#2F2F2F'
            : '#EDFBFF',
        }}>
        <StatusBar
          barStyle={
            !isSignedIn
              ? 'light-content'
              : isDarkMode
              ? 'light-content'
              : 'dark-content'
          }
        />
      </View>
    );
  }

  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <NotifierWrapper>
        {Platform.OS === 'ios' ? (
          <StatusBarPlaceHolder />
        ) : (
          <StatusBar
            barStyle={isDarkMode ? 'light-content' : 'dark-content'}
            backgroundColor={
              !isSignedIn ? '#00465C' : isDarkMode ? '#2F2F2F' : '#EDFBFF'
            }
          />
        )}
        <SafeAreaView
          style={{
            backgroundColor: !isSignedIn
              ? '#00465C'
              : isDarkMode
              ? '#2F2F2F'
              : '#EDFBFF',
            flex: 1,
          }}>
          <NetInfo />
          <NavContainer
            style={{fontFamily: 'SF Pro'}}
            appTheme={isDarkMode ? DarkTheme : LightTheme}
            signedIn={isSignedIn}
            navRef={NavigationService.navigationRef}
          />
          <Loader isLoading={isLoading} />
        </SafeAreaView>
      </NotifierWrapper>
    </GestureHandlerRootView>
  );
};
export default App;
