import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {inspectionConfig} from './config';
import {Helper} from '../utils/Helper';
import {store} from '../store/configureStore';
import {signOutUser} from '../store/slices/authSlice';
let dispatch;

export const getDispatch = dispatchRef => (dispatch = dispatchRef);

const inspectionAxiosInstance = axios.create({
  baseURL: inspectionConfig.baseUrl,
});

inspectionAxiosInstance.interceptors.request.use(
  async configReq => {
    let token = store.getState().auth.token;
    console.log('TOKEN', token);
    configReq.headers.Authorization = `${token}`;
    configReq.baseURL = inspectionConfig.baseUrl;
    console.log(configReq.baseURL + configReq.url);
    return configReq;
  },
  error => Promise.reject(error),
);

inspectionAxiosInstance.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error && error.response && error.response.data.error) {
      if (
        error.response.status === 401 ||
        error.response.data.error === 'Unauthorized'
      ) {
        AsyncStorage.removeItem('token');
        dispatch(signOutUser());
        Helper.showFlashMessage('danger', 'Session Expired');
        return Promise.reject(error);
      }
    } else if (
      error.response.data.message &&
      !error?.response?.config?.url?.includes('image')
    ) {
      Helper.showFlashMessage(
        'danger',
        error.response.data.message ?? 'Network Error.',
      );
    }

    return Promise.reject(error);
  },
);
export default inspectionAxiosInstance;
