export const authConfig = {
  pageSize: 50,
  baseUrl: 'https://fnm-mobile.benchmarkit.solutions',
  tokenExpiryCheckInterval: 900000,
};

export const inspectionConfig = {
  pageSize: 50,
  baseUrl: 'https://fnm-mobile1.benchmarkit.solutions',
  tokenExpiryCheckInterval: 900000,
};

export const microServices = {
  inspections: '/occInspection/periodAuth/',
};

export const OFFLINE_MODULE_NAME = {
  INSPECTION_DETERMINATIONS: 'Determinations',
  INSPECTION_CAUSE: 'Causes',
  INSPECTION_INTENSITY_CLASS: 'IntensityClass',
  INSPECTION_CHECKLIST: 'Checklist',
  INSPECTION_CODE_ELEMENT: 'CodeElement',
  INSPECTION_CODE_ELEMENT_GUIDE: 'CodeElementGuide',
  INSPECTION_CHECKLIST_SPACE_TYPE: 'ChecklistSpaceTypes',
  INSPECTIONS: 'Inspections',
  INSPECTIONS_INSPECTED_SPACE: 'InspectionSpace',
  INSPECTIONS_INSPECTED_SPACE_ELEMENT: 'InspectionInspectedSpaceElement',
  INSPECTIONS_BLOB_BYTE: 'InspectionBlobByte',
  INSPECTION_INSPECTED_ELEMENT_PHOTO_DOC: 'InspectedSpaceELementPhotoDoc',
  INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS: 'ChecklistSpaceTypeElements',
};

export const OFFLINE_TABLE_NAME = {
  INSPECTION_DETERMINATIONS: 'occinspectiondetermination',
  INSPECTION_CAUSE: 'occinspectioncause',
  INSPECTION_INTENSITY_CLASS: 'intensityclass',
  INSPECTION_CHECKLIST: 'occchecklist',
  INSPECTION_CODE_ELEMENT: 'codeelement',
  INSPECTION_CODE_ELEMENT_GUIDE: 'codeelementguide',
  INSPECTION_CHECKLIST_SPACE_TYPE: 'occchecklistspacetype',
  INSPECTIONS: 'occinspection',
  INSPECTIONS_INSPECTED_SPACE: 'occinspectedspaces',
  INSPECTIONS_INSPECTED_SPACE_ELEMENT: 'occInspectedSpaceElementList',
  INSPECTIONS_BLOB_BYTE: 'blobbytes',
  INSPECTION_INSPECTED_ELEMENT_PHOTO_DOC: 'occinspectedspaceelementphotodoc',
  INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS: 'occChecklistSpaceTypeElements',
};
