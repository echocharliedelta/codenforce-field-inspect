import axios from 'axios';
import {authConfig} from './config';
import {store} from '../store/configureStore';
import {signOutUser} from '../store/slices/authSlice';
let dispatch;

export const getDispatch = dispatchRef => (dispatch = dispatchRef);

const axiosInstance = axios.create({
  baseURL: authConfig.baseUrl,
});

axiosInstance.interceptors.request.use(
  async configReq => {
    let token = store.getState().auth.token;
    console.log('TOKEN', token);
    configReq.headers.Authorization = `${token}`;
    configReq.baseURL = authConfig.baseUrl;
    console.log(configReq.baseURL + configReq.url);
    return configReq;
  },
  error => Promise.reject(error),
);

axiosInstance.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    console.log('ERROR', error.response);
    if (error.response.status === 401 && error.config.url !== '/login/user') {
      console.log('ERROR', error.response.status);
      dispatch(signOutUser());
      return Promise.reject(error);
    }
    return Promise.reject(error);
  },
);
export default axiosInstance;
