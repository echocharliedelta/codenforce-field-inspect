import {
  BackHandler,
  FlatList,
  KeyboardAvoidingView,
  Platform,
  Text,
  View,
} from 'react-native';
import PageTemplate from '../../components/templates/PageTemplate/PageTemplate';
import CustomInput from '../../components/molecules/CustomInput/CustomInput';
import {dynamicCommonStyles} from '../../assets/styles/CommonStyles';
import TextInput from '../../components/atoms/TextInput/TextInput';
import AddInspectionCard from '../../components/molecules/AddInspectionCard/AddInspectionCard';
import {useEffect, useState} from 'react';
import BottomButtonGroup from '../../components/atoms/BottomButtonGroup/BottomButtonGroup';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import LangStrings from '../../utils/langStrings';
import {OFFLINE_TABLE_NAME} from '../../config/config';
import {useDispatch} from 'react-redux';
import {uiStartLoading, uiStopLoading} from '../../store/slices/uiSlice';
import {Helper} from '../../utils/Helper';
import Database from '../../database/Database';
import CustomDialog from '../../components/molecules/CustomDialog/CustomDialog';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {MasterDBUtils} from '../../utils/DBUtils/MasterDBUtils';
import {useHeaderHeight} from '@react-navigation/elements';

const defaultAddInspectionObject = {
  propertyAddress: '',
  unitNumber: '',
};
const AddInspectionScreen = ({route}) => {
  const commonStyle = dynamicCommonStyles();
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const [checkListData, setCheckListData] = useState([]);
  const [selectedCheckListItem, setSelectedCheckListItem] = useState(null);
  const [addInspectionData, setAddInspectionData] = useState(
    defaultAddInspectionObject,
  );
  const [fieldErrors, setFieldErrors] = useState([]);
  const [
    isShowDialogForCancelAddInspection,
    setIsShowDialogForCancelAddInspection,
  ] = useState(false);
  const height = useHeaderHeight();

  useEffect(() => {
    if (isFocused) {
      getChecklist();
    } else {
      setCheckListData([]);
      setIsShowDialogForCancelAddInspection(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  useEffect(() => {
    const backAction = () => {
      setAddInspectionData(defaultAddInspectionObject);
      navigation.navigate('Inspection', {setIndexToOne: false});
      return true;
    };
    if (isFocused) {
      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        backAction,
      );
      return () => {
        backHandler.remove();
      };
    }
    return undefined;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  // getting list of inspection causes for dropdown
  const getChecklist = async () => {
    let municipality_code = await AsyncStorage.getItem('municipality_code');
    try {
      let where = '';
      dispatch(uiStartLoading());
      let otherJoin =
        ' INNER JOIN occchecklistspacetype ON occchecklist.checkListId=occchecklistspacetype.checklistId';
      where = `(occchecklist.deactivatedTs='' OR occchecklist.deactivatedTs IS NULL) AND occchecklist.muniCode=${municipality_code} AND (occchecklistspacetype.deactivatedTs='' OR occchecklistspacetype.deactivatedTs IS NULL) GROUP BY occChecklist.checkListId`;
      let orderBy = 'occChecklist.title ASC';

      const checkList = await Database.getRecords(
        OFFLINE_TABLE_NAME.INSPECTION_CHECKLIST,
        '*',
        where,
        otherJoin,
        orderBy,
      );
      if (checkList?.length) {
        let formattedData =
          await MasterDBUtils.prepareInspectionCheckListObject(checkList);
        setSelectedCheckListItem(formattedData.data[0]?.id);
        setCheckListData(formattedData.data);
      } else {
        Helper.showFlashMessage('danger', LangStrings.LBL_NO_CHECKLIST_FOUND);
      }
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage('danger', error);
    }
  };

  const handleCardClick = itemId => {
    setSelectedCheckListItem(itemId);
  };

  const renderItem = ({item}) => {
    const isSelected = !Helper.isUndefined(selectedCheckListItem)
      ? selectedCheckListItem === item.id
      : item.id === checkListData[0]?.id;

    return (
      <AddInspectionCard
        item={item}
        isSelected={isSelected}
        onCardClick={() => handleCardClick(item.id)}
      />
    );
  };

  const handleInputChange = (field, value) => {
    setAddInspectionData({...addInspectionData, [field]: value});
  };

  const clearState = () => {
    setAddInspectionData(defaultAddInspectionObject);
  };

  const navigateToInspectionDetails = async () => {
    if (await validate()) {
      navigation.navigate('InspectionDetail', {
        onClearState: clearState,
        isFormAddInspectionScreen: true,
        addInspectionData: {
          propertyAddress: addInspectionData.propertyAddress.trim(),
          unitNumber: addInspectionData.unitNumber.trim(),
        },
        selectedCheckListItem,
        propertyAddress: addInspectionData?.propertyAddress,
      });
    }
  };

  const validate = async () => {
    let fieldErrors = {};
    let result = true;

    if (Helper.isNull(addInspectionData?.propertyAddress.trim())) {
      fieldErrors.propertyAddress = LangStrings.LBL_PROPERTY_ADDRESS_VALIDATION;
      result = false;
    }
    setFieldErrors(fieldErrors);
    return result;
  };

  const onBackdropPress = () => {
    setIsShowDialogForCancelAddInspection(false);
  };

  const resetData = () => {
    setCheckListData([]);
    setSelectedCheckListItem(null);
    setAddInspectionData(defaultAddInspectionObject);
    setFieldErrors([]);
  };

  return (
    <PageTemplate pageTitle={LangStrings.LBL_ADD_NEW}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={commonStyle.F1}
        keyboardVerticalOffset={height + 20}>
        <View style={commonStyle.mH18}>
          <View style={commonStyle.mT20}>
            <CustomInput
              renderErrorMessage={false}
              multiline={true}
              numberOfLines={3}
              minHeight={Platform.OS === 'ios' ? 70 : 0}
              containerStyle={[commonStyle.pH0, commonStyle.bR7]}
              inputContainerStyle={[
                commonStyle.bBTransparent,
                Platform.OS === 'ios' ? commonStyle.mL16 : commonStyle.mL13,
              ]}
              inputStyle={[
                commonStyle.textAlignVerticalTop,
                commonStyle.pdT5,
                commonStyle.ftSize17,
                commonStyle.textInput,
              ]}
              placeholder={LangStrings.LBL_PROPERTY_ADDRESS_PLACEHOLDER}
              textInputLabel={LangStrings.LBL_PROPERTY_ADDRESS}
              value={addInspectionData.propertyAddress}
              onChangeText={value =>
                handleInputChange('propertyAddress', value)
              }
              error={Helper.isUndefined(fieldErrors?.propertyAddress)}
              errorMessage={
                !Helper.isUndefined(fieldErrors?.propertyAddress)
                  ? fieldErrors?.propertyAddress
                  : ''
              }
            />
          </View>
          <View style={[commonStyle.mT10]}>
            <TextInput
              renderErrorMessage={false}
              containerStyle={[
                commonStyle.bR7,
                commonStyle.pH0,
                commonStyle.bgTextarea,
              ]}
              inputContainerStyle={[
                commonStyle.bBTransparent,
                Platform.OS === 'ios' ? commonStyle.mL16 : commonStyle.mL13,
              ]}
              inputStyle={[
                Platform.OS === 'ios' ? commonStyle.h45 : commonStyle.h36,
                commonStyle.minH35,
                commonStyle.textDark1,
                commonStyle.ftSize17,
              ]}
              placeholder={LangStrings.LBL_UNIT_NUMBER}
              value={addInspectionData.unitNumber}
              onChangeText={value => handleInputChange('unitNumber', value)}
            />
          </View>
          <Text
            style={[
              commonStyle.mT20,
              commonStyle.textUpper,
              commonStyle.textLight,
            ]}>
            {LangStrings.LBL_CHOOSE_CHECKLIST}
          </Text>
        </View>
        {checkListData?.length ? (
          <FlatList
            data={checkListData}
            renderItem={renderItem}
            keyExtractor={item => item.id}
            style={[commonStyle.mT10, commonStyle.mB10]}
          />
        ) : (
          <Text>{LangStrings.LBL_NO_CHECKLIST_FOUND}</Text>
        )}
        <BottomButtonGroup
          status={0}
          onPress={[
            () => {
              setIsShowDialogForCancelAddInspection(true);
            },
            () => navigateToInspectionDetails(),
          ]}>
          <Text style={[commonStyle.colorWhite, commonStyle.textUpper]}>
            {LangStrings.LBL_CANCEL}
          </Text>

          <Text style={commonStyle.colorWhite}>{LangStrings.LBL_NEXT}</Text>
        </BottomButtonGroup>

        {isShowDialogForCancelAddInspection ? (
          <CustomDialog
            isVisible={isShowDialogForCancelAddInspection}
            onBackdropPress={onBackdropPress}
            firstAction={() => {
              resetData();
              navigation.navigate('Inspection', {setIndexToOne: false});
            }}
            secondAction={setIsShowDialogForCancelAddInspection}
            title={LangStrings.LBL_CANCEL_ADD_INSPECTION_CONFIRMATION}
          />
        ) : null}
      </KeyboardAvoidingView>
    </PageTemplate>
  );
};

export default AddInspectionScreen;
