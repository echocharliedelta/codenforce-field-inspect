import {StyleSheet} from 'react-native';
export const dynamicStyles = () => {
  return StyleSheet.create({
    loadingContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    imageStyle: {
      resizeMode: 'contain',
      width: 250,
      left: 0,
      right: 0,
      top: 10,
      margin: 10,
    },
  });
};
