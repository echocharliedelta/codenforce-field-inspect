import React from 'react';
import {ActivityIndicator, Image, SafeAreaView} from 'react-native';
import {dynamicStyles} from './Style';

const LoadingScreen = () => {
  const styles = dynamicStyles();

  return (
    <SafeAreaView style={styles.loadingContainer}>
      <ActivityIndicator size="large" color="blue" />
      <Image style={styles.imageStyle} />
    </SafeAreaView>
  );
};

export default LoadingScreen;
