import {
  View,
  Text,
  FlatList,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import React, {useState, useLayoutEffect, useEffect} from 'react';
import PageTemplate from '../../components/templates/PageTemplate/PageTemplate';
import {dynamicCommonStyles} from '../../assets/styles/CommonStyles';
import SpacesCard from '../../components/molecules/SpacesCard/SpacesCard';
import LangStrings from '../../utils/langStrings';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import BottomButtonGroup from '../../components/atoms/BottomButtonGroup/BottomButtonGroup';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../../config/config';
import {Helper} from '../../utils/Helper';
import Database from '../../database/Database';
import {useDispatch, useSelector} from 'react-redux';
import {uiStartLoading, uiStopLoading} from '../../store/slices/uiSlice';
import CustomBottomSheet from '../../components/molecules/CustomBottomSheet/CustomBottomSheet';
import CustomCheckbox from '../../components/atoms/CustomCheckbox/CustomCheckbox';
import Dropdowns from '../../components/atoms/Dropdowns/Dropdowns';
import CustomInput from '../../components/molecules/CustomInput/CustomInput';
import {offlineService} from '../../services/offlineService';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {InspectionStatus} from '../../utils/constants';
import {HeaderBackButton} from '@react-navigation/elements';
import {setSelectedIndex} from '../../store/slices/authSlice';
import {useBackHandler} from '@react-native-community/hooks';
import {Sync} from '../../utils/Sync';
import {checkHasInternet} from '../../utils/functions/checkHasInternet';
import {MasterDBUtils} from '../../utils/DBUtils/MasterDBUtils';

const SpacesScreen = ({route}) => {
  const commonStyle = dynamicCommonStyles();
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const {userInfo, selectIndex} = useSelector(state => state?.auth);

  const [spacesList, setSpacesList] = useState([]);
  const [determinationList, setDeterminationList] = useState([]);
  const [uploadInspectionData, setUploadInspectionData] = useState({
    determinationDetId: '',
    comment: '',
    determinationStatus: '',
  });
  const [isChecked, setIsChecked] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [spacesCount, setSpacesCount] = useState(0);
  const {
    propertyAddress,
    unitNumber,
    inspectionId,
    occChecklistId,
    governingCodeSourceId,
  } = route?.params;

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: props => (
        <HeaderBackButton
          {...props}
          style={{marginHorizontal: 0, marginLeft: 0}}
          onPress={() => {
            navigation.navigate('Inspection', {
              setIndexToOne: selectIndex === 0 ? false : true,
            });
          }}
        />
      ),
      headerTitleAlign: 'left',
      headerTitle: `${propertyAddress}`,
      headerStyle: {...commonStyle.hederStyle},
      headerTitleStyle: {...commonStyle.hederTitleStyle},
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigation]);

  useEffect(() => {
    if (isFocused) {
      getInspectedSpaces();
      getSpacesCountAgainstChecklistId();
    } else {
      setSpacesList([]);
      setDeterminationList([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  useBackHandler(() => {
    if (isFocused) {
      navigation.navigate('Inspection', {
        setIndexToOne: selectIndex === 0 ? false : true,
      });
      return true;
    }
  }, [isFocused]);

  const getInspectedSpaces = async () => {
    try {
      dispatch(uiStartLoading());
      const municipality_code = await AsyncStorage.getItem('municipality_code');
      let inspectedSpacesDataList =
        await Database.getInspectedSpacesOfflineListData(
          OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE,
          OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE,
          {municipality_code: municipality_code, inspectionId: inspectionId},
        );
      setSpacesList(inspectedSpacesDataList.data);
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage(
        'danger',
        'Something went wrong in inspection spaces screen',
      );
    }
  };

  const getSpacesCountAgainstChecklistId = async () => {
    try {
      let spacesCountAgainstCheckList =
        await offlineService.getSpacesCountAgainstCheckListId(occChecklistId);
      setSpacesCount(spacesCountAgainstCheckList);
    } catch (error) {
      console.log('ERROR', error);
      Helper.showFlashMessage(
        'danger',
        'Something went wrong while getting count',
      );
    }
  };

  const onPressUploadInspectionHandler = async () => {
    await getDeterminationStatus();
    setIsVisible(true);
  };

  const handleCheck = () => {
    setIsChecked(!isChecked);
  };

  const navigateToAddSpacesScreen = async () => {
    navigation.navigate('AddSpaceScreen', {
      ...route?.params,
      propertyAddress: propertyAddress,
    });
  };

  const deleteInspectionSpace = async inspectedSpaceId => {
    try {
      let isSuccess = await offlineService.offlineDeleteSpace(
        inspectedSpaceId,
        userInfo,
      );
      if (isSuccess) {
        getInspectedSpaces();
      } else {
        Helper.showFlashMessage(
          'danger',
          'something goes wrong while deleting the space.',
        );
      }
    } catch (error) {
      console.log('ERROR', error);
      Helper.showFlashMessage(
        'danger',
        'something goes wrong while deleting the space.',
      );
    }
  };

  // getting list of inspection causes for dropdown
  const getDeterminationStatus = async () => {
    try {
      dispatch(uiStartLoading());
      const municipality_code = await AsyncStorage.getItem('municipality_code');
      let where = `muniCode=${municipality_code}`;
      const determination = await Database.getRecords(
        OFFLINE_TABLE_NAME.INSPECTION_DETERMINATIONS,
        '*',
        where,
      );
      if (determination?.length) {
        let formattedData =
          await MasterDBUtils.prepareInspectionDeterminationObject(
            determination,
          );
        setDeterminationList(formattedData.data);
      } else {
        Helper.showFlashMessage('danger', 'No Determination details found');
      }
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage('danger', error);
    }
  };

  const changeTextHandler = (key, selectedValue) => {
    if (key === 'determinationDetId') {
      setUploadInspectionData({
        ...uploadInspectionData,
        determinationDetId: selectedValue?.id,
        determinationStatus: selectedValue?.name,
      });
    } else {
      setUploadInspectionData({...uploadInspectionData, [key]: selectedValue});
    }
  };

  const uploadInspection = async () => {
    let payload;
    if (isChecked) {
      payload = {comment: uploadInspectionData?.comment.trim()};
    } else {
      payload = {
        ...uploadInspectionData,
        comment: uploadInspectionData?.comment.trim(),
      };

      // payload = uploadInspectionData;
    }
    offlineService
      .offlineUploadInspection(inspectionId, payload, isChecked)
      .then(async response => {
        if (response && (await checkHasInternet())) {
          dispatch(uiStartLoading());
          await Sync.syncInspectionModulesDataDownToUp(
            userInfo?.userId,
            inspectionId,
          );
          await offlineService.getPhotosDataAvailableForSync();
        }
        LangStrings.formatString(LangStrings.LBL_COMPLETE_INSPECTION, [
          propertyAddress,
        ]);
        dispatch(setSelectedIndex(1));
        navigation.navigate('Inspection');
        Helper.showFlashMessage(
          'success',
          LangStrings.formatString(LangStrings.LBL_UPLOAD_SUCCESS_MESSAGE, [
            propertyAddress,
          ]),
        );
        dispatch(uiStopLoading());
      })
      .catch(function (error) {
        dispatch(uiStopLoading());
        return 0;
      });
  };

  const renderInspectionItem = ({item}) => (
    <SpacesCard
      data={item}
      deleteInspectionSpace={deleteInspectionSpace}
      index={selectIndex}
      propertyAddress={propertyAddress}
      unitNumber={unitNumber}
      inspectionId={inspectionId}
      occChecklistId={occChecklistId}
      governingCodeSourceId={governingCodeSourceId}
    />
  );

  return (
    <PageTemplate>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={commonStyle.F1}>
        <View style={[commonStyle.mB40, commonStyle.F1]}>
          <Text style={[commonStyle.mL18, commonStyle.textSecondary]}>
            {`Unit Number ${
              Helper.isUndefined(unitNumber) ? 'PRIMARY' : unitNumber
            }`}{' '}
          </Text>
          <Text
            style={[commonStyle.mL18, commonStyle.mT16, commonStyle.textMain]}>
            {LangStrings.LBL_INSPECTED_SPACES}
          </Text>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={spacesList}
            renderItem={renderInspectionItem}
            keyExtractor={item => item?.inspectedSpaceId}
          />
        </View>
        <BottomButtonGroup
          isSpacesInProgress={
            selectIndex === InspectionStatus.IN_PROGRESS ? true : false
          }
          isSpacesDone={
            selectIndex === InspectionStatus.IN_PROGRESS ? false : true
          }
          // status={2}
          status={selectIndex === InspectionStatus.IN_PROGRESS ? 2 : 1}
          onPress={[
            () => {
              if (
                spacesList?.length !== spacesCount &&
                selectIndex === InspectionStatus.IN_PROGRESS
              ) {
                navigateToAddSpacesScreen();
              } else if (
                spacesList?.length === spacesCount &&
                selectIndex === InspectionStatus.IN_PROGRESS
              ) {
                Helper.showFlashMessage(
                  'danger',
                  'All spaces have already been added.',
                );
              }
            },
            () =>
              navigation.navigate('OverallPhotos', {
                propertyAddress: propertyAddress,
                unitNumber: unitNumber,
                inspectionId: inspectionId,
                index: selectIndex,
                isFromOrdinancesScreen:
                  selectIndex === InspectionStatus.IN_PROGRESS,
              }),
            () => {
              if (selectIndex === InspectionStatus.IN_PROGRESS) {
                onPressUploadInspectionHandler();
              }
            },
          ]}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              name="plus-circle"
              size={16}
              color="white"
              style={{marginRight: 5}}
            />
            <Text style={commonStyle.colorWhite}>
              {LangStrings.LBL_ADD_SPACES}
            </Text>
          </View>
          <View>
            <Text style={commonStyle.colorWhite}>
              {LangStrings.LBL_OVERALL_PHOTOS}
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              name="cloud-upload"
              size={16}
              color="white"
              style={{marginRight: 5}}
            />
            <Text style={commonStyle.colorWhite}>
              {LangStrings.LBL_UPLOAD_INSP}
            </Text>
          </View>
        </BottomButtonGroup>
        {isVisible && (
          <CustomBottomSheet
            isVisible={isVisible}
            onBackdropPress={() => setIsVisible(false)}>
            <View
              style={[
                commonStyle.bgBottomSheet,
                commonStyle.pH18,
                commonStyle.pdT20,
              ]}>
              <View style={commonStyle.fRow}>
                <CustomCheckbox
                  iconType="material-community"
                  uncheckedIcon={'checkbox-blank-outline'}
                  checkedIcon="checkbox-marked"
                  onPress={handleCheck}
                  checked={isChecked}
                  containerStyle={[
                    commonStyle.p0,
                    commonStyle.bgBottomSheet,
                    {margin: 0},
                  ]}
                />
                <Text style={[commonStyle.pdT1, commonStyle.textDark1]}>
                  {LangStrings.LBL_SUBMIT_WITHOUT_DETERMINATION}
                </Text>
              </View>
              <View style={commonStyle.pdB70}>
                {!isChecked && (
                  <View style={commonStyle.mV20}>
                    <Dropdowns
                      title="Determination Status"
                      dropdownPosition="top"
                      data={determinationList}
                      currentCauseId={uploadInspectionData.determinationDetId}
                      onchange={text => {
                        changeTextHandler('determinationDetId', text);
                      }}
                    />
                  </View>
                )}
                <CustomInput
                  renderErrorMessage={false}
                  multiline={true}
                  numberOfLines={3}
                  containerStyle={[commonStyle.pH0, commonStyle.bR7]}
                  inputContainerStyle={[
                    commonStyle.bBTransparent,
                    Platform.OS === 'ios' ? commonStyle.mL16 : commonStyle.mL12,
                  ]}
                  inputStyle={[
                    commonStyle.textAlignVerticalTop,
                    commonStyle.pdT5,
                    commonStyle.ftSize17,
                    commonStyle.textInput,
                  ]}
                  placeholder="Comment goes here..."
                  textInputLabel="Comment"
                  onChangeText={text => {
                    changeTextHandler('comment', text);
                  }}
                />
              </View>
            </View>
            <BottomButtonGroup
              status={0}
              onPress={[
                () => {
                  setIsVisible(false);
                },
                () => {
                  uploadInspection();
                },
              ]}>
              <Text style={commonStyle.colorWhite}>
                {LangStrings.LBL_CANCEL}
              </Text>
              <Text style={commonStyle.colorWhite}>{LangStrings.LBL_SAVE}</Text>
            </BottomButtonGroup>
          </CustomBottomSheet>
        )}
      </KeyboardAvoidingView>
    </PageTemplate>
  );
};

export default SpacesScreen;
