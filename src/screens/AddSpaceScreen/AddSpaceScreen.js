import {View, FlatList} from 'react-native';
import React, {useEffect, useState} from 'react';
import PageTemplate from '../../components/templates/PageTemplate/PageTemplate';
import AddSpaceCard from '../../components/molecules/AddSpaceCard/AddSpaceCard';
import {dynamicCommonStyles} from '../../assets/styles/CommonStyles';
import {useDispatch} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import {uiStartLoading, uiStopLoading} from '../../store/slices/uiSlice';
import {offlineService} from '../../services/offlineService';
import {Helper} from '../../utils/Helper';
import LangStrings from '../../utils/langStrings';

const AddSpaceScreen = ({route}) => {
  const commonStyle = dynamicCommonStyles();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const [addSpacesList, setAddSpacesList] = useState([]);
  const {
    occChecklistId,
    governingCodeSourceId,
    inspectionId,
    propertyAddress,
    index,
  } = route?.params;

  useEffect(() => {
    if (isFocused) {
      fetchExistingInspectionSpaceIds();
    } else {
      setAddSpacesList([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  const getAvailableSpaces = async inspectionSpaceIds => {
    try {
      dispatch(uiStartLoading());
      let inspectedSpacesDataList =
        await offlineService.getAvailableSpacesAgainstInspection(
          inspectionSpaceIds,
          occChecklistId,
        );
      setAddSpacesList(inspectedSpacesDataList?.data);
      dispatch(uiStopLoading());
    } catch (error) {
      console.log('ERROR', error);
      dispatch(uiStopLoading());
      Helper.showFlashMessage(
        'danger',
        LangStrings.LBL_SOMETHING_WRONG_IN_SPACES,
      );
    }
  };

  const fetchExistingInspectionSpaceIds = async () => {
    try {
      let response = await offlineService.getInspectedSpacesAgainstInspection(
        inspectionId,
      );
      if (response) {
        getAvailableSpaces(response);
      }
    } catch (error) {
      console.log('ERROR *******', error);
    }
  };
  const renderInspectionItem = ({item}) => (
    <AddSpaceCard
      data={item}
      governingCodeSourceId={governingCodeSourceId}
      inspectionId={inspectionId}
      propertyAddress={propertyAddress}
      index={index}
      occChecklistId={occChecklistId}
    />
  );
  return (
    <PageTemplate pageTitle={LangStrings.LBL_EXTERIOR_ONLY}>
      <View style={[commonStyle.mB60]}>
        {addSpacesList?.length > 0 ? (
          <FlatList
            data={addSpacesList}
            renderItem={renderInspectionItem}
            keyExtractor={item => item?.checklistSpaceTypeId}
          />
        ) : null}
      </View>
    </PageTemplate>
  );
};

export default AddSpaceScreen;
