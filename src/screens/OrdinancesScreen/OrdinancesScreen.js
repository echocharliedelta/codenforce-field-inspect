import React, {useEffect, useLayoutEffect, useState} from 'react';
import {
  View,
  FlatList,
  KeyboardAvoidingView,
  BackHandler,
  Text,
  Platform,
} from 'react-native';
import PageTemplate from '../../components/templates/PageTemplate/PageTemplate';
import OrdinancesCategories from '../../components/organisms/OrdinancesCategories/OrdinancesCategories';
import {dynamicCommonStyles} from '../../assets/styles/CommonStyles';
import TextInput from '../../components/atoms/TextInput/TextInput';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import OrdinancesCard from '../../components/molecules/OrdinancesCard/OrdinancesCard';
import LangStrings from '../../utils/langStrings';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../../config/config';
import {uiStartLoading, uiStopLoading} from '../../store/slices/uiSlice';
import {Helper} from '../../utils/Helper';
import Database from '../../database/Database';
import {offlineService} from '../../services/offlineService';
import FindingsBottomsheet from '../../components/molecules/FindingsBottomsheet/FindingsBottomsheet';
import CustomDialog from '../../components/molecules/CustomDialog/CustomDialog';
import Buttons from '../../components/atoms/Buttons/Buttons';
import {InspectionStatus} from '../../utils/constants';
import {HeaderBackButton} from '@react-navigation/elements';
import {useHeaderHeight} from '@react-navigation/elements';

const OrdinancesScreen = ({route}) => {
  const commonStyle = dynamicCommonStyles();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {userInfo, selectIndex} = useSelector(state => state?.auth);

  const {
    inspectedSpaceId,
    inspectionId,
    governingCodeSourceId = '',
    viewOnly = false,
    spaceTitle = '',
    index = 0,
    propertyAddress = '',
    unitNumber = '',
    occChecklistId,
    checklistSpaceTypeId,
  } = route?.params;

  const [selectedIndex, setSelectedIndex] = useState(0);
  const [Checked, setChecked] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [ordinancesList, setOrdinanceList] = useState([]);
  const [categoriesList, setCategoriesList] = useState([]);
  const [inspectedSpaceElementId, setInspectedSpaceElementId] = useState();
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredOrdinances, setFilteredOrdinances] = useState([]);
  const [currentOrdinanceDetails, setCurrentOrdinanceDetails] = useState([]);
  const [extraData, setExtraData] = useState(false);
  const [showConfirmationDialog, setShowConfirmationDialog] = useState(false);
  const [confirmationMessage, setConfirmationMessage] = useState('');
  const [bulkActionState, setBulkActionState] = useState(false);
  const [ordinanceIdsState, setOrdinanceIdsState] = useState();
  const [actionTypeState, setActionTypeState] = useState('');
  const height = useHeaderHeight();

  useLayoutEffect(() => {
    const headerStyle = {...commonStyle.hederStyle};
    const headerTitleStyle = {...commonStyle.hederTitleStyle};
    if (viewOnly) {
      navigation.setOptions({
        headerBackTitle: Platform.OS === 'ios' && 'Back',

        headerTitle: 'Ordinances to Inspect',
        headerStyle: {...headerStyle},
        headerTitleStyle: {...headerTitleStyle},
      });
    } else {
      navigation.setOptions({
        headerLeft: props => (
          <HeaderBackButton
            {...props}
            onPress={() => {
              if (viewOnly === false) {
                navigation.navigate('Spaces', {
                  index: index,
                  inspectedSpaceId: inspectedSpaceId,
                  inspectionId: inspectionId,
                  governingCodeSourceId: governingCodeSourceId,
                  viewOnly: viewOnly,
                  spaceTitle: spaceTitle,
                  propertyAddress: propertyAddress,
                  unitNumber: unitNumber,
                  occChecklistId: occChecklistId,
                });
              } else {
                navigation.goBack();
              }
            }}
          />
        ),
        headerTitle: `${spaceTitle}`,
        headerStyle: {...headerStyle},
        headerTitleStyle: {...headerTitleStyle},
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigation, viewOnly, route?.params]);

  useEffect(() => {
    if (isFocused) {
      if (viewOnly) {
        getSpacesOrdinanceAndCategoriesDetails(checklistSpaceTypeId);
      } else {
        getSpacesOrdinanceAndCategories();
        setChecked(true);
      }
    } else {
      setOrdinanceList([]);
      setCategoriesList([]);
      setSelectedIndex(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  useEffect(() => {
    filterInspections();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm, ordinancesList]);

  useEffect(() => {
    const backAction = () => {
      if (viewOnly === false) {
        navigation.navigate('Spaces', {
          index: selectIndex,
          inspectedSpaceId: inspectedSpaceId,
          inspectionId: inspectionId,
          governingCodeSourceId: governingCodeSourceId,
          viewOnly: viewOnly,
          spaceTitle: spaceTitle,
          propertyAddress: propertyAddress,
          unitNumber: unitNumber,
          occChecklistId: occChecklistId,
        });
      } else {
        navigation.goBack();
      }
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => {
      backHandler.remove();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  // getting ordinance categories
  const getSpacesOrdinanceAndCategories = async (guideEntryId = null) => {
    try {
      dispatch(uiStartLoading());
      let response = await Database.getOrdinanceAndCategories(
        OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
        OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
        {inspectedSpaceId: inspectedSpaceId, guideEntryId: guideEntryId},
      );
      setOrdinanceList(response.ordinancesList);
      Helper.isUndefined(guideEntryId) &&
        setCategoriesList(response.categoryList);
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage(
        'danger',
        LangStrings.LBL_SOMETHING_WRONG_IN_INSP,
      );
    }
  };

  // getting ordinances details
  const getSpacesOrdinanceAndCategoriesDetails = async (
    checklistSpaceTypeId,
    guideEntryId = null,
  ) => {
    try {
      dispatch(uiStartLoading());
      let response = await Database.getOrdinanceAndCategoriesDetails(
        OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS,
        OFFLINE_TABLE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS,
        {
          governingCodeSourceId: governingCodeSourceId,
          guideEntryId: guideEntryId,
          checklistSpaceTypeId: checklistSpaceTypeId,
        },
      );

      setOrdinanceList(response.ordinancesList);

      Helper.isUndefined(guideEntryId) &&
        setCategoriesList(response.categoryList);
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage(
        'danger',
        LangStrings.LBL_SOMETHING_WRONG_IN_INSP,
      );
    }
  };

  // filtering categories based on user search
  const filterInspections = () => {
    const lowerCaseSearch = searchTerm.toLowerCase();
    const filteredData = ordinancesList?.filter(item => {
      return item.headerStringStatic
        .toString()
        .toLowerCase()
        .includes(lowerCaseSearch);
    });
    setFilteredOrdinances(filteredData);
  };

  const onPress = (spaceElementId, item, selectedValue) => {
    setActionTypeState(
      !Helper.isUndefined(item?.complianceGrantedTS)
        ? 'inspectionPassed'
        : !Helper.isUndefined(item?.lastInspectedTS)
        ? 'inspectionViolated'
        : 'inspectionNotInspected',
    );
    setCurrentOrdinanceDetails(item);
    setInspectedSpaceElementId(spaceElementId);
    setIsVisible(!isVisible);
  };

  const RenderOrdinancesCard = React.memo(
    ({item, index, Checked, onPress, batchPassedOrdinance, viewOnly}) => {
      return (
        <OrdinancesCard
          data={item}
          index={index}
          Checked={Checked}
          onPress={onPress}
          batchPassedOrdinance={batchPassedOrdinance}
          viewOnly={viewOnly}
        />
      );
    },
  );

  const checkBoxClick = () => {
    setChecked(!Checked);
  };

  const handleTabPress = (index, guideEntryId) => {
    setSelectedIndex(index);
    if (viewOnly) {
      getSpacesOrdinanceAndCategoriesDetails(
        checklistSpaceTypeId,
        guideEntryId,
      );
    } else {
      getSpacesOrdinanceAndCategories(guideEntryId);
    }
  };

  const getConfirmationMessage = async actionType => {
    return actionType === 'inspectionPassed'
      ? LangStrings.LBL_CONFIRM_PASS_INSP
      : LangStrings.LBL_CONFIRM_VIOLATE_INSP;
  };

  const setOrdinanceIds = async (isBulkAction, inspectedSpaceElementId) => {
    let ordinanceIds;
    if (isBulkAction === true) {
      ordinanceIds = await getInspectionOrdinanceIds();
      setOrdinanceIdsState(ordinanceIds);
    } else {
      ordinanceIds = [inspectedSpaceElementId];
      setOrdinanceIdsState(ordinanceIds);
    }
    return ordinanceIds;
  };

  const batchPassedOrdinance = async (
    isBulkAction = false,
    actionType,
    inspectedSpaceElementId = null,
    data,
  ) => {
    setBulkActionState(isBulkAction);
    setActionTypeState(actionType);
    try {
      // Set ordinanceIds based on bulk action
      const ordinanceIds = await setOrdinanceIds(
        isBulkAction,
        inspectedSpaceElementId,
      );
      // Show the confirmation dialog for bulk actions only
      if (isBulkAction === true) {
        const confirmationMessage = await getConfirmationMessage(actionType);
        setConfirmationMessage(confirmationMessage);
        setShowConfirmationDialog(true);
      } else {
        // For single actions, directly handle the confirmation
        handleBatchConfirm(isBulkAction, actionType, ordinanceIds);
        if (actionType !== 'inspectionNotInspected') {
          setCurrentOrdinanceDetails(data);
          setInspectedSpaceElementId(inspectedSpaceElementId);
          if (Checked) {
            setIsVisible(!isVisible);
          }
        }
      }
    } catch (error) {
      console.error(LangStrings.LBL_ERROR_IN_BATCHPASSORDINANCE, error);
    }
  };

  const handleBatchConfirm = async (isBulkAction, actionType, ordinanceIds) => {
    if (ordinanceIds.length) {
      try {
        const isSuccess = await offlineService.offlineOrdinancePassFail(
          isBulkAction,
          ordinanceIds.join(', '),
          actionType,
          userInfo,
        );
        if (isSuccess) {
          bulkEditOrdinanceListStatus(actionType, isBulkAction, ordinanceIds);
          setShowConfirmationDialog(false);
          if (isBulkAction && actionType === 'inspectionViolated') {
            Helper.showFlashMessage(
              'success',
              LangStrings.LBL_VIOLATED_INSP_SUCCESS,
            );
          } else if (isBulkAction && actionType === 'inspectionPassed') {
            Helper.showFlashMessage(
              'success',
              LangStrings.LBL_PASSED_INSP_SUCCESS,
            );
          }
        }
      } catch (error) {
        console.log(LangStrings.LBL_ERROR_IN_BATCHPASSORDINANCE, error);
      }
    } else {
      setShowConfirmationDialog(false);
      Helper.showFlashMessage(
        'danger',
        LangStrings.LBL_NO_ORDINANCES_AVAILABLE,
      );
    }
  };

  const updateOrdinanceStatus = (
    deepCopyArray,
    ordinanceIds,
    actionType,
    isBulkAction,
  ) => {
    return deepCopyArray.map(ordinance => {
      if (
        ordinance?.inspectedSpaceElementId === ordinanceIds[0] ||
        (isBulkAction && Helper.isUndefined(ordinance.lastInspectedTS))
      ) {
        const commonUpdate = {
          lastInspectedByUserId: userInfo?.userId,
          lastInspectedTS: Helper.getCurrentUTCDateTime(),
        };

        switch (actionType) {
          case 'inspectionPassed':
            return {
              ...ordinance,
              ...commonUpdate,
              complianceGrantedByUserId: userInfo?.userId,
              complianceGrantedTS: Helper.getCurrentUTCDateTime(),
            };
          case 'inspectionViolated':
            return {
              ...ordinance,
              ...commonUpdate,
              complianceGrantedByUserId: '',
              complianceGrantedTS: '',
            };
          default:
            return {
              ...ordinance,
              lastInspectedByUserId: '',
              lastInspectedTS: '',
              complianceGrantedByUserId: '',
              complianceGrantedTS: '',
            };
        }
      } else {
        return ordinance;
      }
    });
  };

  const bulkEditOrdinanceListStatus = async (
    actionType,
    isBulkAction = false,
    ordinanceIds,
  ) => {
    if (isBulkAction) {
      const updatedOrdinanceStatus = updateOrdinanceStatus(
        ordinancesList,
        ordinanceIds,
        actionType,
        isBulkAction,
      );
      setOrdinanceList([...updatedOrdinanceStatus]);
      setExtraData(!extraData);
    } else {
      updateSingleOrdinance(actionType, ordinanceIds);
    }
  };

  const updateSingleOrdinance = (actionType, ordinanceIds, isBulkAction) => {
    const updatedOrdinanceStatus = updateOrdinanceStatus(
      ordinancesList,
      ordinanceIds,
      actionType,
      isBulkAction,
    );
    setOrdinanceList([...updatedOrdinanceStatus]);
    setExtraData(!extraData);
  };

  const getInspectionOrdinanceIds = async () => {
    const quotedElements = ordinancesList
      .filter(ordinance => Helper.isNull(ordinance?.lastInspectedTS))
      .map(ordinance => `'${ordinance.inspectedSpaceElementId}'`);
    // .join(', ');
    return quotedElements;
  };

  return (
    <PageTemplate pageTitle={viewOnly && propertyAddress}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={commonStyle.F1}
        keyboardVerticalOffset={height + 20}>
        <View
          style={[
            commonStyle.F1,
            commonStyle.fRow,
            index === InspectionStatus.IN_PROGRESS && viewOnly === false
              ? commonStyle.pdB20
              : null,
          ]}>
          <OrdinancesCategories
            withTools={!viewOnly}
            checkboxValue={Checked}
            onCheckboxClick={checkBoxClick}
            selectedIndex={selectedIndex}
            tabs={categoriesList}
            onPress={handleTabPress}
            batchPassedOrdinance={batchPassedOrdinance}
            index={index}
          />
          <View style={[commonStyle.mT20, commonStyle.F1]}>
            {viewOnly && (
              <Text
                style={[
                  commonStyle.textDark1,
                  commonStyle.ftSize13,
                  commonStyle.textUpper,
                ]}>
                {spaceTitle} -Ordinances
              </Text>
            )}
            <TextInput
              onChangeText={text => setSearchTerm(text)}
              containerStyle={[
                commonStyle.pH0,
                commonStyle.mT1,
                commonStyle.bgSearchInput,
                commonStyle.h35,
              ]}
              inputContainerStyle={[
                commonStyle.bBTransparent,
                commonStyle.mL11,
                commonStyle.mB25,
              ]}
              inputStyle={[
                commonStyle.h36,
                commonStyle.minH35,
                commonStyle.textLight,
                commonStyle.ftSize14,
                commonStyle.mB10,
              ]}
              placeholderTextColor={'rgba(255, 255, 255, 0.50)'}
              leftIconContainerStyle={[commonStyle.searchInputIcon]}
              placeholder={LangStrings.LBL_SEARCH}
              value={searchTerm}
              leftIcon={
                <Icon
                  name={'magnify'}
                  size={17}
                  style={[commonStyle.textDark3, commonStyle.opacity50]}
                />
              }
            />

            <FlatList
              data={filteredOrdinances?.length && filteredOrdinances}
              renderItem={({item}) => (
                <RenderOrdinancesCard
                  item={item}
                  index={index}
                  Checked={Checked}
                  onPress={onPress}
                  batchPassedOrdinance={batchPassedOrdinance}
                  viewOnly={viewOnly}
                />
              )}
              keyExtractor={item =>
                item.elementId +
                item?.inspectedSpaceElementId +
                Helper.getOrdinanceInspectionStatus(item).toString()
              }
              contentContainerStyle={commonStyle.mB40}
            />
          </View>
        </View>

        {showConfirmationDialog && (
          <CustomDialog
            isVisible={showConfirmationDialog}
            onBackdropPress={() => setShowConfirmationDialog(false)}
            firstAction={() => {
              handleBatchConfirm(
                bulkActionState,
                actionTypeState,
                ordinanceIdsState,
              );
            }}
            secondAction={() => setShowConfirmationDialog(false)}
            title={confirmationMessage}
          />
        )}

        {index === InspectionStatus.IN_PROGRESS && viewOnly === false ? (
          <Buttons
            title={LangStrings.LBL_COMPLETE_INSP}
            containerStyle={[commonStyle.addSpaceButton]}
            buttonStyle={[commonStyle.bgBlue]}
            titleStyle={[commonStyle.ftSFpro]}
            onPress={() => {
              navigation.navigate('Spaces', route?.params);
            }}
          />
        ) : null}
      </KeyboardAvoidingView>
      {isVisible && (
        <FindingsBottomsheet
          index={index}
          showViolationSeverity={
            actionTypeState == 'inspectionViolated' ? true : false
          }
          isVisible={isVisible}
          onBackdropPress={() => setIsVisible(false)}
          inspectedSpaceElementId={inspectedSpaceElementId}
          inspectionId={inspectionId}
          inspectedSpaceId={inspectedSpaceId}
          currentOrdinanceDetails={currentOrdinanceDetails}
          propertyAddress={propertyAddress}
          unitNumber={unitNumber}
        />
      )}
    </PageTemplate>
  );
};

export default OrdinancesScreen;
