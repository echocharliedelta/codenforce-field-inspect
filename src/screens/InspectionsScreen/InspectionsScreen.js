import TextInput from '../../components/atoms/TextInput/TextInput';
import PageTemplate from '../../components/templates/PageTemplate/PageTemplate';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {dynamicCommonStyles} from '../../assets/styles/CommonStyles';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import InspectionsCard from '../../components/molecules/InspectionCard/InspectionsCard';
import {Tab, TabView} from '@rneui/themed';
import {useEffect, useLayoutEffect, useState} from 'react';
import LangStrings from '../../utils/langStrings';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../../config/config';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import Database from '../../database/Database';
import {Helper} from '../../utils/Helper';
import {useDispatch, useSelector} from 'react-redux';
import {uiStartLoading, uiStopLoading} from '../../store/slices/uiSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Buttons from '../../components/atoms/Buttons/Buttons';
import CustomDialog from '../../components/molecules/CustomDialog/CustomDialog';
import {setSelectedIndex, signOutUser} from '../../store/slices/authSlice';
import {offlineService} from '../../services/offlineService';
import {Sync} from '../../utils/Sync';
import moment from 'moment';
import {useNetInfo} from '@react-native-community/netinfo';
import {useHeaderHeight} from '@react-navigation/elements';

const InspectionScreen = ({route}) => {
  const {userInfo, selectIndex} = useSelector(state => state?.auth);

  const commonStyle = dynamicCommonStyles();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const NetInfo = useNetInfo();
  const height = useHeaderHeight();

  const [inspectionData, setInspectionData] = useState([]);
  const [isShowDialogForLogout, setIsShowDialogForLogout] = useState(false);
  const [lastSyncDateTime, setLastSyncDateTime] = useState('N/A');
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredInspectionData, setFilteredInspectionData] = useState([]);
  const [isInternetConnected, setIsInternetConnected] = useState(null);
  const [reloadData, setReloadData] = useState(0);


  useLayoutEffect(() => {
    navigation.setOptions({
      headerShadowVisible: false,
      headerStyle: {...commonStyle.hederStyle},
      headerTitleStyle: {...commonStyle.hederTitleStyle},
      tabBarLabel: 'Inspections',
      tabBarIcon: () => (
        <Icon name={'home-search'} size={27} color={'#CDE161'} />
      ),
      headerRight: () => (
        <Buttons
          type={'clear'}
          title={'Logout'}
          titleStyle={[
            commonStyle.ftSize16,
            commonStyle.ftWeight600,
            commonStyle.colorLogout,
          ]}
          onPress={() => {
            setIsShowDialogForLogout(true);
          }}
        />
      ),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigation]);

  useEffect(() => {
    if (isFocused) {
      setSearchTerm('');
      setIsInternetConnected(NetInfo?.isConnected);
    }
  }, [NetInfo, isFocused]);

  // Call filterInspections whenever search term changes
  useEffect(() => {
    filterInspections();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm, inspectionData]);

  // Function to filter inspection data based on search term
  const filterInspections = () => {
    const lowerCaseSearch = searchTerm.toLowerCase();
    const filteredData = inspectionData?.filter(
      item =>
        item.propertyAddress.toLowerCase().includes(lowerCaseSearch) ||
        item.unitNumber.toString().toLowerCase().includes(lowerCaseSearch) ||
        item.inspectorName.toLowerCase().includes(lowerCaseSearch) ||
        item.checklistName.toLowerCase().includes(lowerCaseSearch),
    );
    setFilteredInspectionData(filteredData);
  };
  const onBackdropPress = () => {
    setIsShowDialogForLogout(false);
  };

  const logOutUser = () => {
    dispatch(signOutUser());
  };

  useEffect(() => {
    if (isFocused) {
      getInspections();
      getLastSyncDateTime();
    }
    return () => setInspectionData([]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused, selectIndex, reloadData]);

  const getInspections = async index => {
    const municipalityCode = await AsyncStorage.getItem('municipality_code');
    const authPeriod = await AsyncStorage.getItem('auth_period');
    try {
      dispatch(uiStartLoading());
      let inspectionDataList = await Database.getInspectionOfflineListData(
        OFFLINE_MODULE_NAME.INSPECTIONS,
        OFFLINE_TABLE_NAME.INSPECTIONS,
        {
          municipality_code: municipalityCode,
          authPeriod: authPeriod,
          isInspectionDone: selectIndex === 0 ? 0 : 1,
        },
      );
      setInspectionData(inspectionDataList.data);
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage(
        'danger',
        LangStrings.LBL_SOMETHING_WRONG_IN_INSP,
      );
    }
  };

  const deleteInitiatedInspection = async inspectionId => {
    try {
      let isSuccess = await offlineService.deleteInitiatedInspection(
        inspectionId,
      );
      if (isSuccess) {
        setReloadData(reloadData + 1);
        Helper.showFlashMessage(
          'success',
          LangStrings.LBL_INSPECTION_DELETE_SUCCESS,
        );
      }
    } catch (error) {
      console.log('Error while deleting the inspection');
    }
  };

  const renderInspectionItem = ({item}) => (
    <InspectionsCard
      data={item}
      index={selectIndex}
      deleteInitiatedInspection={deleteInitiatedInspection}
    />
  );

  const syncInspections = async () => {
    try {
      dispatch(uiStartLoading());
      await Sync.syncInspectionModulesDataDownToUp(userInfo?.userId);
      await offlineService.getPhotosDataAvailableForSync();
      getLastSyncDateTime();
      getInspections();
      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
    }
  };

  const getLastSyncDateTime = async () => {
    let lastSyncDate = await AsyncStorage.getItem('lastSyncDateTime');
    if (!Helper.isUndefined(lastSyncDate)) {
      setLastSyncDateTime(
        moment(lastSyncDate).format('ddd DD MMM YYYY, HH:mm'),
      );
    } else {
      setLastSyncDateTime('N/A');
    }
  };
  const indexHandler = e => {
    setSearchTerm('');
    dispatch(setSelectedIndex(e));
  };

  return (
    <PageTemplate pageTitle={selectIndex === 0 ? 'in-progress' : 'done'}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={commonStyle.F1}
        enabled
        keyboardVerticalOffset={height + 5}>
        <TextInput
          containerStyle={[
            commonStyle.pH0,
            commonStyle.bgSearchInput,
            commonStyle.h35,
          ]}
          inputContainerStyle={[
            commonStyle.bBTransparent,
            commonStyle.mL11,
            commonStyle.mB25,
          ]}
          inputStyle={[
            commonStyle.h36,
            commonStyle.minH35,
            commonStyle.textWhite,
            commonStyle.ftSize14,
            commonStyle.mB10,
          ]}
          placeholderTextColor={'rgba(255, 255, 255, 0.50)'}
          leftIconContainerStyle={[commonStyle.searchInputIcon]}
          placeholder={LangStrings.LBL_SEARCH}
          leftIcon={
            <Icon
              name={'magnify'}
              size={17}
              style={[commonStyle.textDark3, commonStyle.opacity50]}
            />
          }
          value={searchTerm}
          onChangeText={text => setSearchTerm(text)}
        />
        {selectIndex === 1 ? (
          <View
            style={[
              commonStyle.bgPrimaryLight,
              commonStyle.h40,
              commonStyle.dFlex,
              commonStyle.fRow,
              commonStyle.alignItemsCenter,
              commonStyle.justifyContentSpaceBetween,
            ]}>
            <View
              style={[
                commonStyle.dFlex,
                commonStyle.fRow,
                commonStyle.alignItemsCenter,
              ]}>
              <Text
                style={[
                  commonStyle.mL18,
                  commonStyle.textSecondary,
                  commonStyle.ftWeight600,
                  commonStyle.ftSize14,
                ]}>
                {LangStrings.LBL_LAST_SYNC}
              </Text>
              <Text
                style={[
                  commonStyle.mL5,
                  commonStyle.textSecondary,
                  commonStyle.ftWeight500,
                  commonStyle.ftSize14,
                ]}>
                {lastSyncDateTime}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                borderRadius: 50,
                paddingHorizontal: 3,
                paddingVertical: 3,
                marginRight: 18,
                backgroundColor: isInternetConnected ? '#F58F31' : '#787878',
              }}
              onPress={() => {
                syncInspections();
              }}>
              <Icon
                name="sync"
                size={24}
                style={{
                  color: 'white',
                }}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        <>
          <TabView
            value={selectIndex}
            onChange={e => indexHandler(e)}
            animationType="spring">
            <TabView.Item style={{width: '100%'}}>
              <FlatList
                showsVerticalScrollIndicator={false}
                data={
                  filteredInspectionData?.length
                    ? filteredInspectionData
                    : searchTerm === ''
                    ? inspectionData
                    : null
                }
                renderItem={renderInspectionItem}
                keyExtractor={item => item.inspectionId}
              />
            </TabView.Item>
            <TabView.Item style={{width: '100%'}}>
              <FlatList
                data={
                  filteredInspectionData?.length
                    ? filteredInspectionData
                    : searchTerm === ''
                    ? inspectionData
                    : null
                }
                renderItem={renderInspectionItem}
                keyExtractor={item => item.inspectionId}
              />
            </TabView.Item>
          </TabView>
          <Tab
            value={selectIndex}
            onChange={e => indexHandler(e)}
            indicatorStyle={{
              height: 0,
            }}
            buttonStyle={commonStyle.pdT1}
            style={[commonStyle.mB48, commonStyle.h40]}
            variant="primary">
            <Tab.Item
              containerStyle={active => ({
                backgroundColor: active ? '#00B4EA' : '#00465C',
              })}
              title={LangStrings.LBL_IN_PROGRESS}
              titleStyle={{
                fontSize: 13,
                fontFamily: 'SF Pro',
                textTransform: 'uppercase',
              }}
            />
            <Tab.Item
              containerStyle={active => ({
                backgroundColor: active ? '#00B4EA' : '#00465C',
              })}
              title={LangStrings.LBL_DONE}
              titleStyle={{
                fontSize: 13,
                fontFamily: 'SF Pro',
                textTransform: 'uppercase',
              }}
            />
          </Tab>
        </>
        {isShowDialogForLogout && (
          <CustomDialog
            isVisible={isShowDialogForLogout}
            onBackdropPress={onBackdropPress}
            firstAction={logOutUser}
            secondAction={setIsShowDialogForLogout}
            title={LangStrings.LBL_LOG_OUT_CONFIRMATION}
          />
        )}
      </KeyboardAvoidingView>
    </PageTemplate>
  );
};

export default InspectionScreen;
