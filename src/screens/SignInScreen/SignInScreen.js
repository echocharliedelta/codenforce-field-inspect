import AuthTemplate from '../../components/templates/AuthTemplate/AuthTemplate';
import SignInForm from '../../components/molecules/SignInForm/SignInForm';
import LangStrings from '../../utils/langStrings';

const SignInScreen = () => {
  return (
    <AuthTemplate AuthTemplateHeading={LangStrings.LBL_LOGIN}>
      <SignInForm />
    </AuthTemplate>
  );
};

export default SignInScreen;
