import React, {useEffect, useState} from 'react';
import {Dimensions, View, Text, Platform} from 'react-native';
import {AutoDragSortableView} from 'react-native-drag-sort';
import ImageCard from '../../components/molecules/ImageCard/ImageCard';
import {dynamicCommonStyles} from '../../assets/styles/CommonStyles';
import PageTemplate from '../../components/templates/PageTemplate/PageTemplate';
import OverallPhotosCard from '../../components/molecules/OverallPhotosCard/OverallPhotosCard';
import BottomButtonGroup from '../../components/atoms/BottomButtonGroup/BottomButtonGroup';
import {useLayoutEffect} from 'react';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {offlineService} from '../../services/offlineService';
import {useDispatch, useSelector} from 'react-redux';
import {uiStartLoading, uiStopLoading} from '../../store/slices/uiSlice';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../../config/config';
import {Helper} from '../../utils/Helper';
import Database from '../../database/Database';
import ImageDialog from '../../components/molecules/ImageDialog/ImageDialog';
import LangStrings from '../../utils/langStrings';
import CustomDialog from '../../components/molecules/CustomDialog/CustomDialog';

const {width} = Dimensions.get('window');
const parentWidth = width - 8;
const childrenWidthPercentage = 0.22; // 20% of the screen width
const childrenWidth = width * childrenWidthPercentage;
const childrenHeight = 88;
const headerViewHeight = 100;
const bottomViewHeight = 40;

const OverallPhotosScreen = ({route}) => {
  const commonStyle = dynamicCommonStyles();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const {userInfo} = useSelector(state => state?.auth);
  const {
    propertyAddress = '',
    inspectionId = '',
    unitNumber = '',
    index,
    ordinancePhotosIds,
  } = route?.params;

  const [overAllPhotosData, setOverAllPhotosData] = useState([]);
  const [overallPhotosCount, setOverAllPhotosCount] = useState(0);
  const [ordinanceSelectedPhotos, setOrdinanceSelectedPhotos] = useState([]);
  const [sortedImages, setSortedImages] = useState([]);
  const [imageDialogVisible, setImageDialogVisible] = useState(false);
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const [
    isShowDialogForCancelAddInspection,
    setIsShowDialogForCancelAddInspection,
  ] = useState(false);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerBackTitle: Platform.OS === 'ios' && 'Back',
      headerTitle: `${propertyAddress}`,
      headerStyle: {...commonStyle.hederStyle},
      headerTitleStyle: {...commonStyle.hederTitleStyle},
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigation]);

  useEffect(() => {
    if (isFocused) {
      getInspectionsPhotos();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  const getInspectionsPhotos = async () => {
    try {
      dispatch(uiStartLoading());
      let inspectionOverallPhotoList =
        await Database.getInspectionOverallPhotosListData(
          OFFLINE_MODULE_NAME.INSPECTIONS_BLOB_BYTE,
          OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
          {inspectionId: inspectionId, ordinancePhotosIds: ordinancePhotosIds},
        );
      if (
        !Helper.isUndefined(inspectionOverallPhotoList.data) &&
        inspectionOverallPhotoList.data?.length > 0
      ) {
        setOverAllPhotosData(inspectionOverallPhotoList.data);
        setOverAllPhotosCount(inspectionOverallPhotoList.data?.length);
      }
      dispatch(uiStopLoading());
    } catch (error) {
      console.log('ERROR ********', error);
      dispatch(uiStopLoading());
      Helper.showFlashMessage(
        'danger',
        'Something went wrong in inspection screen',
      );
    }
  };

  const renderItem = (item, imageIndex) => {
    const isSelected = ordinanceSelectedPhotos.includes(item.bytesId);
    return (
      <ImageCard
        image={item}
        showNumber={imageIndex + 1}
        deleteSelectedPhoto={deletePhotoFromOverall}
        isOrdinanceSelectedPhoto={isSelected}
        isFromFindings={false}
        deletable={index}
      />
    );
  };

  const uploadSelectedImages = async (images, photo_type) => {
    const promises = [];
    if (images.length) {
      for (let i = 0; i < images.length; i++) {
        promises.push(
          offlineService.offlineAddInspectionPhoto(
            images[i],
            inspectionId,
            userInfo,
          ),
        );
      }
      Promise.all(promises)
        .then(() => {
          // All operations inside the loop have completed
          getInspectionsPhotos();
        })
        .catch(error => {
          dispatch(uiStopLoading());
          // Handle errors if any of the promises reject
          console.error(error);
        });
    }
  };

  const deletePhotoFromOverall = async bytesId => {
    let isSuccess = offlineService.offlineDeletePhoto(
      bytesId,
      OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
    );
    if (isSuccess) {
      setOverAllPhotosData([]);
      getInspectionsPhotos();
    }
  };

  const updateSortedImages = async () => {
    const promises = [];
    // if (sortedImages.length) {
    for (let i = 0; i < sortedImages.length; i++) {
      promises.push(
        offlineService.updateSortedImages(i + 1, inspectionId, sortedImages[i]),
      );
    }
    Promise.all(promises)
      .then(() => {
        navigation.goBack();
      })
      .catch(error => {
        // Handle errors if any of the promises reject
        console.error(error);
      });
    // }
  };

  const expandedImage = index => {
    setCurrentImageIndex(index);
    setImageDialogVisible(true);
  };

  return (
    <PageTemplate>
      <Text style={[commonStyle.mL18, commonStyle.textDark2]}>
        Unit Number {unitNumber ? unitNumber : 'PRIMARY'}
      </Text>
      <View
        style={[
          commonStyle.dFlex,
          commonStyle.fRow,
          commonStyle.alignItemsCenter,
        ]}>
        <Text
          style={[commonStyle.mL18, commonStyle.mT10, commonStyle.textMain]}>
          OVERALL PHOTOS
        </Text>

        <Text style={commonStyle.overallPhotosBadge}>{overallPhotosCount}</Text>
      </View>
      {index === 0 && (
        <View
          style={[
            commonStyle.mL18,
            commonStyle.mT20,
            commonStyle.dFlex,
            commonStyle.fRow,
            commonStyle.mB10,
          ]}>
          <OverallPhotosCard
            type="camera"
            uploadSelectedImages={uploadSelectedImages}
          />
          <OverallPhotosCard
            type="gallery"
            uploadSelectedImages={uploadSelectedImages}
          />
        </View>
      )}
      <View
        style={[
          commonStyle.bB1,
          commonStyle.bBPrimary,
          commonStyle.pdB20,
          commonStyle.F1,
          commonStyle.mB20,
        ]}>
        <AutoDragSortableView
          sortable={index === 1 ? false : true}
          dataSource={overAllPhotosData}
          parentWidth={parentWidth}
          childrenWidth={childrenWidth}
          childrenHeight={childrenHeight}
          marginChildrenBottom={9}
          marginChildrenRight={18}
          marginChildrenLeft={18}
          onClickItem={(data, item, index) => {
            if (ordinanceSelectedPhotos.includes(item.bytesId)) {
              // If already selected, remove from the array
              setOrdinanceSelectedPhotos(
                ordinanceSelectedPhotos.filter(id => id !== item.bytesId),
              );
            } else {
              // If not selected, add to the array
              setOrdinanceSelectedPhotos([
                ...ordinanceSelectedPhotos,
                item.bytesId,
              ]);
            }
            expandedImage(index);
          }}
          onDataChange={newData => {
            setSortedImages(newData.map(obj => obj.bytesId));
            setOverAllPhotosData(newData);
          }}
          keyExtractor={(item, index) => item.bytesId}
          renderItem={(item, index) => {
            return renderItem(item, index);
          }}
          headerViewHeight={headerViewHeight}
          bottomViewHeight={bottomViewHeight}
        />
      </View>
      {index === 0 && sortedImages.length ? (
        <BottomButtonGroup
          status={0}
          onPress={[
            () => setIsShowDialogForCancelAddInspection(true),
            () => updateSortedImages(),
          ]}>
          <Text style={commonStyle.colorWhite}>{LangStrings.LBL_CANCEL}</Text>
          <Text style={commonStyle.colorWhite}>{LangStrings.LBL_SAVE}</Text>
        </BottomButtonGroup>
      ) : null}
      <ImageDialog
        currentImageIndex={currentImageIndex}
        image={overAllPhotosData}
        isVisible={imageDialogVisible}
        onBackdropPress={() => {
          setImageDialogVisible(false);
        }}
      />
      {isShowDialogForCancelAddInspection ? (
        <CustomDialog
          isVisible={isShowDialogForCancelAddInspection}
          onBackdropPress={() => {
            setIsShowDialogForCancelAddInspection(false);
          }}
          firstAction={() => {
            navigation.navigate('Inspection');
          }}
          secondAction={() => {
            setIsShowDialogForCancelAddInspection(false);
          }}
          title={LangStrings.LBL_CONFIRM_DISCARD_PHOTOS}
        />
      ) : null}
    </PageTemplate>
  );
};

export default OverallPhotosScreen;
