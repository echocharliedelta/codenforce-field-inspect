import React, {useEffect} from 'react';
import {View, FlatList} from 'react-native';
import PageTemplate from '../../components/templates/PageTemplate/PageTemplate';
import SessionsAccordion from '../../components/molecules/SessionsAccordion/SessionsAccordion';
import LangStrings from '../../utils/langStrings';
import {dynamicCommonStyles} from '../../assets/styles/CommonStyles';
import {getUserSessions} from '../../store/middleware/auth';
import {useDispatch, useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import {checkHasInternet} from '../../utils/functions/checkHasInternet';

const SessionScreen = () => {
  const commonStyle = dynamicCommonStyles();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const {userSessions} = useSelector(state => state?.auth);

  useEffect(() => {
    if (isFocused) {
      getSessions();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  const getSessions = async () => {
    if (await checkHasInternet()) {
      dispatch(getUserSessions());
    }
  };

  const renderAccordionItem = ({item}) => <SessionsAccordion item={item} />;

  return (
    <PageTemplate pageTitle={LangStrings.LBL_LOAD_SESSION}>
      <View style={commonStyle.pdB14}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={userSessions}
          renderItem={renderAccordionItem}
          keyExtractor={item => item?.muniAuthPeriodId}
        />
      </View>
    </PageTemplate>
  );
};

export default SessionScreen;
