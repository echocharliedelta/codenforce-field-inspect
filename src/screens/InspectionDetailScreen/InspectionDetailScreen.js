import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  View,
} from 'react-native';
import PageTemplate from '../../components/templates/PageTemplate/PageTemplate';
import CustomInput from '../../components/molecules/CustomInput/CustomInput';
import {dynamicCommonStyles} from '../../assets/styles/CommonStyles';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import BottomButtonGroup from '../../components/atoms/BottomButtonGroup/BottomButtonGroup';
import IncrementDecrementCounter from '../../components/molecules/IncrementDecrementCounter/IncrementDecrementCounter';
import {useEffect, useState, useLayoutEffect} from 'react';
import Dropdowns from '../../components/atoms/Dropdowns/Dropdowns';
import DateTimePicker from '../../components/atoms/DateTimePicker/DateTimePicker';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {uiStartLoading, uiStopLoading} from '../../store/slices/uiSlice';
import Database from '../../database/Database';
import {OFFLINE_TABLE_NAME} from '../../config/config';
import {Helper} from '../../utils/Helper';
import LangStrings from '../../utils/langStrings';
import moment from 'moment';
import {offlineService} from '../../services/offlineService';
import CustomDialog from '../../components/molecules/CustomDialog/CustomDialog';
import {setSelectedIndex} from '../../store/slices/authSlice';
import {MasterDBUtils} from '../../utils/DBUtils/MasterDBUtils';
import useKeyboard from '../../utils/hooks/keyboardHook';

const defaultInspectionDetails = {
  causeId: '',
  causeOfInspection: '',
  generalComments: '',
  maxOccupantsAllowed: 0,
  numBedRooms: 0,
  numBathRooms: 0,
  notesPreInspection: '',
  dispatchNotes: '',
};

const InspectionDetailScreen = ({route}) => {
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const commonStyle = dynamicCommonStyles();
  const isKeyboardOpen = useKeyboard();

  const {userInfo} = useSelector(state => state?.auth);
  const {
    inspectionId,
    isFormAddInspectionScreen = false,
    addInspectionData = '',
    selectedCheckListItem = '',
    isFromDone = 0,
    propertyAddress = '',
    onClearState,
  } = route?.params;

  const [inspectionDetails, setInspectionDetails] = useState(
    defaultInspectionDetails,
  );
  const [InspectionCause, setInspectionCause] = useState([]);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [
    isShowDialogForCancelAddInspection,
    setIsShowDialogForCancelAddInspection,
  ] = useState(false);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerBackTitle: Platform.OS === 'ios' && 'Back',
      headerTitle: `${propertyAddress}`,
      headerStyle: {...commonStyle.hederStyle},
      headerTitleStyle: {...commonStyle.hederTitleStyle},
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigation]);

  useEffect(() => {
    if (isFocused) {
      getInspectionCause();
      getInspectionsDetails();
    } else {
      setInspectionDetails(defaultInspectionDetails);
      setInspectionCause([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  // getting inspection details from local db
  const getInspectionsDetails = async () => {
    try {
      if (isFormAddInspectionScreen) {
        const currentTime = Helper.getCurrentUTCDateTime(); //moment(Helper.getCurrentUTCDateTime()).format('MM/DD/YYYY HH:mm')
        setInspectionDetails({
          ...defaultInspectionDetails,
          effectiveDate: Helper.getCurrentUTCDateTime(),
          timeStart: currentTime,
        });
      } else {
        dispatch(uiStartLoading());
        const inspectionDetailsResponse = await Database.getRecords(
          OFFLINE_TABLE_NAME.INSPECTIONS,
          '*',
          `inspectionId = '${inspectionId}'`,
        );
        if (inspectionDetailsResponse?.length) {
          const formattedDetails = {
            ...inspectionDetailsResponse[0],
            timeStart: Helper.isUndefined(
              inspectionDetailsResponse[0]?.timeStart,
            )
              ? Helper.getCurrentUTCDateTime()
              : inspectionDetailsResponse[0]?.timeStart,
          };
          setInspectionDetails(formattedDetails);
        } else {
          Helper.showFlashMessage(
            'danger',
            LangStrings.LBL_NO_INSP_DETAILS_FOUND,
          );
          setInspectionDetails(defaultInspectionDetails);
        }
        dispatch(uiStopLoading());
      }
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage('danger', error);
    }
  };

  // getting list of inspection causes for dropdown
  const getInspectionCause = async () => {
    try {
      dispatch(uiStartLoading());
      const inspectionCause = await Database.getRecords(
        OFFLINE_TABLE_NAME.INSPECTION_CAUSE,
      );
      if (inspectionCause?.length) {
        let formattedData = await MasterDBUtils.prepareInspectionCauseObject(
          inspectionCause,
        );
        setInspectionCause(formattedData.data);
      } else {
        Helper.showFlashMessage(
          'danger',
          LangStrings.LBL_NO_INSP_DETAILS_FOUND,
        );
      }

      dispatch(uiStopLoading());
    } catch (error) {
      dispatch(uiStopLoading());
      Helper.showFlashMessage('danger', error);
    }
  };

  const generateErrorMessage = field => {
    switch (field) {
      case 'maxOccupantsAllowed':
        return 'Max Occupancy must be a number consisting one to nine digits.';
      case 'numBedRooms':
        return 'No. of Bedrooms must be a number consisting one to nine digits.';
      case 'numBathRooms':
        return 'No. of Bathrooms must be a number consisting one to nine digits.';
      default:
        return 'Invalid field.';
    }
  };

  const showErrorMessage = field => {
    const errorMessage = generateErrorMessage(field);
    Helper.showFlashMessage('danger', errorMessage);
  };

  const handleIncrementDecrement = (field, action) => {
    setInspectionDetails(prevState => {
      let newValue;
      if (action === 'increment') {
        newValue = Number(prevState[field]) + 1;
      } else if (action === 'decrement') {
        newValue = Math.max(Number(prevState[field]) - 1, 0);
      } else {
        newValue = prevState[field];
      }
      // Ensure the new value is between 0 and 9
      if (newValue <= 999999999) {
        return {
          ...prevState,
          [field]: newValue,
        };
      } else {
        showErrorMessage(field);
        return {
          ...prevState,
        };
      }
    });
  };

  const handleDirectValueForIncDec = (field, value) => {
    // Use a regular expression to remove non-numeric characters, excluding . , -
    const numericValue = value.replace(/[^0-9]/g, '');

    // Update the value with the numericValue if it has 9 or fewer digits
    if (numericValue.length <= 9) {
      setInspectionDetails(prevState => ({
        ...prevState,
        [field]: numericValue,
      }));
    } else {
      showErrorMessage(field);
    }
  };

  const toggleDateTimePicker = pickerType => {
    if (pickerType === 'date') {
      setDatePickerVisibility(!isDatePickerVisible);
    } else if (pickerType === 'time') {
      setTimePickerVisibility(!isTimePickerVisible);
    }
  };

  // changing inspection cause
  const changeInspectionCause = selectedValue => {
    setInspectionDetails({...inspectionDetails, causeId: selectedValue.id});
  };

  // changing effective Date
  const changeEffectiveDate = (date, time) => {
    const formattedTime = moment(time).utc().format('HH:mm:ss');
    const formattedDate = moment(date).utc().format('YYYY-MM-DD');
    const combinedDateTimeString = moment(
      `${formattedDate}T${formattedTime}`,
    ).format('YYYY-MM-DDTHH:mm:ss[Z]');
    setInspectionDetails({
      ...inspectionDetails,
      effectiveDate: combinedDateTimeString,
    });
    setDatePickerVisibility(false);
  };

  // changing start time
  const changeStartTime = time => {
    let timeInUTC = moment(`${time}`).utc().format('YYYY-MM-DDTHH:mm:ss[Z]');
    setInspectionDetails({...inspectionDetails, timeStart: timeInUTC});
    setTimePickerVisibility(false);
  };

  // handling input change
  const handleInputChange = (field, value) => {
    setInspectionDetails(prevState => ({
      ...prevState,
      [field]: value,
    }));
  };

  // saving updated data to local db

  const saveUpdatedData = async () => {
    if (isFormAddInspectionScreen) {
      let isSuccess = await offlineService.offlineAddNewInspection(
        {
          ...inspectionDetails,
          generalComments: inspectionDetails.generalComments.trim(),
          propertyAddress: addInspectionData.propertyAddress,
          unitNumber: addInspectionData?.unitNumber,
          checkListId: selectedCheckListItem,
          inspectorName: userInfo?.userName,
          isInspectionCreatedOnMobile: 1,
          isInspectionStarted: 1,
        },
        OFFLINE_TABLE_NAME.INSPECTIONS,
      );
      if (isSuccess) {
        dispatch(setSelectedIndex(0));
        navigation.navigate('Inspection', {
          setIndexToOne: false,
          isFromAddInspectionDetails: true,
        });
        Helper.showFlashMessage(
          'success',
          LangStrings.LBL_INSP_CREATED_SUCCESSFULLY,
        );
        onClearState();
      }
    } else {
      let isSuccess = await offlineService.offlineUpdateInspectionDetails(
        inspectionId,
        inspectionDetails,
        OFFLINE_TABLE_NAME.INSPECTIONS,
      );
      if (isSuccess) {
        dispatch(setSelectedIndex(0));
        navigation.navigate('Inspection', {setIndexToOne: false});
        Helper.showFlashMessage('success', LangStrings.LBL_CHANGES_UPDATED);
      }
    }
  };

  const cancelInspection = () => {
    if (isFormAddInspectionScreen) {
      setIsShowDialogForCancelAddInspection(true);
    } else {
      navigation.goBack();
    }
  };

  const customDialogFirstAction = () => {
    onClearState();
    navigation.navigate('Inspection', {setIndexToOne: false});
  };

  return (
    <PageTemplate>
      <KeyboardAvoidingView
        style={[
          {
            marginBottom:
              isKeyboardOpen.isKeyboardVisible && Platform.OS === 'ios'
                ? isKeyboardOpen.keyboardHeight
                : null,
          },
          commonStyle.F1,
        ]}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={[commonStyle.pH18, commonStyle.mT16, commonStyle.mB20]}>
          <View>
            <DateTimePicker
              mode="date"
              toggleDatePicker={() => {
                toggleDateTimePicker('date');
              }}
              onConfirm={changeEffectiveDate}
              isVisible={isDatePickerVisible}
              containerStyle={[
                commonStyle.pH0,
                commonStyle.h45,
                commonStyle.bR7,
              ]}
              inputContainerStyle={[
                commonStyle.bBTransparent,
                Platform.OS === 'ios' ? commonStyle.mL16 : commonStyle.mL12,
              ]}
              inputStyle={[commonStyle.dateTimePickerInput]}
              placeholder={LangStrings.LBL_ENTER_EFFECTIVE_DATE}
              value={
                inspectionDetails?.effectiveDate
                  ? moment(inspectionDetails?.effectiveDate).format(
                      'MM/DD/YYYY',
                    )
                  : null
              }
              textInputLabel={LangStrings.LBL_EFFECTIVE_DATE}
              rightIconContainerStyle={[
                commonStyle.loginTextInputIcon,
                commonStyle.pdL10,
              ]}
              disabled={isFromDone}
              rightIcon={
                <Icon
                  name={'calendar-clock-outline'}
                  size={25}
                  style={[commonStyle.iconColor, commonStyle.opacity50]}
                />
              }
            />
          </View>
          <View style={[commonStyle.mT10, commonStyle.mB10]}>
            <DateTimePicker
              mode="time"
              toggleDatePicker={() => {
                toggleDateTimePicker('time');
              }}
              onConfirm={changeStartTime}
              isVisible={isTimePickerVisible}
              containerStyle={[
                commonStyle.pH0,
                commonStyle.h45,
                commonStyle.bR7,
              ]}
              inputContainerStyle={[
                commonStyle.bBTransparent,
                Platform.OS === 'ios' ? commonStyle.mL16 : commonStyle.mL12,
              ]}
              inputStyle={[commonStyle.dateTimePickerInput]}
              value={
                inspectionDetails?.timeStart
                  ? moment(inspectionDetails?.timeStart).format(
                      'MM/DD/YYYY HH:mm',
                    )
                  : null
              }
              placeholder={LangStrings.LBL_ENTER_START_TIME}
              textInputLabel={LangStrings.LBL_START_TIME}
              rightIconContainerStyle={[
                commonStyle.loginTextInputIcon,
                commonStyle.pdL10,
              ]}
              disabled={isFromDone}
              rightIcon={
                <Icon
                  name={'calendar-clock-outline'}
                  size={25}
                  style={[commonStyle.iconColor, commonStyle.opacity50]}
                />
              }
            />
          </View>

          <Dropdowns
            placeholder={isFromDone ? '—' : 'Select cause of inspection'}
            disabled={isFromDone}
            title={LangStrings.LBL_CAUSE_FOR_INSP}
            data={InspectionCause}
            currentCauseId={inspectionDetails?.causeId}
            onchange={changeInspectionCause}
          />

          <View style={[commonStyle.pdT5]}>
            <View style={[commonStyle.pV5]}>
              <IncrementDecrementCounter
                onChangeText={value =>
                  handleDirectValueForIncDec('maxOccupantsAllowed', value)
                }
                title={LangStrings.LBL_MAX_OCCUPANCY}
                onPressInc={() =>
                  handleIncrementDecrement('maxOccupantsAllowed', 'increment')
                }
                onPressDec={() =>
                  handleIncrementDecrement('maxOccupantsAllowed', 'decrement')
                }
                count={
                  isFromDone
                    ? inspectionDetails?.maxOccupantsAllowed === 0
                      ? '—'
                      : inspectionDetails?.maxOccupantsAllowed
                    : inspectionDetails?.maxOccupantsAllowed
                }
                disabled={isFromDone}
              />
            </View>
            <View style={[commonStyle.pV5]}>
              <IncrementDecrementCounter
                onChangeText={value =>
                  handleDirectValueForIncDec('numBedRooms', value)
                }
                title={LangStrings.LBL_NUM_OF_BEDROOMS}
                onPressInc={() =>
                  handleIncrementDecrement('numBedRooms', 'increment')
                }
                onPressDec={() =>
                  handleIncrementDecrement('numBedRooms', 'decrement')
                }
                count={
                  isFromDone
                    ? inspectionDetails?.numBedRooms === 0
                      ? '—'
                      : inspectionDetails?.numBedRooms
                    : inspectionDetails?.numBedRooms
                }
                disabled={isFromDone}
              />
            </View>
          </View>
          <View style={[commonStyle.pV5]}>
            <IncrementDecrementCounter
              onChangeText={value =>
                handleDirectValueForIncDec('numBathRooms', value)
              }
              title={LangStrings.LBL_NUM_OF_BATHROOMS}
              onPressInc={() =>
                handleIncrementDecrement('numBathRooms', 'increment')
              }
              onPressDec={() =>
                handleIncrementDecrement('numBathRooms', 'decrement')
              }
              count={
                isFromDone
                  ? inspectionDetails?.numBathRooms === 0
                    ? '—'
                    : inspectionDetails?.numBathRooms
                  : inspectionDetails?.numBathRooms
              }
              disabled={isFromDone}
            />
          </View>
          <View style={commonStyle.mT10}>
            <CustomInput
              renderErrorMessage={false}
              multiline={true}
              numberOfLines={3}
              minHeight={Platform.OS === 'ios' ? 70 : 0}
              containerStyle={[commonStyle.pH0, commonStyle.bR7]}
              inputContainerStyle={[
                commonStyle.bBTransparent,
                Platform.OS === 'ios' ? commonStyle.mL16 : commonStyle.mL12,
              ]}
              inputStyle={[commonStyle.customInputStyle]}
              // value={inspectionDetails?.generalComments}
              onChangeText={value =>
                handleInputChange('generalComments', value)
              }
              placeholder={LangStrings.LBL_COMMENTS_GOES_HERE}
              // value={inspectionDetails?.generalComments != '' ? inspectionDetails?.generalComments : isFromDone ? '—' : ''}
              value={
                inspectionDetails?.generalComments === '' && isFromDone
                  ? '—'
                  : inspectionDetails?.generalComments
              }
              textInputLabel={LangStrings.LBL_COMMENTS}
              disabled={isFromDone}
            />
          </View>
          {!isFormAddInspectionScreen &&
          inspectionDetails?.isInspectionCreatedOnMobile !== 1 ? (
            <>
              <View style={commonStyle.mT20}>
                <CustomInput
                  renderErrorMessage={false}
                  multiline={true}
                  numberOfLines={3}
                  minHeight={Platform.OS === 'ios' ? 70 : 0}
                  containerStyle={[commonStyle.pH0, commonStyle.bR7]}
                  inputContainerStyle={[
                    commonStyle.bBTransparent,
                    Platform.OS === 'ios' ? commonStyle.mL16 : commonStyle.mL12,
                  ]}
                  inputStyle={[commonStyle.customInputStyle]}
                  onChangeText={value =>
                    handleInputChange('notesPreInspection', value)
                  }
                  value={inspectionDetails?.notesPreInspection}
                  placeholder={LangStrings.LBL_NOTES_GOES_HERE}
                  textInputLabel={LangStrings.LBL_PRE_INSPECTION_NOTES}
                  disabled={isFromDone}
                />
              </View>
              <View style={commonStyle.mT20}>
                <CustomInput
                  disabled={!isFormAddInspectionScreen}
                  renderErrorMessage={false}
                  multiline={true}
                  numberOfLines={3}
                  minHeight={Platform.OS === 'ios' ? 70 : 0}
                  containerStyle={[commonStyle.pH0, commonStyle.bR7]}
                  inputContainerStyle={[
                    commonStyle.bBTransparent,
                    Platform.OS === 'ios' ? commonStyle.mL16 : commonStyle.mL12,
                  ]}
                  inputStyle={[commonStyle.customInputStyle]}
                  onChangeText={value =>
                    handleInputChange('dispatchNotes', value)
                  }
                  value={inspectionDetails?.dispatchNotes}
                  placeholder={LangStrings.LBL_DISPATCH_NOTES_GOES_HERE}
                  textInputLabel={LangStrings.LBL_DISPATCH_NOTES}
                />
              </View>
            </>
          ) : null}
        </ScrollView>
        {!isFromDone ? (
          <BottomButtonGroup
            status={0}
            onPress={[() => cancelInspection(), () => saveUpdatedData()]}>
            <Text style={commonStyle.colorWhite}>CANCEL</Text>

            <Text style={commonStyle.colorWhite}>SAVE</Text>
          </BottomButtonGroup>
        ) : null}

        {isShowDialogForCancelAddInspection ? (
          <CustomDialog
            isVisible={isShowDialogForCancelAddInspection}
            onBackdropPress={() => {
              setIsShowDialogForCancelAddInspection(false);
            }}
            firstAction={customDialogFirstAction}
            secondAction={() => {
              setIsShowDialogForCancelAddInspection(false);
            }}
            title={LangStrings.LBL_CANCEL_ADD_INSPECTION_CONFIRMATION}
          />
        ) : null}
      </KeyboardAvoidingView>
    </PageTemplate>
  );
};

export default InspectionDetailScreen;
