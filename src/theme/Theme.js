import {DefaultTheme} from '@react-navigation/native';
export const LightTheme = {
  ...DefaultTheme,
  dark: false,
  colors: {
    WHITE: '#ffffff',
    BLACK: '#000000',

    BG_PRIMARY: '#00465C',
    BG_SECONDARY: '#EDFBFF',
    BG_TEXTAREA: '#F2F2F7',
    BG_LIGHT_BLUE: '#CCDFE4',
    BG_SEARCH: '#00465C',
    BG_BOTTOMSHEET: '#FFF',

    BG_SYNC: '#F58F31',
    BG_GREEN: '#65B824',

    TEXT_PRIMARY: '#E2E651',
    TEXT_SECONDARY: '#00465C',
    TEXT_INPUT: '#3C3C43',
    TEXT_LIGHT: '#969696',
    TEXT_MAIN: '#787878',
    TEXT_DARK_1: '#323232',
    TEXT_DARK_2: '#2F2F2F',
    TEXT_DARK_3: '#CBCBCB',
    TEXT_BLUE: '#00B4EA',
    BG_CATEGORY: '#EBEBEB',

    BORDER_PRIMARY: '#CCD9DD',

    BUTTON_PRIMARY: '#00B4EA',
    BUTTON_PRIMARY_LIGHT: '#C6ECF8',
    BUTTON_UPLOAD_INSP: '#00465C',

    BG_BOTTOM_NAVIGATION: '#323232',
    BG_BOTTOM_NAVIGATION_ICON: '#CDE161',
    ICON_COLOR: '#323232',
  },
};

export const DarkTheme = {
  ...DefaultTheme,
  dark: true,
  colors: {
    WHITE: '#ffffff',
    BLACK: '#000000',

    BG_PRIMARY: '#2F2F2F',
    BG_SECONDARY: '#2F2F2F',
    BG_TEXTAREA: 'rgba(242, 242, 247, 0.10)',
    BG_LIGHT_BLUE: '#CCDFE4',
    BG_CATEGORY: 'rgba(242, 242, 247, 0.10)',
    BG_SEARCH: 'rgba(242, 242, 247, 0.10)',
    BG_BOTTOMSHEET: '#000',

    BG_SYNC: '#F58F31',
    BG_GREEN: '#65B824',

    TEXT_PRIMARY: '#E2E651',
    TEXT_SECONDARY: 'rgba(235, 235, 235, 0.80)',
    TEXT_INPUT: 'rgba(255, 255, 255, 0.80)',
    TEXT_LIGHT: '#969696',
    TEXT_MAIN: 'rgba(255, 255, 255, 0.80)',
    TEXT_DARK_1: 'rgba(255, 255, 255, 0.80)',
    TEXT_DARK_2: 'rgba(255, 255, 255, 0.80)',
    TEXT_DARK_3: '#CBCBCB',
    TEXT_BLUE: '#00B4EA',
    BUTTON_UPLOAD_INSP: '#00465C',
    BORDER_PRIMARY: '#CCD9DD',
    BUTTON_PRIMARY: '#00B4EA',
    ICON_COLOR: '#323232',
  },
};
