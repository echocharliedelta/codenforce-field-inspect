import {Helper} from './Helper';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import moment from 'moment';

const checkIsBooleanValue = value => {
  if (['true', 'false'].includes(value)) {
    return value === 'true' ? 1 : 0;
  }
  return value;
};

const getSearchStringForDumpingList = async moduleName => {
  let municipality_code = await AsyncStorage.getItem('municipality_code');
  let auth_period = await AsyncStorage.getItem('auth_period');
  let LastSyncDate = await AsyncStorage.getItem(
    `${municipality_code}_LastSyncOfflineDataDate`,
  );
  let lastSyncDate = !Helper.isUndefined(LastSyncDate)
    ? '&lastupdatedts=' + LastSyncDate
    : '';
  let searchParamsString =
    '?municipality_code=' +
    municipality_code +
    '&auth_period=' +
    auth_period +
    lastSyncDate;
  return searchParamsString;
};

export const HelperOffline = {
  checkIsBooleanValue,
  getSearchStringForDumpingList,
};
