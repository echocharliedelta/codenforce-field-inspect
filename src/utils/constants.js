export const InspectionStatus = {
  IN_PROGRESS: 0,
  DONE: 1,
};

export const OFFLINE_INSPECTION_RELATIONAL = [
  'occInspectedSpaceElementList',
  'occinspectedspaces',
  'occinspection',
];

export const OFFLINE_BLOB_BYTES_RELATIONAL = [
  'occinspectedspaceelementphotodoc',
  'blobbytes',
];
