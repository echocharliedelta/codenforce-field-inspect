import {useEffect, useState} from 'react';
import {Keyboard, Platform} from 'react-native';

const useKeyboard = () => {
  const [keyboardStatus, setKeyboardStatus] = useState({
    isKeyboardVisible: false,
    keyboardHeight: 0,
  });

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      event => {
        setKeyboardStatus({
          isKeyboardVisible: true,
          keyboardHeight: event.endCoordinates.height,
        });
      },
    );

    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardStatus({
          isKeyboardVisible: false,
          keyboardHeight: 0,
        });
      },
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  return keyboardStatus;
};

export default useKeyboard;
