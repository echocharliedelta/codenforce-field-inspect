import {useColorScheme} from 'react-native';
import {useEffect, useState} from 'react';
const useAppearance = () => {
  const [isDarkMode, setIsDarkMode] = useState();
  const colorScheme = useColorScheme();
  useEffect(() => {
    const isDMode = colorScheme === 'dark';
    setIsDarkMode(isDMode);
  }, [colorScheme]);
  return isDarkMode;
};
export default useAppearance;
