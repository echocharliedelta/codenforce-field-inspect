import {checkHasInternet} from './functions/checkHasInternet';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../config/config';
import {Helper} from './Helper';
import {
  getCodeElementGuideSyncService,
  getInspectionDeterminationSyncService,
  getInspectionCauseSyncService,
  getInspectionIntensityClassesSyncService,
  getInspectionChecklistsSyncService,
  getCodeElementsSyncService,
  getChecklistSpaceTypesSyncService,
  getInspectionsListSyncService,
  uploadInspectionSyncService,
  uploadInspectionPhotosSyncService,
  getChecklistSpaceTypeElementsSyncService,
} from '../services/syncService';
import {SyncUtils} from './syncUtils';
import Database from '../database/Database';
import {HelperOffline} from './HelperOffline';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {InspectionDBUtils} from './DBUtils/InspectionDBUtils';
import moment from 'moment';
import {
  OFFLINE_BLOB_BYTES_RELATIONAL,
  OFFLINE_INSPECTION_RELATIONAL,
} from './constants';
import store from '../store/configureStore';

// To Sync Offline Data
const syncData = async userInfo => {
  console.log('=============:Start:dumpData');
  const municipality_code = await AsyncStorage.getItem('municipality_code');
  const isSignedIn = store.getState().auth.isSignedIn;
  if ((await checkHasInternet()) && isSignedIn) {
    let dumpStatus = [];
    dumpStatus.push(dumpMasterModulesData()); // Dump appointments server data to local
    dumpStatus.push(dumpInspectionsModulesData());
    let isAllDataDumped = await Promise.all(dumpStatus);
    if (isAllDataDumped) {
      let CurrentSyncDate = Helper.getCurrentUTCDateTime('YYYY-MM-DD hh:mm:ss');
      await AsyncStorage.setItem(
        `${municipality_code}_LastSyncOfflineDataDate`,
        CurrentSyncDate,
      ); // Added for set date after all //module sync
    }
  }
  await autoCleanData();
};

// To Dump Data into Local Storage
const dumpData = async (moduleName, tableName, selectFields) => {
  // let CurrentSyncDate = Helper.getCurrentCSTDateTime();
  return await getModuleWiseServiceSyncDataUptoDown(moduleName)
    .then(async response => {
      if (!Helper.isUndefined(response) && response.length) {
        await Database.insertList(tableName, response, selectFields)
          .then(async () => {
            // await AsyncStorage.setItem(
            //   'LastSyncDate_' + moduleName,
            //   CurrentSyncDate,
            // );
          })
          .catch(error => {
            console.log(error);
          });
      }
      return 1;
    })
    .catch(function (error) {
      console.log(error);
      return 0;
    });
};

const getModuleWiseServiceSyncDataUptoDown = async (
  moduleName,
  searchParams = '',
) => {
  let responseData = '';
  let searchParamsString = await HelperOffline.getSearchStringForDumpingList();

  switch (moduleName) {
    case OFFLINE_MODULE_NAME.INSPECTION_DETERMINATIONS:
      responseData = await getInspectionDeterminationSyncService(
        searchParamsString,
      );
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CAUSE:
      responseData = await getInspectionCauseSyncService(searchParamsString);
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_INTENSITY_CLASS:
      responseData = await getInspectionIntensityClassesSyncService(
        searchParamsString,
      );
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST:
      responseData = await getInspectionChecklistsSyncService(
        searchParamsString,
      );
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CODE_ELEMENT:
      responseData = await getCodeElementsSyncService(searchParamsString);
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CODE_ELEMENT_GUIDE:
      responseData = await getCodeElementGuideSyncService(searchParamsString);
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE:
      responseData = await getChecklistSpaceTypesSyncService(
        searchParamsString,
      );
      break;
    case OFFLINE_MODULE_NAME.INSPECTIONS:
      responseData = await getInspectionsListSyncService(
        searchParamsString,
        moduleName,
      );
      break;
    case OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE:
      responseData = await getInspectionsListSyncService(
        searchParamsString,
        moduleName,
      );
      break;
    case OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT:
      responseData = await getInspectionsListSyncService(
        searchParamsString,
        moduleName,
      );
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS:
      responseData = await getChecklistSpaceTypeElementsSyncService(
        searchParamsString,
        moduleName,
      );
  }
  return responseData;
};

const dumpMasterModulesData = async () => {
  let status = [];

  // ******** start Dump Determination Data STL ********
  const occInspectionDeterminationFields =
    await SyncUtils.getSyncFieldsByModuleWise(
      OFFLINE_MODULE_NAME.INSPECTION_DETERMINATIONS,
    );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTION_DETERMINATIONS,
      OFFLINE_TABLE_NAME.INSPECTION_DETERMINATIONS,
      occInspectionDeterminationFields,
    ),
  );

  // ******** start Dump Cause Data STL ********
  const occInspectionCauseFields = await SyncUtils.getSyncFieldsByModuleWise(
    OFFLINE_MODULE_NAME.INSPECTION_CAUSE,
  );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTION_CAUSE,
      OFFLINE_TABLE_NAME.INSPECTION_CAUSE,
      occInspectionCauseFields,
    ),
  );

  // ******** start Dump Intensity Data STL ********
  const occInspectionIntensityFields =
    await SyncUtils.getSyncFieldsByModuleWise(
      OFFLINE_MODULE_NAME.INSPECTION_INTENSITY_CLASS,
    );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTION_INTENSITY_CLASS,
      OFFLINE_TABLE_NAME.INSPECTION_INTENSITY_CLASS,
      occInspectionIntensityFields,
    ),
  );

  // ******** start Dump CHECKLIST Data STL ********
  const occInspectionChecklistFields =
    await SyncUtils.getSyncFieldsByModuleWise(
      OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST,
    );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST,
      OFFLINE_TABLE_NAME.INSPECTION_CHECKLIST,
      occInspectionChecklistFields,
    ),
  );

  // ******** start Dump INSPECTION_CODE_ELEMENT Data STL ********
  const occInspectionCodeElementFields =
    await SyncUtils.getSyncFieldsByModuleWise(
      OFFLINE_MODULE_NAME.INSPECTION_CODE_ELEMENT,
    );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTION_CODE_ELEMENT,
      OFFLINE_TABLE_NAME.INSPECTION_CODE_ELEMENT,
      occInspectionCodeElementFields,
    ),
  );

  // ******** start Dump Code Element Data STL ********
  const codeElementGuideFields = await SyncUtils.getSyncFieldsByModuleWise(
    OFFLINE_MODULE_NAME.INSPECTION_CODE_ELEMENT_GUIDE,
  );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTION_CODE_ELEMENT_GUIDE,
      OFFLINE_TABLE_NAME.INSPECTION_CODE_ELEMENT_GUIDE,
      codeElementGuideFields,
    ),
  );

  // ******** start Dump Inspection Checklist Space Type Data STL ********
  const inspectionChecklistSpaceTypeFields =
    await SyncUtils.getSyncFieldsByModuleWise(
      OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE,
    );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE,
      OFFLINE_TABLE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE,
      inspectionChecklistSpaceTypeFields,
    ),
  );

  // ******** start Dump Checklist space elements Data STL ********
  const occChecklistSpaceTypeElementsFields =
    await SyncUtils.getSyncFieldsByModuleWise(
      OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS,
    );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS,
      OFFLINE_TABLE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS,
      occChecklistSpaceTypeElementsFields,
    ),
  );

  let APP_DUMP_STATUS = await Promise.all(status);
  return APP_DUMP_STATUS;
};

const dumpInspectionsModulesData = async () => {
  let status = [];

  // ******** start Dump Inspections Data STL ********
  const occInspectionsFields = await SyncUtils.getSyncFieldsByModuleWise(
    OFFLINE_MODULE_NAME.INSPECTIONS,
  );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTIONS,
      OFFLINE_TABLE_NAME.INSPECTIONS,
      occInspectionsFields,
    ),
  );

  // ******** start Dump Inspections Inspected Space Data STL ********
  const occInspectedSpaceFields = await SyncUtils.getSyncFieldsByModuleWise(
    OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE,
  );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE,
      OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE,
      occInspectedSpaceFields,
    ),
  );

  // ******** start Dump Inspections Inspected Space Element Data STL ********
  const occInspectedSpaceElementFields =
    await SyncUtils.getSyncFieldsByModuleWise(
      OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
    );
  status.push(
    await dumpData(
      OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
      OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
      occInspectedSpaceElementFields,
    ),
  );

  let INSPECTIONS_APP_DUMP_STATUS = await Promise.all(status);
  return INSPECTIONS_APP_DUMP_STATUS;
};

const syncInspectionModulesDataDownToUp = async (
  userId,
  inspectionId = null,
) => {
  const isSignedIn = store.getState().auth.isSignedIn;
  if ((await checkHasInternet()) && isSignedIn) {
    let where = '';
    let otherJoin =
      ' JOIN occinspectedspaces ON occInspection.inspectionId = occinspectedspaces.occInspectionId';

    if (!Helper.isUndefined(inspectionId)) {
      where += ` inspectionId='${inspectionId}' AND isInspectionDone=${1} AND isSync=${0} GROUP BY inspectionId`;
    } else {
      where += `isInspectionDone=${1} AND isSync=${0} GROUP BY inspectionId`;
    }

    const inspectionListAvailableForSync = await Database.getRecords(
      OFFLINE_TABLE_NAME.INSPECTIONS,
      'occInspection.*,occinspectedspaces.*',
      where,
      otherJoin,
    );

    const dataPayload =
      await InspectionDBUtils.preparePayloadForUploadInspection(
        inspectionListAvailableForSync,
        userId,
      );

    if (dataPayload?.inspectionList?.length > 0) {
      const promises = dataPayload.inspectionList.map(async element => {
        try {
          await uploadInspectionSyncService(
            element,
            element?.muniCode,
            parseInt(element?.authPeriod),
            inspectionId,
          );
        } catch (error) {
          console.log('ERROR ****** in upload inspection', error);
        }
      });
      // Wait for all promises to resolve
      await Promise.all(promises);
      // Set lastSyncDateTime after all promises have resolved
      await AsyncStorage.setItem(
        'lastSyncDateTime',
        Helper.getCurrentUTCDateTime(),
      );
    }
  }
};

const syncInspectionModulesPhotosDataDownToUp = async (
  dataList,
  uploadedInspectionId,
) => {
  if (dataList?.length > 0) {
    dataList.forEach(async element => {
      try {
        let response = await uploadInspectionPhotosSyncService(
          element,
          element?.municipality_code,
          element?.auth_period,
        );
        if (!Helper.isUndefined(uploadedInspectionId)) {
          await AsyncStorage.removeItem('uploadedInspectionId');
        }
      } catch (error) {
        console.log('ERROR ****** in upload inspection', error?.response?.data);
      }
    });
  }
};

const autoCleanData = async () => {
  console.log('autoCleanData:start');
  // Delete Past 31 Days Inspections Offline Data
  await deletePastDatedInspectionPhotosData();
  await deletePastDatedInspectionRelationalData();

  console.log('autoCleanData:End');
  return 1;
};

const deletePastDatedInspectionPhotosData = async () => {
  const oneMonthPastDate = moment().subtract(1, 'months').format('YYYY-MM-DD');

  let where = `occinspection.effectiveDate <= '${oneMonthPastDate}%' AND blobbytes.isSync=0`;

  let inspection_join =
    ' JOIN occinspection on blobbytes.inspectionId = occinspection.inspectionId';

  const pastInspectionsPhotos = await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTIONS_BLOB_BYTE,
    'bytesId',
    where,
    inspection_join,
  );

  if (pastInspectionsPhotos?.length) {
    const blobbytesIds = [
      ...new Set(pastInspectionsPhotos.map(photo => `'${photo.bytesId}'`)),
    ].filter(bytesId => !Helper.isUndefined(bytesId));

    let whereInBlobBytesIds = [];

    if (blobbytesIds?.length) {
      whereInBlobBytesIds = `bytesId IN (${blobbytesIds.join(',')})`;
    }

    let deleteQueries = [];
    let inspectionBlobBytesRelationalTables = OFFLINE_BLOB_BYTES_RELATIONAL;

    inspectionBlobBytesRelationalTables.forEach(table => {
      if (blobbytesIds.length && whereInBlobBytesIds?.length) {
        let query = `DELETE FROM ${table} WHERE ${whereInBlobBytesIds}`;
        deleteQueries.push(query);
      }
    });

    await Database.getDatabase().then(async db => {
      return await db.sqlBatch(deleteQueries).then(async result => {
        console.log('PAST INSPECTIONS DELETE DONE');
        return result;
      });
    });
  }
};

const deletePastDatedInspectionRelationalData = async () => {
  const oneMonthPastDate = moment().subtract(1, 'months').format('YYYY-MM-DD');

  let where = `effectiveDate <= '${oneMonthPastDate}%' AND isSync=1`;

  let inspectedspaces_join =
    ' LEFT JOIN occinspectedspaces on occinspection.inspectionId = occinspectedspaces.occInspectionId';

  let inspectedspacesElement_join =
    ' LEFT JOIN occInspectedSpaceElementList on occInspectedSpaceElementList.inspectedSpaceId = occinspectedspaces.inspectedSpaceId';

  let otherJoin = inspectedspaces_join + inspectedspacesElement_join;

  const pastInspections = await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTIONS,
    'inspectionId , occinspectedspaces.inspectedSpaceId ,occInspectedSpaceElementList.inspectedSpaceElementId',
    where,
    otherJoin,
  );

  if (pastInspections?.length) {
    let inspectionIds = [
      ...new Set(pastInspections.map(inspection => inspection.inspectionId)),
    ].filter(inspectionId => !Helper.isUndefined(inspectionId));

    let inspectedSpacesIds = [
      ...new Set(
        pastInspections.map(inspection => inspection.inspectedSpaceElementId),
      ),
    ].filter(
      inspectedSpaceElementId => !Helper.isUndefined(inspectedSpaceElementId),
    );

    let whereInInspectionIds = [];
    let whereInOccInspectionIds = [];
    let whereInInspectedSpaceIds = [];

    if (inspectionIds?.length) {
      whereInInspectionIds = `inspectionId IN (${inspectionIds.join(',')})`;
      whereInOccInspectionIds = `occInspectionId IN (${inspectionIds.join(
        ',',
      )})`;
    }

    if (inspectedSpacesIds?.length) {
      whereInInspectedSpaceIds = `inspectedSpaceId IN (${inspectedSpacesIds.join(
        ',',
      )})`;
    }

    let deleteQueries = [];
    let inspectionRelationalTables = OFFLINE_INSPECTION_RELATIONAL;

    inspectionRelationalTables.forEach(table => {
      if (
        table === 'occInspectedSpaceElementList' &&
        whereInInspectedSpaceIds.length
      ) {
        let query = `DELETE FROM ${table} WHERE ${whereInInspectedSpaceIds}`;
        deleteQueries.push(query);
      } else if (
        table === 'occinspectedspaces' &&
        whereInOccInspectionIds.length
      ) {
        let query = `DELETE FROM ${table} WHERE ${whereInOccInspectionIds}`;
        deleteQueries.push(query);
      } else if (inspectionIds.length) {
        let query = `DELETE FROM ${table} WHERE ${whereInInspectionIds}`;
        deleteQueries.push(query);
      }
    });
    await Database.getDatabase().then(async db => {
      return await db.sqlBatch(deleteQueries).then(async result => {
        console.log('PAST INSPECTIONS DELETE DONE');
        return result;
      });
    });
  }
};

export const Sync = {
  syncData,
  syncInspectionModulesPhotosDataDownToUp,
  syncInspectionModulesDataDownToUp,
};
