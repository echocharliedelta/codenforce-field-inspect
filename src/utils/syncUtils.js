import {OFFLINE_MODULE_NAME} from '../config/config';

const getCodeElementGuideFields = () => {
  return ['guideEntryId', 'category', 'description', 'muniCode'];
};

const getOccInspectionDeterminations = () => {
  return [
    'determinationId',
    'title',
    'description',
    'qualifiesAsPassed',
    'deactivatedTs',
    'muniCode',
  ];
};

const getOccInspectionCauses = () => {
  return ['causeId', 'title', 'deactivatedTs', 'muniCode'];
};

const getOccInspectionIntensityClass = () => {
  return [
    'classId',
    'title',
    'municode',
    'active',
    'numericRating',
    'schemaName',
    'iconId',
  ];
};

const getOccInspectionChecklist = () => {
  return [
    'checkListId',
    'title',
    'description',
    'active',
    'governingCodeSourceId',
    'municode',
    'deactivatedTs',
  ];
};

const getOccInspectionElement = () => {
  return [
    'elementId',
    'codeSourceId',
    'ordChapterNo',
    'ordChapterTitle',
    'ordSecNum',
    'ordSecTitle',
    'ordSubSecNum',
    'ordSubSecTitle',
    'ordTechnicalText',
    'guideEntryId',
    'notes',
    'ordSubSubSecNum',
    'deactivatedTS',
    'name',
    'year',
    'muniCode',
    'headerStringStatic',
  ];
};

const getOccInspectionSpaceTypes = () => {
  return [
    'checklistSpaceTypeId',
    'checklistId',
    'required',
    'spaceTypeId',
    'deactivatedTs',
    'spaceTitle',
    'description',
    'muniCode',
  ];
};

const getOccInspectionsFields = () => {
  return [
    'inspectionId',
    'dispatchId',
    'occChecklistId',
    'causeId',
    'propertyAddress',
    'unitNumber',
    'effectiveDate',
    'timeStart',
    'numBedRooms',
    'maxOccupantsAllowed',
    'notesPreInspection',
    'dispatchNotes',
    'numBathRooms',
    'determinationDetId',
    'generalComments',
    'retrievalTS',
    'synchronizationTS',
    'synchronizationNotes',
    'inspectorName',
    'muniCode',
    'authPeriod',
    'isInspectionCreatedOnMobile',
    'determinationStatus',
    'isInspectionStarted',
  ];
};

const getOccInspectedSpacesFields = () => {
  return [
    'inspectedSpaceId',
    'occInspectionId',
    'occChecklistSpaceTypeId',
    'deactivatedts',
    'inspectedSpaceDeactivatedByUserId',
    'addedToChecklistByUserid',
    'addedToChecklistTS',
  ];
};

const getOCcInspectedSpaceElementFields = () => {
  return [
    'inspectedSpaceElementId',
    'notes',
    'lastInspectedByUserId',
    'lastInspectedTS',
    'complianceGrantedByUserId',
    'complianceGrantedTS',
    'inspectedSpaceId',
    'occChecklistSpaceTypeElementId',
    'failureSeverityIntensityClassId',
    'codeSetElementId',
    'codeElementId',
    'required',
  ];
};

const getOccInspectionBlobBytes = () => {
  return [
    'bytesId',
    'inspectionId',
    'blob',
    'metadata',
    'municode',
    'uploadedbyuserid',
    'uploadbydts',
    'lastupdatedts',
    'deactivatedts',
  ];
};

const getOccInspectionElementPhotoDoc = () => {
  return ['bytesId', 'inspectedSpaceElementId'];
};

const getOccChecklistSpaceTypeElements = () => {
  return [
    'spaceElementId',
    'required',
    'checklistSpaceTypeId',
    'codeSetElementId',
    'deactivatedTs',
    'lastupdatedTs',
    'codeElementLinkedId',
  ];
};

const getSyncFieldsByModuleWise = async module => {
  let module_fields = [];
  switch (module) {
    case OFFLINE_MODULE_NAME.INSPECTION_DETERMINATIONS:
      module_fields = getOccInspectionDeterminations();
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CAUSE:
      module_fields = getOccInspectionCauses();
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_INTENSITY_CLASS:
      module_fields = getOccInspectionIntensityClass();
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST:
      module_fields = getOccInspectionChecklist();
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CODE_ELEMENT:
      module_fields = getOccInspectionElement();
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CODE_ELEMENT_GUIDE:
      module_fields = getCodeElementGuideFields();
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE:
      module_fields = getOccInspectionSpaceTypes();
      break;
    case OFFLINE_MODULE_NAME.INSPECTIONS:
      module_fields = getOccInspectionsFields();
      break;
    case OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE:
      module_fields = getOccInspectedSpacesFields();
      break;
    case OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT:
      module_fields = getOCcInspectedSpaceElementFields();
      break;
    case OFFLINE_MODULE_NAME.INSPECTIONS_BLOB_BYTE:
      module_fields = getOccInspectionBlobBytes();
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_INSPECTED_ELEMENT_PHOTO_DOC:
      module_fields = getOccInspectionElementPhotoDoc();
      break;
    case OFFLINE_MODULE_NAME.INSPECTION_CHECKLIST_SPACE_TYPE_ELEMENTS:
      module_fields = getOccChecklistSpaceTypeElements();
  }
  return module_fields;
};

export const SyncUtils = {
  getCodeElementGuideFields,
  getSyncFieldsByModuleWise,
  getOccInspectionDeterminations,
  getOccInspectionCauses,
  getOccInspectionIntensityClass,
  getOccInspectionChecklist,
  getOccInspectionElement,
  getOccInspectionSpaceTypes,
  getOccInspectionsFields,
  getOccInspectedSpacesFields,
  getOCcInspectedSpaceElementFields,
  getOccInspectionElementPhotoDoc,
  getOccInspectionBlobBytes,
  getOccChecklistSpaceTypeElements,
};
