import LocalizedStrings from 'react-native-localization';
import en from '../assets/lables/en_us.json';
// import es from './languages/es.json';

const LangStrings = new LocalizedStrings({
  en: en,
  // es: es,
});

export default LangStrings;
