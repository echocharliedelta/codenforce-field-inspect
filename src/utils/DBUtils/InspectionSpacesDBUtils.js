import moment from 'moment';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../../config/config';
import Database from '../../database/Database';
import {Helper} from '../Helper';

const prepareInspectionSpacesObject = async (
  dataList,
  isFromUpload = false,
  userId,
) => {
  if (dataList.length) {
    let inspectedSpaceElementDTOName = isFromUpload
      ? 'occInspectedSpaceElementDTOList'
      : 'occInspectedSpaceElementList';

    const inspections = dataList.map(async space => {
      return {
        inspectedSpaceId:
          Helper.isUUID(space?.inspectedSpaceId) && isFromUpload
            ? 0
            : space?.inspectedSpaceId,
        occInspectionId:
          isFromUpload && Helper.isUUID(space?.occInspectionId)
            ? 0
            : space?.occInspectionId,
        occChecklistSpaceTypeId: space?.occChecklistSpaceTypeId,
        spaceTitle: space?.spaceTitle,
        inspectedSpaceDeactivatedTS: space?.deactivatedts,
        inspectedSpaceDeactivatedByUserId:
          space?.inspectedSpaceDeactivatedByUserId,
        addedToChecklistByUserid: space?.addedToChecklistByUserid,
        addedToChecklistTS: moment().utc().format('YYYY-MM-DDTHH:mm:ss[Z]'), //space?.addedToChecklistTS,
        appInspectedSpaceId: space?.inspectedSpaceId.toString(),
        [inspectedSpaceElementDTOName]: await getInspectedSpacesRelationData(
          OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
          OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE_ELEMENT,
          {
            inspectedSpaceId: space?.inspectedSpaceId,
            isFromUpload: isFromUpload,
            userId: userId,
          },
        ),
      };
    });
    const inspectionList = await Promise.all(inspections);
    return isFromUpload ? {inspectionList} : {data: inspectionList};
  }
};

const getInspectedSpacesRelationData = async (moduleName, tableName, ids) => {
  let inspectedSpacesRelationalData =
    await Database.getInspectedSpacesRelationData(moduleName, tableName, ids);
  if (ids?.isFromUpload) {
    let formattedData = await prepareInspectionSpacesElementListObject(
      inspectedSpacesRelationalData,
      ids?.userId,
    );

    console.log('FORMATTED DATA ***********', formattedData);

    return formattedData?.length > 0 ? formattedData : [];
  } else {
    return inspectedSpacesRelationalData;
  }
};

const prepareAddSpacesListObject = async dataList => {
  if (dataList.length) {
    const spaces = dataList.map(async space => {
      return {
        checklistSpaceTypeId: space?.checklistSpaceTypeId,
        checklistId: space?.checklistId,
        required: space?.required === 1 ? true : false,
        spaceTypeId: space?.spaceTypeId,
        spaceTitle: space?.spaceTitle,
        description: space?.description,
      };
    });
    const availableSpacesList = await Promise.all(spaces);
    return {data: availableSpacesList};
  }
};

const prepareAddSpaceObject = async data => {
  if (!Helper.isUndefined(data)) {
    return {
      inspectedSpaceId: await Helper.getUUID(),
      occInspectionId: data?.inspectionId,
      occChecklistSpaceTypeId: data?.checklistSpaceTypeId,
      deactivatedts: '',
      addedToChecklistByUserid: data?.userId,
      addedToChecklistTS: Helper.getCurrentUTCDateTime(),
    };
  }
};

const prepareAddSpaceElementObject = async (
  dataList,
  checklistSpaceTypeId,
  inspectedSpaceId,
) => {
  if (dataList.length) {
    const elements = dataList.map(async element => {
      return {
        inspectedSpaceElementId: await Helper.getUUID(),
        inspectedSpaceId: inspectedSpaceId,
        // occChecklistSpaceTypeElementId: checklistSpaceTypeId,
        occChecklistSpaceTypeElementId: element?.occChecklistSpaceTypeElementId,
        codeSetElementId: element?.codeSetElementId,
        notes: '',
        lastInspectedByUserId: '',
        lastInspectedTS: '',
        complianceGrantedByUserId: '',
        complianceGrantedTS: '',
        failureSeverityIntensityClassId: '',
        required: 1,
        codeElementId: element?.elementId,
        deactivatedts: '',
      };
    });
    const elementList = await Promise.all(elements);
    return elementList;
  }
};

const prepareInspectionSpacesElementListObject = async (dataList, userId) => {
  if (dataList.length) {
    const elements = dataList.map(async element => {
      return {
        inspectedSpaceElementId: Helper.isUUID(element?.inspectedSpaceElementId)
          ? 0
          : parseInt(element?.inspectedSpaceElementId, 10),
        inspectedSpaceId: Helper.isUUID(element?.inspectedSpaceId)
          ? 0
          : parseInt(element?.inspectedSpaceId, 10),
        occChecklistSpaceTypeElementId: Helper.isUUID(
          element?.occChecklistSpaceTypeElementId,
        )
          ? 0
          : parseInt(element?.occChecklistSpaceTypeElementId, 10),
        codeSetElementId: Helper.isUUID(element?.codeSetElementId)
          ? 0
          : parseInt(element?.codeSetElementId, 10),
        notes: element?.notes,
        lastInspectedByUserId: Helper.isUndefined(
          element?.lastInspectedByUserId,
        )
          ? null
          : parseInt(element?.lastInspectedByUserId, 10),
        lastInspectedTS: element?.lastInspectedTS,
        complianceGrantedByUserId: Helper.isUndefined(
          element?.complianceGrantedByUserId,
        )
          ? null
          : parseInt(element?.complianceGrantedByUserId, 10),
        complianceGrantedTS: element?.complianceGrantedTS,
        failureSeverityIntensityClassId: Helper.isUndefined(
          element?.failureSeverityIntensityClassId,
        )
          ? ''
          : parseInt(element?.failureSeverityIntensityClassId, 10),
        required: element?.required ? true : false,
        codeElementId: element?.codeElementId,
        deactivatedts: element?.deactivatedts,
        appInspectedSpaceElementId: element?.inspectedSpaceElementId,
      };
    });
    const elementList = await Promise.all(elements);
    return elementList;
  }
};

export const InspectionSpacesDBUtils = {
  prepareInspectionSpacesObject,
  getInspectedSpacesRelationData,
  prepareAddSpacesListObject,
  prepareAddSpaceObject,
  prepareAddSpaceElementObject,
};
