import AsyncStorage from '@react-native-async-storage/async-storage';

const prepareOccChecklistSpaceTypeObject = async dataList => {
  let muniCode = await AsyncStorage.getItem('municipality_code');
  if (dataList.length) {
    const inspections = dataList.map(async inspection => {
      return {
        ...inspection,
        muniCode: muniCode,
      };
    });
    const inspectionList = await Promise.all(inspections);
    return inspectionList;
  }
};

const prepareIntensityObject = async dataList => {
  let muniCode = await AsyncStorage.getItem('municipality_code');
  if (dataList.length) {
    const inspections = dataList.map(async inspection => {
      return {
        ...inspection,
        muniCode: muniCode,
      };
    });
    const inspectionList = await Promise.all(inspections);
    return inspectionList;
  }
};

const prepareInspectionCauseObject = async dataList => {
  if (dataList.length) {
    const inspections = dataList.map(async app => {
      return {
        id: app?.causeId,
        name: app?.title,
      };
    });
    const inspectionList = await Promise.all(inspections);
    return {data: inspectionList};
  }
};

const prepareInspectionCheckListObject = async dataList => {
  if (dataList.length) {
    const checkList = dataList.map(async checkListElement => {
      return {
        id: checkListElement?.checkListId,
        title: checkListElement?.title,
        description: checkListElement?.description,
      };
    });
    const checkListData = await Promise.all(checkList);
    return {data: checkListData};
  }
};

const prepareInspectionDeterminationObject = async dataList => {
  if (dataList.length) {
    const determinations = dataList.map(async app => {
      return {
        id: app?.determinationId,
        name: app?.title,
      };
    });
    const determinationList = await Promise.all(determinations);
    return {data: determinationList};
  }
};

const prepareOrdinanceViolationSeverity = async dataList => {
  if (dataList.length) {
    const violationSeverity = dataList.map(async app => {
      return {
        id: app?.classId,
        name: app?.title,
        active: app?.active,
      };
    });
    const violationSeverityList = await Promise.all(violationSeverity);
    return {data: violationSeverityList};
  }
};

export const MasterDBUtils = {
  prepareOccChecklistSpaceTypeObject,
  prepareIntensityObject,
  prepareInspectionCauseObject,
  prepareInspectionCheckListObject,
  prepareInspectionDeterminationObject,
  prepareOrdinanceViolationSeverity,
};
