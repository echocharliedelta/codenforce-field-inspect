import AsyncStorage from '@react-native-async-storage/async-storage';
import {Helper} from '../Helper';
import {OFFLINE_MODULE_NAME, OFFLINE_TABLE_NAME} from '../../config/config';
import Database from '../../database/Database';
import {offlineService} from '../../services/offlineService';

const prepareInspectionObject = async dataList => {
  if (dataList.length) {
    const inspections = dataList.map(async inspection => {
      return {
        inspectionId: inspection?.inspectionId,
        dispatchId: inspection?.dispatchId,
        propertyAddress: inspection?.propertyAddress,
        unitNumber: inspection?.unitNumber,
        effectiveDate: inspection?.effectiveDate,
        checklistName: inspection?.title,
        inspectorName: inspection?.inspectorName,
        overallPhotosCount: await getPhotosCountsAgainstInspections(
          inspection?.inspectionId,
        ), //inspection?.overAllPhotosCount,
        inspectedSpacesCount: await getInspectedSpacesCountsAgainstInspections(
          inspection?.inspectionId,
        ), //inspection?.inspectedSpacesCount,
        occChecklistId: inspection?.occChecklistId,
        governingCodeSourceId: inspection?.governingCodeSourceId,
        authPeriod: inspection?.authPeriod,
        isSync: inspection?.isSync,
        isInspectionCreatedOnMobile: inspection?.isInspectionCreatedOnMobile,
        determinationStatus: !Helper.isUndefined(
          inspection?.determinationStatus,
        )
          ? inspection?.determinationStatus
          : 'Pending Finalization',
        isInspectionStarted: Helper.isUndefined(inspection?.isInspectionStarted)
          ? 0
          : inspection?.isInspectionStarted,
        timeStart: inspection.timeStart,
      };
    });
    const inspectionList = await Promise.all(inspections);
    return {data: inspectionList};
  }
};

const getPhotosCountsAgainstInspections = async inspectionId => {
  return await offlineService.getPhotoCountAgainstInspection(inspectionId);
};

const getInspectedSpacesCountsAgainstInspections = async inspectionId => {
  return await offlineService.getInspectedSpacesCountAgainstInspection(
    inspectionId,
  );
};

const prepareInspectionOverallPhotosObject = async dataList => {
  if (dataList.length) {
    const overallPhotos = dataList.map(async photos => {
      return {
        bytesId: photos?.bytesId,
        inspectionId: photos?.inspectionId,
        imageOrder: photos?.imageOrder,
        blob: 'data:image/jpeg;base64,' + photos?.blob,
      };
    });
    const overallPhotosList = await Promise.all(overallPhotos);
    return {data: overallPhotosList};
  }
};

const prepareInspectionDetailsObject = async dataList => {
  if (dataList.length) {
    const inspections = dataList.map(async app => {
      return {
        inspectionId: app?.inspectionId,
        dispatchId: app?.dispatchId,
        propertyAddress: app?.propertyAddress,
        unitNumber: app?.unitNumber,
        effectiveDate: app?.effectiveDate,
        inspectorName: app?.inspectorName,
        numBedRooms: app?.numBedRooms,
        numBathRooms: app?.numBedRooms,
        maxOccupantsAllowed: app?.maxOccupantsAllowed,
        timeStart: app?.timeStart,
        notesPreInspection: app?.notesPreInspection,
        generalComments: app?.generalComments,
        causeId: app?.causeId,
      };
    });
    const inspectionList = await Promise.all(inspections);
    return {data: inspectionList[0]};
  }
};

const addInspectionPhotoRequest = async (imageData, inspectionId, userInfo) => {
  const municipality_code = await AsyncStorage.getItem('municipality_code');
  return {
    bytesId: await Helper.getUUID(),
    inspectionId: inspectionId,
    blob: imageData.image_name,
    metadata: imageData.file_name,
    municode: municipality_code,
    uploadedbyuserid: userInfo?.userId,
    uploadbydts: await Helper.getCurrentUTCDateTime(),
    lastupdatedts: await Helper.getCurrentUTCDateTime(),
    deactivatedts: null,
    isSync: 1,
  };
};

const prepareInspectedOrdinanceAndCategories = async (dataList = '') => {
  if (dataList.length) {
    const uniqueCategoryByKey = [
      ...new Map(
        dataList
          .filter(item => !Helper.isUndefined(item.guideEntryId)) // Filter out items where guideEntryId is null
          .map(item => [item.guideEntryId, item]),
      ).values(),
    ];

    const categoryList = uniqueCategoryByKey.map(async category => {
      return {
        guideEntryId: category?.guideEntryId,
        category: category?.category,
        description: category?.description,
      };
    });
    const categoriesList = await Promise.all(categoryList);

    const ordinancesData = dataList.map(async ordinance => {
      return {
        inspectedSpaceElementId: ordinance?.inspectedSpaceElementId,
        inspectedSpaceId: ordinance?.inspectedSpaceId,
        codeElementId: ordinance?.codeElementId,
        name: ordinance?.name,
        year: ordinance?.year,
        ordSubSecTitle: ordinance?.ordSubSecTitle,
        ordSubSecNum: ordinance?.ordSubSecNum,
        complianceGrantedTS: ordinance?.complianceGrantedTS,
        lastInspectedTS: ordinance?.lastInspectedTS,
        ordTechnicalText: ordinance?.ordTechnicalText,
        guideEntryId: ordinance?.guideEntryId,
        elementId: ordinance?.elementId,
        headerStringStatic: ordinance?.headerStringStatic,
        codeSetElementId: ordinance?.codeSetElementId,
        occChecklistSpaceTypeElementId: ordinance?.spaceElementId,
        lastInspectedByUserId: ordinance?.lastInspectedByUserId,
        complianceGrantedByUserId: ordinance?.complianceGrantedByUserId,
      };
    });
    const ordinancesList = await Promise.all(ordinancesData);
    return {
      ordinancesList: ordinancesList,
      categoryList: [
        {
          guideEntryId: null,
          category: 'All',
          description: 'Test',
        },
        ...categoriesList,
      ],
    };
  }
};

const prepareAddSNewInspectionObject = async data => {
  let muniCode = await AsyncStorage.getItem('municipality_code');
  let auth_period = await AsyncStorage.getItem('auth_period');
  if (!Helper.isUndefined(data)) {
    return {
      inspectionId: await Helper.getUUID(),
      dispatchId: '',
      occChecklistId: data?.checkListId,
      causeId: data?.causeId,
      propertyAddress: data?.propertyAddress,
      unitNumber: data?.unitNumber,
      effectiveDate: data?.effectiveDate,
      timeStart: data?.timeStart,
      numBedRooms: data?.numBedRooms,
      maxOccupantsAllowed: data?.maxOccupantsAllowed,
      notesPreInspection: data?.notesPreInspection,
      dispatchNotes: data?.dispatchNotes,
      numBathRooms: data?.numBathRooms,
      determinationDetId: '',
      generalComments: data?.generalComments,
      retrievalTS: '',
      synchronizationTS: '',
      synchronizationNotes: '',
      muniCode: muniCode,
      authPeriod: auth_period,
      inspectorName: data?.inspectorName,
      isInspectionCreatedOnMobile: data?.isInspectionCreatedOnMobile,
      isInspectionStarted: data?.isInspectionStarted,
    };
  }
};

const preparePayloadToAddFindingPhoto = async data => {
  return {
    bytesId: data.bytesId,
    inspectedSpaceElementId: data.inspectedSpaceElementId,
  };
};

const preparePayloadForUploadInspection = async (dataList, userId) => {
  if (dataList.length) {
    const inspections = dataList.map(async inspection => {
      return {
        inspectionId: Helper.isUUID(inspection?.inspectionId)
          ? 0
          : parseInt(inspection?.inspectionId),
        dispatchId: Helper.isNull(inspection?.dispatchId)
          ? 0
          : inspection?.dispatchId,
        propertyAddress: inspection?.propertyAddress,
        unitNumber: inspection?.unitNumber,
        inspectorUserId: userId,
        occChecklistId: inspection?.occChecklistId,
        notesPreInspection: inspection?.notesPreInspection,
        maxOccupantsAllowed: inspection?.maxOccupantsAllowed,
        numBedRooms: inspection?.numBedRooms,
        numBathRooms: inspection?.numBathRooms,
        effectiveDate: inspection?.effectiveDate, //Helper.getCurrentUTCDateTime(),
        timeStart: inspection?.timeStart, //Helper.getCurrentUTCDateTime(),//"00:30",//,
        determinationDetId: inspection?.determinationDetId, //inspection?.determinationDetId,
        determinationByUserId: Helper.isUndefined(
          inspection?.determinationDetId,
        )
          ? ''
          : userId,
        determinationTS: inspection?.determinationTS,
        generalComments: inspection?.generalComments,
        causeId: inspection?.causeId,
        dispatchNotes: inspection?.dispatchNotes,
        retrievalTS: inspection?.retrievalTS,
        synchronizationTS: inspection?.synchronizationTS,
        synchronizationNotes: inspection?.synchronizationNotes,
        dispatchLastUpdatedTs: null,
        appInspectionId: inspection?.inspectionId,
        muniCode: inspection?.muniCode,
        authPeriod: inspection?.authPeriod,
        occInspectedSpaceDTOList: await getOccInspectedDTOList(
          dataList,
          userId,
          inspection?.inspectionId,
        ),
      };
    });
    const inspectionList = await Promise.all(inspections);
    return {inspectionList};
  }
};

const getOccInspectedDTOList = async (dataList, userId, inspectionId) => {
  let inspectedSpacesDataList =
    await Database.getInspectedSpacesOfflineListData(
      OFFLINE_MODULE_NAME.INSPECTIONS_INSPECTED_SPACE,
      OFFLINE_TABLE_NAME.INSPECTIONS_INSPECTED_SPACE,
      {inspectionId: inspectionId, isFromUpload: true, userId: userId},
    );
  return inspectedSpacesDataList?.inspectionList;
};

const prepareInspectionOverallPhotosObjectForSync = async dataList => {
  if (dataList.length) {
    const imagesData = await dataList.map(async blob => {
      return {
        inspectionId: parseInt(blob?.inspectionId),
        municipality_code: blob?.muniCode,
        auth_period: parseInt(blob?.authPeriod),
        inspectedSpaceElementIds: await getOrdinancePhotosForSync(
          blob?.bytesId,
        ),
        blobBytes: {
          bytesId: 0,
          createdTS: blob?.uploadbydts,
          blob: blob?.blob,
          uploadedByUserId: blob?.uploadedbyuserid,
          fileName: blob?.metadata,
        },
      };
    });
    const imageList = await Promise.all(imagesData);
    console.log('IMGAE LIST ******************', imageList);
    return imageList;
  }
};

const getOrdinancePhotosForSync = async bytesId => {
  let ordinanceIds;
  let where = `deactivatedts='' OR deactivatedts IS NULL AND bytesId='${bytesId}'`;

  let ordinancePhotosIds = await Database.getRecords(
    OFFLINE_TABLE_NAME.INSPECTION_INSPECTED_ELEMENT_PHOTO_DOC,
    'inspectedSpaceElementId',
    where,
  );

  if (ordinancePhotosIds?.length) {
    ordinanceIds = ordinancePhotosIds.map(element => {
      return element.inspectedSpaceElementId;
    });
  } else {
    ordinanceIds = [];
  }
  return ordinanceIds;
};

export const InspectionDBUtils = {
  prepareInspectionObject,
  addInspectionPhotoRequest,
  prepareInspectionOverallPhotosObject,
  prepareInspectionDetailsObject,
  prepareInspectedOrdinanceAndCategories,
  prepareAddSNewInspectionObject,
  preparePayloadToAddFindingPhoto,
  preparePayloadForUploadInspection,
  prepareInspectionOverallPhotosObjectForSync,
  getPhotosCountsAgainstInspections,
};
