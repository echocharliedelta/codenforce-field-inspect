import {Notifier, Easing, NotifierComponents} from 'react-native-notifier';
import moment from 'moment';
import UUIDGenerator from 'react-native-uuid-generator';
import momenttz from 'moment-timezone';

const isNull = value => {
  return value === '' ? true : false;
};

const showFlashMessage = (type = '', description = '') => {
  if (!isNull(type) && !isUndefined(description)) {
    Notifier.showNotification({
      description: description,
      duration: 5000,
      showAnimationDuration: 800,
      showEasing: Easing.bounce,
      Component: NotifierComponents.Alert,
      componentProps: {
        alertType: type === 'success' ? 'success' : 'error',
      },
    });
  }
};

const isUndefined = value => {
  return value === '' ||
    value === undefined ||
    value === null ||
    value === 'undefined'
    ? true
    : false;
};

const checkIsValidEmail = text => {
  const regex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regex.test(text) === false) {
    return false;
  } else {
    return true;
  }
};

const isEqual = (val, compareVal) => {
  return val === compareVal;
};

function escapeDoubleQuotes(str) {
  let strNew = str.toString();
  return strNew.replace(/"/g, '""');
}

const getCurrentDateTime = (format = '') => {
  return moment().format('YYYY-MM-DD HH:mm:ss');
};

const capitalizeFirstLetter = string => {
  return string.charAt(0).toUpperCase() + string.toLowerCase().slice(1);
};

async function getUUID() {
  return await UUIDGenerator.getRandomUUID();
}

async function getUUIDNumber() {
  var str = await getUUID();
  var numb = str.match(/\d/g);
  numb = numb.join('').substring(0, 8);
  return parseInt(numb);
}

const getCurrentCSTDateTime = (format = '') => {
  let date = new Date();
  let dateFormat = format ? format : 'YYYY-MM-DD HH:mm:ss';
  return momenttz(date).tz('america/chicago').format(dateFormat);
};

const getCurrentUTCDateTime = (format = 'YYYY-MM-DDTHH:mm:ss[Z]') => {
  return moment().utc().format(format);
};

const truncateString = (str, num) => {
  if (str.length > num) {
    return str.slice(0, num) + '...';
  } else {
    return str;
  }
};

const getOrdinanceInspectionStatus = data => {
  if (
    Helper.isNull(data?.complianceGrantedTS) &&
    !Helper.isNull(data?.lastInspectedTS)
  ) {
    return 2;
  } else if (!Helper.isUndefined(data?.complianceGrantedTS)) {
    return 1;
  } else {
    return 0;
  }
};

const isUUID = str => {
  const uuidRegex =
    /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
  return uuidRegex.test(str);
};

export const Helper = {
  isEqual,
  isNull,
  checkIsValidEmail,
  isUndefined,
  showFlashMessage,
  escapeDoubleQuotes,
  getCurrentDateTime,
  capitalizeFirstLetter,
  getUUID,
  getUUIDNumber,
  getCurrentCSTDateTime,
  truncateString,
  getOrdinanceInspectionStatus,
  isUUID,
  getCurrentUTCDateTime,
};
