import NetInfo from '@react-native-community/netinfo';

export const checkHasInternet = () => {
  return new Promise(async resolve => {
    const {isConnected} = await NetInfo.fetch();
    resolve(isConnected);
  });
};
