import {Linking} from 'react-native';
export const openUrlInBrowser = async url => {
  Linking.openURL(url);
};
