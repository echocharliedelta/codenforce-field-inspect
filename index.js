/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import {store, persister} from './src/store/configureStore';
import {PersistGate} from 'redux-persist/integration/react';
import App from './src/App';
import {LoadingScreen} from './src/screens';

const Root = () => (
  <Provider store={store}>
    <PersistGate
      loading={
        <>
          <LoadingScreen />
        </>
      }
      persistor={persister}>
      <App />
    </PersistGate>
  </Provider>
);

AppRegistry.registerComponent(appName, () => Root);
